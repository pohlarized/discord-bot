from sqlalchemy import BigInteger, MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int_pk

REACTION_ROLES_SCHEMA_NAME = "reaction_roles"


class Base(DeclarativeBase):
    metadata = MetaData(schema=REACTION_ROLES_SCHEMA_NAME)


class ReactionRoleMessage(Base):
    __tablename__ = "reaction_role_messages"
    message_id: Mapped[big_int_pk]
    role_id: Mapped[int] = mapped_column(BigInteger)
