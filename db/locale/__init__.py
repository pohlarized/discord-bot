import datetime
from zoneinfo import ZoneInfo

from sqlalchemy.orm import DeclarativeBase, Mapped

from db import big_int_pk


class Base(DeclarativeBase):
    pass


class Locale(Base):
    __tablename__ = "locales"
    id: Mapped[big_int_pk]
    locale: Mapped[str]

    def __str__(self) -> str:
        return str(self.locale)

    @property
    def tzinfo(self) -> datetime.tzinfo:
        return ZoneInfo(self.locale)
