from __future__ import annotations

import base64
import enum
import secrets
from datetime import datetime, timedelta  # noqa: TC003 required by SQLAlchemy
from functools import cached_property
from typing import NamedTuple

from discord import ChannelType
from sqlalchemy import BigInteger, Enum, ForeignKey, Identity, MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int, big_int_pk  # noqa: TC001 required by SQLAlchemy

DISCORD_REWIND_SCHEMA_NAME = "discord_rewind"


class CommandType(enum.Enum):
    CLASSIC = 1
    CUSTOM = 2
    SLASH = 3


class Base(DeclarativeBase):
    metadata = MetaData(schema=DISCORD_REWIND_SCHEMA_NAME)


class ApiKey(Base):
    __tablename__ = "api_keys"
    user_id: Mapped[big_int_pk]
    api_key: Mapped[bytes] = mapped_column(unique=True)

    @staticmethod
    def generate(user_id: int) -> ApiKey:
        return ApiKey(user_id=user_id, api_key=secrets.token_bytes(32))

    @cached_property
    def urlsafe_key(self) -> str:
        return base64.urlsafe_b64encode(self.api_key).decode("utf-8")


class Member(Base):
    __tablename__ = "members"
    id: Mapped[big_int_pk]
    username: Mapped[str]
    display_name: Mapped[str]
    avatar_url: Mapped[str]


class Channel(Base):
    __tablename__ = "channels"
    id: Mapped[big_int_pk]
    guild_id: Mapped[big_int | None]
    parent_id: Mapped[big_int | None]
    name: Mapped[str]
    type: Mapped[ChannelType] = mapped_column(Enum(ChannelType))


class VoiceEventKind(enum.Enum):
    Start = enum.auto()
    End = enum.auto()


class VoiceEvent(NamedTuple):
    user_id: int
    channel_id: int
    timestamp: datetime
    uncertain: bool
    kind: VoiceEventKind


class VoiceSession(Base):
    __tablename__ = "voice_sessions"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    user_id: Mapped[big_int]
    channel_id: Mapped[big_int]
    start: Mapped[datetime]
    end: Mapped[datetime | None]
    end_uncertain: Mapped[bool]
    start_uncertain: Mapped[bool]

    def is_complete(self) -> bool:
        return self.end is not None

    def session_length(self) -> timedelta:
        if self.end is None:
            raise RuntimeError
        return self.end - self.start

    def into_events(self) -> tuple[VoiceEvent, VoiceEvent]:
        if self.end is None:
            raise RuntimeError
        start_event = VoiceEvent(
            user_id=self.user_id,
            channel_id=self.channel_id,
            timestamp=self.start,
            uncertain=self.start_uncertain,
            kind=VoiceEventKind.Start,
        )
        end_event = VoiceEvent(
            user_id=self.user_id,
            channel_id=self.channel_id,
            timestamp=self.end,
            uncertain=self.end_uncertain,
            kind=VoiceEventKind.End,
        )
        return (start_event, end_event)


class StreamSession(Base):
    __tablename__ = "stream_sessions"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    user_id: Mapped[big_int]
    channel_id: Mapped[big_int]
    start: Mapped[datetime]
    end: Mapped[datetime | None]
    end_uncertain: Mapped[bool]
    start_uncertain: Mapped[bool]

    def is_complete(self) -> bool:
        return self.end is not None

    def session_length(self) -> timedelta:
        if self.end is None:
            raise RuntimeError
        return self.end - self.start


class MessageSend(Base):
    __tablename__ = "message_sends"
    id: Mapped[big_int_pk]
    author_id: Mapped[big_int]
    channel_id: Mapped[big_int]
    timestamp: Mapped[datetime]
    content: Mapped[str]


class Mention(Base):
    __tablename__ = "mentions"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    message_id: Mapped[int] = mapped_column(
        BigInteger, ForeignKey(MessageSend.id, ondelete="CASCADE")
    )
    mentioned_id: Mapped[big_int]


class MessageEdit(Base):
    __tablename__ = "message_edits"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    author_id: Mapped[big_int]
    message_id: Mapped[big_int]
    channel_id: Mapped[big_int]
    timestamp: Mapped[datetime]
    new_content: Mapped[str]


class MessageDelete(Base):
    __tablename__ = "message_deletes"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    author_id: Mapped[big_int | None]
    message_id: Mapped[big_int]
    channel_id: Mapped[big_int]
    timestamp: Mapped[datetime]


class ReactionAdd(Base):
    __tablename__ = "reaction_additions"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    author_id: Mapped[big_int]
    message_id: Mapped[big_int]
    channel_id: Mapped[big_int]
    emoji_id: Mapped[big_int | None]
    emoji_name: Mapped[str]
    timestamp: Mapped[datetime]


class ThreadCreate(Base):
    __tablename__ = "thread_creations"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    channel_id: Mapped[big_int]
    thread_id: Mapped[big_int]
    creator_id: Mapped[big_int]
    timestamp: Mapped[datetime]


class CommandExecuted(Base):
    __tablename__ = "command_executions"
    id: Mapped[int] = mapped_column(Identity(), primary_key=True)
    channel_id: Mapped[big_int]
    author_id: Mapped[big_int]
    message_id: Mapped[big_int]
    command_name: Mapped[str]
    success: Mapped[bool]
    type: Mapped[CommandType] = mapped_column(Enum(CommandType))
    timestamp: Mapped[datetime]
