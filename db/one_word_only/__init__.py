from __future__ import annotations

import datetime
from typing import TYPE_CHECKING

from sqlalchemy import MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int_pk  # noqa: TCH001 required for SQLAlchemy

if TYPE_CHECKING:
    import discord

ONE_WORD_ONLY_SCHEMA_NAME = "one_word_only"


class Base(DeclarativeBase):
    metadata = MetaData(schema=ONE_WORD_ONLY_SCHEMA_NAME)


class Message(Base):
    __tablename__ = "messages"
    id: Mapped[big_int_pk]
    timestamp: Mapped[datetime.datetime]
    content: Mapped[str]

    def __repr__(self):
        return str(self)

    def __str__(self):
        return f'<Message id={self.id}, timestamp={self.timestamp}, content="{self.content}">'

    @classmethod
    def from_discord_message(cls, message: discord.Message) -> Message:
        naive_dt = message.created_at.replace(tzinfo=None)
        return cls(id=message.id, timestamp=naive_dt, content=message.content)

    def get_utc_time(self) -> datetime.datetime:
        return self.timestamp.replace(tzinfo=datetime.UTC)


class Day(Base):
    __tablename__ = "days"
    day: Mapped[int] = mapped_column(primary_key=True)
