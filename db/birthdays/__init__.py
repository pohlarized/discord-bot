from sqlalchemy import BigInteger, ForeignKey, MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int_pk
from utils.numbers import stndrdth

BIRTHDAYS_SCHEMA_NAME = "birthdays"

MONTHS = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December",
}


class Base(DeclarativeBase):
    metadata = MetaData(schema=BIRTHDAYS_SCHEMA_NAME)


class Birthday(Base):
    __tablename__ = "birthdays"
    user_id: Mapped[big_int_pk]
    day: Mapped[int]
    month: Mapped[int]

    def __str__(self):
        return f"{MONTHS[self.month]} {self.day}{stndrdth(self.day)}"


class Congratulation(Base):
    __tablename__ = "congratulations"
    user_id: Mapped[int] = mapped_column(
        BigInteger, ForeignKey(Birthday.user_id, ondelete="CASCADE"), primary_key=True
    )
    congratulated_today: Mapped[bool]
