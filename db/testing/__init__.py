from datetime import datetime

from sqlalchemy import BigInteger, MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy.schema import Identity

from db import big_int, big_int_pk

TESTING_SCHEMA_NAME = "testing"


class Base(DeclarativeBase):
    metadata = MetaData(schema=TESTING_SCHEMA_NAME)


class Roll(Base):
    __tablename__ = "rolls"
    id: Mapped[int] = mapped_column(BigInteger, Identity(), primary_key=True)
    timestamp: Mapped[datetime]
    roller_id: Mapped[big_int]
    rolled_id: Mapped[big_int]


class CountClick(Base):
    __tablename__ = "count_clicks"
    discord_id: Mapped[big_int_pk]
    amount: Mapped[int]


class CountMeta(Base):
    __tablename__ = "count_meta"
    count: Mapped[big_int_pk]
    last_click_id: Mapped[big_int_pk]
