from sqlalchemy import BigInteger, MetaData, Text
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int, big_int_pk

DISCORD_STREAMS_SCHEMA_NAME = "discord_streams"


class Base(DeclarativeBase):
    metadata = MetaData(schema=DISCORD_STREAMS_SCHEMA_NAME)


class Subscription(Base):
    __tablename__ = "subscriptions"
    streamer: Mapped[big_int_pk]
    subscriber: Mapped[big_int_pk]

    def __repr__(self):
        return f"<Subscription subscriber={self.subscriber}, streamer={self.streamer}>"


class Streamer(Base):
    __tablename__ = "streamers"
    id: Mapped[int] = mapped_column("streamer", BigInteger, primary_key=True)
    status: Mapped[str | None]
    last_game_status_update: Mapped[big_int] = mapped_column("last_status_update")
    is_live: Mapped[bool]
    live_message_id: Mapped[big_int | None]
    last_gone_offline: Mapped[big_int] = mapped_column("last_seen")

    def __repr__(self):
        return (
            f"<Streamer id={self.id}, status={self.status}, "
            f"last_status_update={self.last_game_status_update}, is_lve={self.is_live}, "
            f"last_seen={self.last_gone_offline}>"
        )


class Following(Base):
    __tablename__ = "followings"
    game: Mapped[str] = mapped_column(Text, primary_key=True)
    follower: Mapped[big_int_pk]

    def __repr__(self):
        return f"<Following game={self.game}, follower={self.follower}>"
