from sqlalchemy import MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int_pk

ACTIVITY_SCHEMA_NAME = "activity"


class Base(DeclarativeBase):
    metadata = MetaData(schema=ACTIVITY_SCHEMA_NAME)


class Record(Base):
    __tablename__ = "activity_records"
    member_id: Mapped[big_int_pk]
    channel_id: Mapped[big_int_pk]
    message_count: Mapped[int] = mapped_column(default=0)
    word_count: Mapped[int] = mapped_column(default=0)
    message_count_monthly: Mapped[int] = mapped_column(default=0)
    word_count_monthly: Mapped[int] = mapped_column(default=0)

    def __repr__(self):
        return (
            f"<ActivityRecord member_id={self.member_id}, "
            f"channel_id={self.channel_id}, "
            f"message_count={self.message_count}, "
            f"word_count={self.word_count}, "
            f"message_count_monthly={self.message_count_monthly}, "
            f"word_count_monthly={self.word_count_monthly}>"
        )

    def __str__(self):
        return self.__repr__()


class Month(Base):
    __tablename__ = "months"
    number: Mapped[int] = mapped_column(primary_key=True, default=0)


class Normie(Base):
    """A user that will not get tracked"""

    __tablename__ = "normies"
    member_id: Mapped[big_int_pk]
