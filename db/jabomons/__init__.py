from __future__ import annotations

import enum
from datetime import datetime  # noqa: TCH003 required by SQLAlchemy
from typing import TYPE_CHECKING

from sqlalchemy import BigInteger, Enum, ForeignKey, MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column
from sqlalchemy.schema import Identity

from config import config
from db import big_int  # noqa: TCH001 required by SQLAlchemy

if TYPE_CHECKING:
    import discord
    from discord.ext import commands

JABOMONS_SCHEMA_NAME = "jabomons"


class Base(DeclarativeBase):
    metadata = MetaData(schema=JABOMONS_SCHEMA_NAME)


class Rarity(enum.IntEnum):
    COMMON = 1
    UNCOMMON = 2
    RARE = 3
    EPIC = 4
    LEGENDARY = 5

    @classmethod
    async def convert(cls, _ctx: commands.Context, argument: str) -> Rarity | None:
        try:
            rarity = cls[argument.upper()]
        except KeyError:
            rarity = None
        return rarity


class Jabkscore(enum.Enum):
    MIN = config.ids.emt_min
    TWO = config.ids.emt_two
    THREE = config.ids.emt_three
    FOUR = config.ids.emt_four
    FIVE = config.ids.emt_five
    SIX = config.ids.emt_six
    SEVEN = config.ids.emt_seven
    EIGHT = config.ids.emt_eight
    NINE = config.ids.emt_nine
    TEN = config.ids.emt_ten
    MAX = config.ids.emt_max


class RLRank(enum.IntEnum):
    Bronze_I = 0
    Bronze_II = 1
    Bronze_III = 2
    Silver_I = 3
    Silver_II = 4
    Silver_III = 5
    Gold_I = 6
    Gold_II = 7
    Gold_III = 8
    Platinum_I = 9
    Platinum_II = 10
    Platinum_III = 11
    Diamond_I = 12
    Diamond_II = 13
    Diamond_III = 14
    Champion_I = 15
    Champion_II = 16
    Champion_III = 17
    Grand_Champion_I = 18
    Grand_Champion_II = 19
    Grand_Champion_III = 20
    Super_Sonic_Legend = 21


class Quality(enum.IntEnum):
    Battle_Scarred = 0
    Well_Worn = 1
    Field_Tested = 2
    Minimal_Wear = 3
    Factory_New = 4


class EventType(enum.Enum):
    ROLL = 1
    GUESS = 2
    EVOLUTION = 3
    COMBINATION = 4


class Jabomon(Base):
    __tablename__ = "jabomons"
    id: Mapped[int] = mapped_column(BigInteger, Identity(), primary_key=True)
    rarity: Mapped[Rarity] = mapped_column(Enum(Rarity))


class Item(Base):
    __tablename__ = "items"
    id: Mapped[int] = mapped_column(BigInteger, Identity(), primary_key=True)
    inventory_id: Mapped[big_int]
    jabomon_id: Mapped[int] = mapped_column(BigInteger, ForeignKey(Jabomon.id, ondelete="CASCADE"))
    shiny: Mapped[bool]
    jabkscore: Mapped[Jabkscore] = mapped_column(Enum(Jabkscore))
    quality: Mapped[Quality] = mapped_column(Enum(Quality))
    rl_rank: Mapped[RLRank] = mapped_column(Enum(RLRank))

    def to_member(self, guild: discord.Guild) -> discord.Member | None:
        return guild.get_member(self.id)


class User(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(BigInteger, primary_key=True)


class Event(Base):
    __tablename__ = "events"
    id: Mapped[int] = mapped_column(BigInteger, Identity(), primary_key=True)
    timestamp: Mapped[datetime]
    type: Mapped[EventType] = mapped_column(Enum(EventType))
    user_id: Mapped[int] = mapped_column(BigInteger, ForeignKey(User.id, ondelete="CASCADE"))
    jabomon_id: Mapped[int] = mapped_column(BigInteger, ForeignKey(User.id, ondelete="CASCADE"))
    shiny: Mapped[bool]
    jabkscore: Mapped[Jabkscore] = mapped_column(Enum(Jabkscore))
    quality: Mapped[Quality] = mapped_column(Enum(Quality))
    rl_rank: Mapped[RLRank] = mapped_column(Enum(RLRank))
