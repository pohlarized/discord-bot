import datetime

from sqlalchemy import MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int, big_int_pk

REMINDME_SCHEMA_NAME = "remindme"


class Base(DeclarativeBase):
    metadata = MetaData(schema=REMINDME_SCHEMA_NAME)


class Reminder(Base):
    __tablename__ = "reminders"
    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[big_int]
    text: Mapped[str]
    time: Mapped[datetime.datetime]

    def __repr__(self) -> str:
        return (
            f"<Reminder id={self.id}, user_id={self.user_id}, text={self.text}, "
            f"time={self.time}>"
        )

    @property
    def utc_time(self) -> datetime.datetime:
        return self.time.replace(tzinfo=datetime.UTC)

    def localtime(self, tz: datetime.tzinfo) -> datetime.datetime:
        return self.utc_time.astimezone(tz)


class Separator(Base):
    __tablename__ = "separators"
    user_id: Mapped[big_int_pk]
    separator: Mapped[str] = mapped_column(primary_key=True)
