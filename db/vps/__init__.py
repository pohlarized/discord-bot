from sqlalchemy import MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

VPS_SCHEMA_NAME = "vps"


class Base(DeclarativeBase):
    metadata = MetaData(schema=VPS_SCHEMA_NAME)


class VpsService(Base):
    __tablename__ = "vps_services"
    name: Mapped[str] = mapped_column(primary_key=True)
    description: Mapped[str]
    connect_info: Mapped[str]
