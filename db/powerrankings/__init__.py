from datetime import date

from sqlalchemy import MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int_pk

POWERRANKINGS_SCHEMA_NAME = "powerrankings"


class Base(DeclarativeBase):
    metadata = MetaData(schema=POWERRANKINGS_SCHEMA_NAME)


class Record(Base):
    __tablename__ = "records"
    member_id: Mapped[big_int_pk]
    day: Mapped[date] = mapped_column(primary_key=True)
    no_messages: Mapped[int]
    no_words: Mapped[int]
    no_emojis: Mapped[int]

    def __repr__(self) -> str:
        return (
            f"<PowerrankingRecord member_id={self.member_id}, "
            f"day={self.day.isoformat()}"
            f"no_messages={self.no_messages}, "
            f"no_words={self.no_words}, "
            f"no_emojis={self.no_emojis}"
        )
