from datetime import datetime

from sqlalchemy import Integer, MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from db import big_int_pk

ONEUPPUZZLE_SCHEMA_NAME = "oneuppuzzle"


class Base(DeclarativeBase):
    metadata = MetaData(schema=ONEUPPUZZLE_SCHEMA_NAME)


class CacheUpdate(Base):
    __tablename__ = "cache_update"
    date: Mapped[datetime] = mapped_column(primary_key=True)


class Puzzle(Base):
    __tablename__ = "puzzles"
    id: Mapped[str]
    number: Mapped[int] = mapped_column(Integer, primary_key=True)
    data: Mapped[str]
    create_time: Mapped[datetime]
    update_time: Mapped[datetime]

    @property
    def link(self) -> str:
        return f"https://www.oneuppuzzle.com/play/{self.id}"


class Result(Base):
    __tablename__ = "score"
    user_id: Mapped[big_int_pk]
    game_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    time_seconds: Mapped[int]
