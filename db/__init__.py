import datetime
import logging
import traceback
from collections.abc import Callable
from typing import Annotated

from sqlalchemy import BigInteger, create_engine
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.asyncio import async_sessionmaker as _async_sessionmaker
from sqlalchemy.orm import mapped_column
from sqlalchemy.schema import CreateSchema

from config import config

ASYNC_ENGINE = create_async_engine(
    f"postgresql+asyncpg://{config.pg_user}:{config.pg_pass}@{config.pg_host}:5432/discordbot"
)
BLOCKING_ENGINE = create_engine(
    f"postgresql://{config.pg_user}:{config.pg_pass}@{config.pg_host}:5432/discordbot"
)
async_sessionmaker: Callable[[], AsyncSession] = _async_sessionmaker(
    ASYNC_ENGINE, expire_on_commit=False, class_=AsyncSession
)

big_int = Annotated[int, mapped_column(BigInteger)]
big_int_pk = Annotated[int, mapped_column(BigInteger, primary_key=True)]


async def create_schema(schema_name: str):
    async with async_sessionmaker() as session, session.begin():
        await session.execute(CreateSchema(schema_name, if_not_exists=True))


def datetime_now() -> datetime.datetime:
    return datetime.datetime.now(datetime.UTC).replace(tzinfo=None)


def to_db_datetime(dt: datetime.datetime) -> datetime.datetime:
    if dt.tzinfo is None:
        logging.warning(
            "Using naive datetime for DB, assuming it as UTC but an explicit timezone would help!"
            "Traceback: %s",
            "".join(traceback.format_stack()),
        )
        dt = dt.replace(tzinfo=datetime.UTC)
    return dt.astimezone(datetime.UTC).replace(tzinfo=None)
