from sqlalchemy import MetaData
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

CUSTOM_COMMANDS_SCHEMA_NAME = "commands"


class Base(DeclarativeBase):
    metadata = MetaData(schema=CUSTOM_COMMANDS_SCHEMA_NAME)


class Command(Base):
    __tablename__ = "commands"
    name: Mapped[str] = mapped_column(primary_key=True)
    content: Mapped[str]

    def __repr__(self):
        return f"<Command name={self.name}, content={self.content}>"
