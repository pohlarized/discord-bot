from sqlalchemy import ForeignKey, MetaData, Text
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship

from db import big_int, big_int_pk

TWITCH_SCHEMA_NAME = "twitch"


class Base(DeclarativeBase):
    metadata = MetaData(schema=TWITCH_SCHEMA_NAME)


class Streamer(Base):
    __tablename__ = "streamers"
    name: Mapped[str] = mapped_column(primary_key=True)
    live_message_id: Mapped[big_int | None]
    subscriptions: Mapped[list["Subscription"]] = relationship(
        cascade="all, delete", passive_deletes=True
    )


class Subscription(Base):
    __tablename__ = "subscriptions"
    streamer: Mapped[str] = mapped_column(
        Text, ForeignKey(Streamer.name, ondelete="CASCADE"), primary_key=True
    )
    subscriber: Mapped[big_int_pk]


class OauthToken(Base):
    __tablename__ = "oauth_token"
    token: Mapped[str] = mapped_column(primary_key=True)
