import asyncio
import logging
import traceback
from pathlib import Path

import aiofiles
import discord
from discord.ext import commands

from bot import Bot
from utils.context import GuildContext
from utils.paginate import paginate_codeblock


class AdminCog(commands.Cog):
    """
    Cog for admin commands

    Mostly for bot management and deletion of messages
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @commands.command()
    @commands.guild_only()
    @commands.is_owner()
    async def sync(self, ctx: GuildContext):
        async with ctx.channel.typing():
            self.bot.tree.copy_global_to(guild=ctx.guild)
            await ctx.bot.tree.sync(guild=ctx.guild)
        await ctx.send("Successfully synced all app commands to the current guild.")

    @commands.command()
    @commands.is_owner()
    async def play(self, ctx: commands.Context, *, text: str):
        """
        Set the bots playing state

        Parameters
        ----------
        text : str
            The 'game' you want to set the bot to

        Examples
        --------
        >>> !play bullying polders
        Sets the bots playing state to 'bullying polders'
        """
        await self.bot.change_presence(activity=discord.Game(name=text.strip(), type=1))
        self.logger.info('Set playing status to "%s".', text)
        await ctx.send(f"Successfully changed the playing game to: **{text}**")

    @commands.command()
    @commands.is_owner()
    async def purge_me(self, ctx: commands.Context, limit: int | None = None):
        count = 0
        async for message in ctx.channel.history(limit=limit):
            if message.author.bot:
                try:
                    await message.delete()
                    count += 1
                except Exception as e:
                    print(e)
        msg = await ctx.channel.send(f"Successfully deleted {count} messages")
        await asyncio.sleep(10)
        await msg.delete()

    @commands.command()
    @commands.is_owner()
    async def load(self, ctx: commands.Context, *, module: str):
        """
        Load a module

        Parameters
        ----------
        module : str
            the name of the module you want to load

        Examples
        --------
        >>> !load admin
        Loads the admin module
        """
        try:
            await self.bot.load_extension(f"cogs.{module}")
        except Exception:
            await ctx.send(f"```py\n{traceback.format_exc()}\n```")
        else:
            await ctx.send(f"Successfully loaded `cogs.{module}`")
            self.logger.info("Successfully loaded `cogs.%s`", module)

    @commands.command()
    @commands.is_owner()
    async def unload(self, ctx: commands.Context, *, module: str):
        """
        Unload a module

        Parameters
        ----------
        module : str
            the name of the module you want to load

        Examples
        --------
        >>>!unload admin
        Unloads the admin module
        """
        try:
            await self.bot.unload_extension(f"cogs.{module}")
        except Exception:
            await ctx.send(f"```py\n{traceback.format_exc()}\n```")
        else:
            await ctx.send(f"Successfully unloaded `cogs.{module}`")
            self.logger.info("Successfully unloaded `cogs.%s`", module)

    @commands.command(name="reload")
    @commands.is_owner()
    async def _reload(self, ctx: commands.Context, *, module: str):
        """
        Reload a module

        Parameters
        ----------
        module : str
            the name of the module you want to load

        Examples
        --------
        >>> !reload admin
        Reloads the admin module
        """
        if module == "*":
            for cog in self.bot.extensions:
                await self.bot.unload_extension(cog)
                await self.bot.load_extension(cog)
            self.logger.info("Successfully reloaded all cogs.")
            return await ctx.send("Successfully reloaded all cogs.")
        try:
            await self.bot.unload_extension(f"cogs.{module}")
            await self.bot.load_extension(f"cogs.{module}")
        except Exception:
            await ctx.send(f"```py\n{traceback.format_exc()}\n```")
        else:
            await ctx.send(f"Successfully reloaded `cogs.{module}`")
            self.logger.info("Successfully reloaded `cogs.%s`", module)

    @commands.command(name="list_cogs")
    @commands.is_owner()
    async def list_cogs(self, ctx: commands.Context):
        """
        List all available cogs
        """
        cogs_path = Path(__file__).parent
        modules = [
            child.with_suffix("").name
            for child in cogs_path.iterdir()
            if child.suffix == ".py" and not child.name.startswith("_")
        ]
        msg = "\n".join(modules)
        return await ctx.send(f"```\n{msg}\n```")

    @commands.command()
    @commands.is_owner()
    async def bye(self, ctx: commands.Context, message: discord.Message):
        """
        Delete a message by messageid
        Parameters
        ----------
        message : Message
            An identifier of the message (ID, link, ...)

        Examples
        --------
        >>> !bye 1232432138320999
        Deletes the message with the id 1232432139320999
        """
        try:
            await message.delete()
        except discord.NotFound:
            await ctx.send("That message does not seem to exist.")
            return
        self.logger.info(
            "Deleted message with id `%d` in channel `%s`", message.id, message.channel
        )

    @commands.command()
    @commands.is_owner()
    async def edit(self, ctx: commands.Context, message: discord.Message, *, new_content: str):
        try:
            await message.edit(content=new_content)
        except discord.NotFound:
            await ctx.send("That message does not seem to exist.")
            return
        self.logger.info(
            "Edited message with id `%d` in channel `%s`.", message.id, message.channel
        )

    @commands.command()
    @commands.is_owner()
    async def log(self, ctx: commands.Context, rows: int = 20):
        """Show the last <rows> rows of the bot log"""
        log_path = Path(__file__).parent.parent.joinpath("bot.log")
        async with aiofiles.open(log_path, encoding="utf-8") as fp:
            lines = await fp.readlines()
        relevant_lines = "".join(lines[-rows:])
        codeblock = f"```\n{relevant_lines}\n```"
        for message in paginate_codeblock(codeblock):
            await ctx.send(f"```\n{message}\n```")


async def setup(bot: Bot):
    await bot.add_cog(AdminCog(bot))
