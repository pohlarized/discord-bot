import io
import logging
import math
import random
import re
import statistics
import sys
import traceback
from collections.abc import Sequence
from datetime import UTC, date, datetime, timedelta
from typing import Any, NamedTuple

import discord
from discord.ext import commands
from emoji import emoji_count
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession
from tabulate import tabulate

import db
from bot import Bot
from db.powerrankings import POWERRANKINGS_SCHEMA_NAME, Base, Record
from utils import format_table_message
from utils.constants import TIMEZONE
from utils.context import GuildContext
from utils.errors import KnownGuildNotFoundError

EMOJI_REGEX = r"<:\w+:[0-9]+>"


class Result(NamedTuple):
    member_id: int
    no_messages: int
    no_emojis: int


class GraphFlags(commands.FlagConverter):
    limit: commands.Range[int, 1, 20] = 10
    start_date: date = date(2022, 10, 8)


def today_midnight_utc() -> datetime:
    return datetime.now(UTC).replace(hour=0, minute=0, second=0, microsecond=0)


def calculate_power(no_emojis: int, no_messages: int) -> float:
    min_no_messages = 30
    if no_messages < min_no_messages:
        return 0.0
    emojis_per_message = no_emojis / no_messages
    return emojis_per_message * math.pi * math.e * 100


def count_discord_emojis(message: str) -> int:
    return len(re.findall(EMOJI_REGEX, message))


def offset_str(n: float) -> str:
    if n == 0:
        return "=  0"
    approx_offset = math.ceil(n / 10) * 10 - 5
    return f"≈ {approx_offset:+}"


def count_emojis(message: str) -> int:
    return emoji_count(message) + count_discord_emojis(message)


async def get_todays_record(session: AsyncSession, author_id: int) -> Record:
    today = datetime.now(UTC).date()
    stmt = select(Record).where(Record.member_id == author_id, Record.day == today)
    async with session.begin():
        record = (await session.execute(stmt)).scalars().first()
        if record is None:
            record = Record(member_id=author_id, no_messages=0, no_words=0, no_emojis=0, day=today)
    return record


async def get_records(session: AsyncSession) -> Sequence[Record]:
    async with session.begin():
        return (await session.execute(select(Record))).scalars().all()


async def get_todays_results(session: AsyncSession) -> list[Result]:
    today = datetime.now(UTC).date()
    thirty_days_ago = today - timedelta(days=30)
    return await get_results_for_time_period(session, start_date=thirty_days_ago, end_date=today)


async def get_results_for_time_period(
    session: AsyncSession, start_date: date, end_date: date
) -> list[Result]:
    msg_sum = func.sum(Record.no_messages)
    emoji_sum = func.sum(Record.no_emojis)
    stmt = (
        select(Record.member_id, msg_sum, emoji_sum)
        .where(Record.day >= start_date, Record.day < end_date)
        .group_by(Record.member_id)
    )
    async with session.begin():
        # TODO: do i need this .all() call?
        return [Result(*res) for res in (await session.execute(stmt)).all()]


class Powerrankings(commands.Cog):
    """
    UwU
    """

    class CacheEntry(NamedTuple):
        link: str
        validity_date: date

    def __init__(self, bot: Bot):
        self.bot = bot
        self.graph_link_cache: dict[str, Powerrankings.CacheEntry] = {}
        self.logger = logging.getLogger(__name__)

    def fetch_graph_link_from_cache(self, cache_key: str) -> str | None:
        cache_entry = self.graph_link_cache.get(cache_key, None)
        if cache_entry is None:
            return None
        if cache_entry.validity_date != datetime.now(UTC).date():
            return None
        return cache_entry.link

    def add_graph_link_to_cache(self, cache_key: str, graph_link: str) -> None:
        self.graph_link_cache[cache_key] = Powerrankings.CacheEntry(
            graph_link, datetime.now(UTC).date()
        )

    async def create_top10_sum_graph(self, ctx: commands.Context) -> str | None:
        async with (
            ctx.typing(),
            self.bot.aiohttp_session.post("http://127.0.0.1:8000/top10_sum_graph") as resp,
        ):
            if not resp.ok:
                error = await resp.text()
                await ctx.channel.send(f"⚠ Failed to create graph: `{error}`.")
                return None
            img = await resp.read()
            with io.BytesIO(img) as file:
                message = await ctx.channel.send(file=discord.File(file, "graph.png"))
        return message.attachments[0].url

    async def create_graph(self, ctx: commands.Context, args: Any) -> str | None:
        async with (
            ctx.typing(),
            self.bot.aiohttp_session.post("http://127.0.0.1:8000/graph", json=args) as resp,
        ):
            if not resp.ok:
                error = await resp.text()
                await ctx.channel.send(f"⚠ Failed to create graph: `{error}`.")
                return None
            img = await resp.read()
            with io.BytesIO(img) as file:
                message = await ctx.channel.send(file=discord.File(file, "graph.png"))
        return message.attachments[0].url

    @commands.group(aliases=["pwr"])
    async def powerrankings(self, ctx: commands.Context):
        """
        Group for commands related to powerrankings.
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @powerrankings.command(aliases=["lb", "sb", "leaderboard"])
    async def scoreboard(self, ctx: commands.Context):
        """
        Show the powerrankings scoreboard.
        """
        async with db.async_sessionmaker() as session:
            results = await get_todays_results(session)
        players = [
            (result.member_id, calculate_power(result.no_emojis, result.no_messages))
            for result in results
        ]
        players.sort(key=lambda e: e[1], reverse=True)
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
        scoreboard_data = [
            (str(i + 1), str(guild.get_member(p[0])), f"{p[1]:.02f}")
            for i, p in enumerate(players[:10])
        ]
        table = tabulate(scoreboard_data, headers=("Rank", "Name", "Power"))
        timestamp = discord.utils.format_dt(today_midnight_utc(), style="F")
        for page in format_table_message(table, f"Powerranking as of {timestamp}"):
            await ctx.send(page)

    @commands.is_owner()
    @powerrankings.command(name="now")
    async def now_scoreboard(self, ctx: commands.Context):
        today = datetime.now(UTC).date() + timedelta(days=1)
        thirty_days_ago = today - timedelta(days=30)
        async with db.async_sessionmaker() as session:
            results = await get_results_for_time_period(
                session, start_date=thirty_days_ago, end_date=today
            )
        players = [
            (result.member_id, calculate_power(result.no_emojis, result.no_messages))
            for result in results
        ]
        players.sort(key=lambda e: e[1], reverse=True)
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
        scoreboard_data = [
            (str(i + 1), str(guild.get_member(p[0])), f"{p[1]:.02f}")
            for i, p in enumerate(players[:10])
        ]
        table = tabulate(scoreboard_data, headers=("Rank", "Name", "Power"))
        timestamp = discord.utils.format_dt(today_midnight_utc() + timedelta(days=1), style="F")
        for page in format_table_message(table, f"Powerranking as of {timestamp}"):
            await ctx.send(page)

    @powerrankings.command()
    async def me(self, ctx: commands.Context):
        """
        Shows your own rank on the leaderboard, in addition to you direct surroundings and their
        difference in power compared to yours.
        """
        async with db.async_sessionmaker() as session:
            results = await get_todays_results(session)
        players = [
            (record.member_id, calculate_power(record.no_emojis, record.no_messages))
            for record in results
        ]
        players.sort(key=lambda e: e[1], reverse=True)
        ranks = {player[0]: i + 1 for i, player in enumerate(players)}
        author_position = [x[0] for x in players].index(ctx.author.id)
        author_score = players[author_position][1]
        surroundings = players[max(0, author_position - 2) : author_position + 3]
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
        scoreboard_data = [
            (str(ranks[p[0]]), str(guild.get_member(p[0])), offset_str(p[1] - author_score))
            for p in surroundings
        ]
        table = tabulate(scoreboard_data, headers=("Rank", "Name", "Power difference"))
        timestamp = discord.utils.format_dt(today_midnight_utc(), style="F")
        for page in format_table_message(
            table, f"{ctx.author.display_name}'s powerranking as of {timestamp}"
        ):
            await ctx.send(page)

    @powerrankings.command(name="statistics", aliases=["stats"])
    async def stats(self, ctx: commands.Context):
        """
        Shows statistics about the current state of the powerrankings, like the median or mean
        power.
        """
        async with db.async_sessionmaker() as session:
            results = await get_todays_results(session)
        powers = [calculate_power(record.no_emojis, record.no_messages) for record in results]
        filtered_powers = [p for p in powers if p > 0]
        timestamp = discord.utils.format_dt(today_midnight_utc(), style="F")
        return await ctx.send(
            f"Powerranking statistics as of {timestamp}:\n"
            "```\n"
            f"Min power:          {min(filtered_powers):.2f}\n"
            f"Max power:          {max(filtered_powers):.2f}\n"
            f"Mean power:         {statistics.mean(filtered_powers):.2f}\n"
            f"Median power:       {statistics.median(filtered_powers):.2f}\n"
            f"Standard deviation: {statistics.stdev(filtered_powers):.2f}\n"
            "```"
        )

    @powerrankings.command(name="top10sumgraph")
    @commands.is_owner()
    async def top10sumgraph_command(self, ctx: commands.Context):
        """
        Show the sum of the top 10 powers over time
        """
        cache_key = "top10sum"
        cached_graph_link = self.fetch_graph_link_from_cache(cache_key)
        if cached_graph_link is not None:
            await ctx.channel.send(cached_graph_link)
        else:
            graph_link = await self.create_top10_sum_graph(ctx)
            if graph_link is not None:
                self.add_graph_link_to_cache(cache_key, graph_link)

    @commands.guild_only()
    @powerrankings.command(name="graph")
    async def graph_cmd(self, ctx: GuildContext, *, flags: GraphFlags):
        """
        Show a graph of the powerrankings.

        # Flags
            limit: int
                The amount of players to show on the graph, must be between 1 and 20 (inclusive).
            start_date: date
                The start date in isoformat, has to be between 2022-10-08 and yesterday.
        # Example
            !powerrankings graph limit: 10 start_date: 2022-10-08
        """

        def is_valid_start_date(d: date) -> bool:
            return date(2022, 10, 8) <= d <= datetime.now(TIMEZONE) - timedelta(days=1)

        members = [
            {
                "name": member.display_name,
                "id": member.id,
                "color": f"#{random.randint(0, 255**3):06X}",
            }
            for member in ctx.guild.members
        ]
        args = {
            "user_list": members,
            "limit": flags.limit,
            "start_date": flags.start_date.isoformat()
            if is_valid_start_date(flags.start_date)
            else "2022-10-08",
        }
        cache_key = f'graph-{flags.limit}-{args["start_date"]}'
        cached_graph_link = self.fetch_graph_link_from_cache(cache_key)
        if cached_graph_link is not None:
            await ctx.channel.send(cached_graph_link)
        else:
            graph_link = await self.create_graph(ctx, args)
            if graph_link is not None:
                self.add_graph_link_to_cache(cache_key, graph_link)

    @me.error
    async def me_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        if isinstance(error, IndexError):
            await ctx.send("You are so weak, you don't even have a powerranking.")
            return
        print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        ctx = await self.bot.get_context(message)
        if (
            message.content.startswith("!")
            or ctx.author.bot
            or not ctx.guild
            or ctx.guild.id != self.bot.config.ids.guild_id
        ):
            return
        no_emojis = count_emojis(message.content)
        no_words = len(message.content.strip(" ").split(" "))
        async with db.async_sessionmaker() as session:
            record: Record = await get_todays_record(session, ctx.author.id)
            record.no_messages += 1
            record.no_words += no_words
            record.no_emojis += no_emojis
            record.day = datetime.now(UTC).date()
        async with db.async_sessionmaker() as session, session.begin():
            session.add(record)

    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        ctx = await self.bot.get_context(before)
        if (
            before.content.startswith("!")
            or ctx.author.bot
            or not ctx.guild
            or ctx.guild.id != self.bot.config.ids.guild_id
        ):
            return
        no_before_emojis = count_emojis(before.content)
        no_before_words = len(before.content.strip(" ").split(" "))
        no_after_emojis = count_emojis(after.content)
        no_after_words = len(after.content.strip(" ").split(" "))
        async with db.async_sessionmaker() as session:
            record = await get_todays_record(session, ctx.author.id)
            record.no_words += no_after_words - no_before_words
            record.no_emojis += no_after_emojis - no_before_emojis
        async with db.async_sessionmaker() as session, session.begin():
            session.add(record)


async def setup(bot: Bot):
    await db.create_schema(POWERRANKINGS_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(Powerrankings(bot))
