from __future__ import annotations

import enum
import logging
import random
import sys
import traceback
from typing import TYPE_CHECKING, NamedTuple, TypeVar

import discord
from discord.ext import commands
from sqlalchemy import Row, distinct, func, select
from tabulate import tabulate

import db
from db.jabomons import (
    JABOMONS_SCHEMA_NAME,
    Base,
    Event,
    EventType,
    Item,
    Jabkscore,
    Jabomon,
    Quality,
    Rarity,
    RLRank,
    User,
)
from utils import format_table_message

if TYPE_CHECKING:
    from collections.abc import Sequence

    from sqlalchemy.ext.asyncio import AsyncSession

    from bot import Bot
    from utils.context import GuildContext


def display_name_or_placeholder(
    member: discord.Member | None, placeholder: str = "Unknown Jabomon"
) -> str:
    if member is not None:
        return member.display_name
    return placeholder


def enum_displayname(elem: enum.Enum) -> str:
    return elem.name.replace("_", " ")


def enum_precedence(elem: enum.Enum) -> int:
    return list(elem.__class__.__members__.keys()).index(elem.name)


T = TypeVar("T", bound=enum.Enum)


def next_enum_variant(elem: T) -> T:
    prec = enum_precedence(elem)
    variants = list(elem.__class__.__members__.keys())
    if len(variants) <= prec:
        return elem
    return elem.__class__[variants[prec + 1]]


class TooFewJabgoodersError(Exception):
    def __init__(self, amount: int):
        self.amount = amount
        super().__init__(f"Only {amount} jabgooders available for combination or evolution.")


class MaxRarityReachedError(Exception):
    def __init__(self, jabomon_name: str):
        self.jabomon_name = jabomon_name
        super().__init__(f"The jabomon {jabomon_name} already has the highest rarity.")


class JabomonStat(NamedTuple):
    jabomon_id: int
    amount: int
    max_jabkscore: Jabkscore
    max_quality: Quality
    max_rl_rank: RLRank


class Jabomons(commands.Cog):
    """
    Roll the jabowheel to get your unique collection of jabomons
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.colors = {
            Rarity.COMMON: discord.Color.blurple(),
            Rarity.UNCOMMON: discord.Color.blue(),
            Rarity.RARE: discord.Color.yellow(),
            Rarity.EPIC: discord.Color.red(),
            Rarity.LEGENDARY: discord.Color.orange(),
            "Shiny": discord.Color.from_rgb(255, 255, 255),
        }

    async def get_user(self, session: AsyncSession, user_id: int) -> User | None:
        stmt = select(User).where(User.id == user_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def get_jabomon(
        self,
        session: AsyncSession,
        /,
        jabomon_id: int | None = None,
        rarity: Rarity | None = None,
    ) -> Sequence[Jabomon]:
        stmt = select(Jabomon)
        if rarity is not None:
            stmt = stmt.where(Jabomon.rarity == rarity)
        if jabomon_id is not None:
            stmt = stmt.where(Jabomon.id == jabomon_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_items(
        self, session: AsyncSession, /, inventory_id: int, jabomon_id: int | None = None
    ) -> list[Item]:
        stmt = select(Item).where(Item.inventory_id == inventory_id)
        if jabomon_id is not None:
            stmt = stmt.where(Item.jabomon_id == jabomon_id)
        async with session.begin():
            return list((await session.execute(stmt)).scalars().all())

    async def count_jabomons(self, session: AsyncSession, inventory_id: int) -> list[JabomonStat]:
        item_count = func.count(Item.jabomon_id)
        max_jabkscore = func.max(Item.jabkscore)
        max_quality = func.max(Item.quality)
        max_rl_rank = func.max(Item.rl_rank)
        stmt = (
            select(Item.jabomon_id, item_count, max_jabkscore, max_quality, max_rl_rank)
            .where(Item.inventory_id == inventory_id)
            .group_by(Item.jabomon_id)
            .order_by(item_count.desc())
        )
        async with session.begin():
            results = (await session.execute(stmt)).all()
        return [JabomonStat(*res) for res in results]

    async def maxed_jabomons(self, session: AsyncSession, inventory_id: int) -> Sequence[int]:
        stmt = (
            select(Item.jabomon_id)
            .where(
                Item.inventory_id == inventory_id,
                Item.jabkscore == Jabkscore.MAX,
                Item.rl_rank == RLRank.Super_Sonic_Legend,
                Item.quality == Quality.Factory_New,
            )
            .group_by(Item.jabomon_id)
        )
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_unique_rolls(
        self, session: AsyncSession, user_id: int, rarity: Rarity | None = None
    ) -> Sequence[Row[tuple[int, int]]]:
        stmt = (
            select(Event.jabomon_id, func.count(Event.jabomon_id))
            .where(Event.user_id == user_id, Event.type == EventType.ROLL)
            .group_by(Event.jabomon_id)
            .order_by(func.count(Event.jabomon_id).desc())
        )
        if rarity is not None:
            stmt = stmt.join(Jabomon, Event.jabomon_id == Jabomon.id).where(
                Jabomon.rarity == rarity
            )
        async with session.begin():
            return (await session.execute(stmt)).all()

    async def get_shinies(self, session: AsyncSession, user_id: int, rarity: Rarity | None) -> int:
        stmt = select(func.count(Item.id)).where(Item.inventory_id == user_id, Item.shiny == True)  # noqa: E712
        if rarity is not None:
            stmt = stmt.join(Jabomon, Item.jabomon_id == Jabomon.id).where(Jabomon.rarity == rarity)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()  # type: ignore func.count always returns an int

    async def get_jabodex_roll_leaderboard(
        self, session: AsyncSession
    ) -> Sequence[tuple[int, int]]:
        stmt = (
            select(Event.user_id, func.count(Event.user_id))
            .where(Event.type == EventType.ROLL)
            .group_by(Event.user_id)
            .order_by(func.count(Event.user_id).desc())
        )
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_unique_jabomons_leaderboard(
        self, session: AsyncSession
    ) -> Sequence[tuple[int, int]]:
        stmt = (
            select(Item.inventory_id, func.count(distinct(Item.jabomon_id)))
            .group_by(Item.inventory_id)
            .order_by(func.count(distinct(Item.jabomon_id)).desc())
        )
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def is_new_roll(self, session: AsyncSession, user_id: int, jabomon_id: int) -> bool:
        stmt = select(func.count(Item.id)).where(
            Item.inventory_id == user_id, Item.jabomon_id == jabomon_id
        )
        async with session.begin():
            return (await session.execute(stmt)).scalars().first() == 0

    async def count_jabgooders(self, session: AsyncSession, rarity: Rarity | None = None) -> int:
        stmt = select(func.count(Jabomon.id))
        if rarity is not None:
            stmt = stmt.where(Jabomon.rarity == rarity)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()  # type: ignore func.count always returns an int

    async def count_rolls(
        self,
        session: AsyncSession,
        /,
        roller: discord.Member | None = None,
        rolled: discord.Member | None = None,
    ) -> int:
        filters = []
        if roller is not None:
            filters.append(Event.user_id == roller.id)
        if rolled is not None:
            filters.append(Event.jabomon_id == rolled.id)
        stmt = select(func.count(Event.id)).where(*filters, Event.type == EventType.ROLL)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()  # type: ignore func.count always returns an int

    async def create_item(self, session: AsyncSession, rarity: Rarity, user_id: int) -> Item:
        rare_jabomons = await self.get_jabomon(session, rarity=rarity)
        jabomon = random.choice(rare_jabomons)
        shiny = random.random() < self.bot.config.shiny_chance
        jabkscore = random.choice(list(Jabkscore))
        quality = random.choice(list(Quality))
        rl_rank = random.choice(list(RLRank))
        return Item(
            inventory_id=user_id,
            jabomon_id=jabomon.id,
            shiny=shiny,
            jabkscore=jabkscore,
            quality=quality,
            rl_rank=rl_rank,
        )

    def get_max_items(self, items: list[Item]) -> tuple[Item, Item, Item]:
        max_cards = [
            item
            for item in items
            if item.quality == Quality.Factory_New
            and item.jabkscore == Jabkscore.MAX
            and item.rl_rank == RLRank.Super_Sonic_Legend
        ]
        if len(max_cards) > 0:
            max_jabkscore_item = max_quality_item = max_rl_rank_item = max_cards[0]
        else:
            max_jabkscore_item = max(items, key=lambda i: enum_precedence(i.jabkscore))
            max_quality_item = max(items, key=lambda i: i.quality)
            max_rl_rank_item = max(items, key=lambda i: enum_precedence(i.rl_rank))
        return max_jabkscore_item, max_quality_item, max_rl_rank_item

    def combine_items(self, items: list[Item], no_items: int) -> tuple[Item, set[Item]]:
        max_jabkscore_item, max_quality_item, max_rl_rank_item = self.get_max_items(items)
        items.remove(max_jabkscore_item)
        if max_quality_item in items:
            items.remove(max_quality_item)
        if max_rl_rank_item in items:
            items.remove(max_rl_rank_item)
        sacrifices = {max_jabkscore_item, max_quality_item, max_rl_rank_item}
        shiny_item = next((item for item in items if item.shiny), None)
        if shiny_item is not None:
            items.remove(shiny_item)
            sacrifices.add(shiny_item)
        sacrifices |= set(items[: no_items - len(sacrifices)])
        item = Item(
            inventory_id=items[0].inventory_id,
            jabomon_id=items[0].jabomon_id,
            shiny=shiny_item is not None,
            jabkscore=max_jabkscore_item.jabkscore,
            quality=max_quality_item.quality,
            rl_rank=max_rl_rank_item.rl_rank,
        )
        return item, sacrifices

    def create_event(self, item: Item, type_: EventType) -> Event:
        return Event(
            timestamp=db.datetime_now(),
            type=type_,
            user_id=item.inventory_id,
            jabomon_id=item.jabomon_id,
            shiny=item.shiny,
            jabkscore=item.jabkscore,
            quality=item.quality,
            rl_rank=item.rl_rank,
        )

    def roll_rarity(self) -> Rarity:
        probability = random.random()
        for rarity_idx, cutoff_proba in self.bot.config.rarities.items():  # noqa: B007 we use rarity_idx later
            if probability <= cutoff_proba:
                break
        return Rarity(rarity_idx)

    def rarity_color(self, rarity: Rarity | str, shiny: bool = False) -> discord.Color:
        if shiny:
            return self.colors["Shiny"]
        return self.colors[rarity]

    def create_jabomon_embed(
        self, rarity: Rarity, member: discord.Member | None, new: bool, item: Item
    ) -> discord.Embed:
        color = self.rarity_color(rarity, item.shiny)
        embed = (
            discord.Embed(color=color)
            .set_author(
                name=display_name_or_placeholder(member),
                icon_url=member.display_avatar.url
                if member is not None
                else self.bot.user.default_avatar.url,
            )
            .add_field(name="Rarity", value=rarity.name.capitalize())
            .add_field(name="Jabkscore", value=self.bot.get_emoji(item.jabkscore.value))
            .add_field(name="Quality", value=enum_displayname(item.quality))
            .add_field(name="RL Rank", value=enum_displayname(item.rl_rank))
        )
        if item.shiny:
            embed.add_field(name="Shiny?", value="SHINY!")
        if new:
            embed.title = "This is a new Jabgooder!"
        return embed

    def remove_valuable_items(self, items: list[Item]) -> list[Item]:
        max_iv_items = set(self.get_max_items(items))
        for item in max_iv_items:
            items.remove(item)
        return [item for item in items if not item.shiny]

    @commands.group(aliases=["jabo"], hidden=True)
    async def jabomons(self, ctx: commands.Context):
        """Group for activity commands"""
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @commands.cooldown(1, 30, type=commands.BucketType.user)
    @commands.guild_only()
    @jabomons.command()
    async def roll(self, ctx: commands.Context):
        """Roll a random Jabgooder"""
        if ctx.guild is None or ctx.guild.id != self.bot.config.ids.guild_id:
            return
        async with ctx.typing():
            rarity = self.roll_rarity()
            async with db.async_sessionmaker() as session:
                item = await self.create_item(session, rarity, ctx.author.id)
                new = await self.is_new_roll(
                    session, user_id=ctx.author.id, jabomon_id=item.jabomon_id
                )
                event = self.create_event(item, EventType.ROLL)
                async with session.begin():
                    session.add(item)
                    session.add(event)
            rolled_member = ctx.guild.get_member(item.jabomon_id)
        self.logger.info(
            "%s - %d rolled %s - %d as a %sshiny.",
            ctx.author.display_name,
            ctx.author.id,
            display_name_or_placeholder(rolled_member, str(item.jabomon_id)),
            item.jabomon_id,
            "" if item.shiny else "non",
        )

        embed = self.create_jabomon_embed(rarity, rolled_member, new, item)
        await ctx.send(f"{ctx.author.display_name} rolled:", embed=embed)

    @roll.error
    async def roll_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandOnCooldown):
            await ctx.send(f"You are on cooldown, try again in {int(error.retry_after)} seconds.")
        elif isinstance(error, commands.NoPrivateMessage):
            await ctx.send("This command can not be used in private messages.")
        else:
            print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.cooldown(1, 1, type=commands.BucketType.user)
    @commands.guild_only()
    @jabomons.command()
    async def evolve(self, ctx: GuildContext, *, jabomon: discord.Member):
        """
        Evolve 10 of the same jabomon to the next rarity.

        This will *not* take your highest IV jabomons, nor any shinies into account. Those can not
        be used for an evolution.

        Parameters
        ----------
        jabomon: Member
            The jabomon that you want to sacrifice for the evolution

        Example
        -------
        >>> !jabomon evolve Batman
        Evolves 10 Batman jabomons into a jabomon of the next rarity
        """
        required_jabomons_to_evolve = 10
        async with ctx.typing():
            member = jabomon
            async with db.async_sessionmaker() as session:
                real_jabomon = (await self.get_jabomon(session, jabomon_id=member.id))[0]
                if real_jabomon.rarity == Rarity.LEGENDARY:
                    raise MaxRarityReachedError(str(member))
                items = await self.get_items(
                    session, inventory_id=ctx.author.id, jabomon_id=real_jabomon.id
                )
                # TODO: allow overwriting this?
                items = self.remove_valuable_items(items)[:required_jabomons_to_evolve]
                if len(items) < required_jabomons_to_evolve:
                    raise TooFewJabgoodersError(len(items))
                next_rarity = next_enum_variant(real_jabomon.rarity)
                new_item = await self.create_item(session, next_rarity, ctx.author.id)
                new = await self.is_new_roll(
                    session, user_id=ctx.author.id, jabomon_id=new_item.jabomon_id
                )
                evolve_event = self.create_event(new_item, EventType.EVOLUTION)
                async with session.begin():
                    for item in items:
                        await session.delete(item)
                    session.add(new_item)
                    session.add(evolve_event)

            rolled_member = ctx.guild.get_member(new_item.jabomon_id)
        self.logger.info(
            "%s - %d evolved %s - %d as a %sshiny.",
            ctx.author.display_name,
            ctx.author.id,
            display_name_or_placeholder(rolled_member, str(new_item.jabomon_id)),
            new_item.jabomon_id,
            "" if new_item.shiny else "non",
        )
        embed = self.create_jabomon_embed(next_rarity, rolled_member, new, new_item)
        await ctx.send(f"{ctx.author.display_name} evolved a new:", embed=embed)

    @evolve.error
    async def evolve_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("You need to provide a jabomon that you want to evolve.")
        elif isinstance(error, commands.CommandOnCooldown):
            await ctx.send(f"You are on cooldown, try again in {int(error.retry_after)} seconds.")
        elif isinstance(error, commands.NoPrivateMessage):
            await ctx.send("This command can not be used in private messages.")
        elif isinstance(error, commands.MemberNotFound):
            await ctx.send(f"Sorry, i could not find the member `{error.argument}`")
        elif isinstance(error, commands.CommandInvokeError):
            error = error.original
            if isinstance(error, TooFewJabgoodersError):
                await ctx.send(
                    "Disregarding your highest IV jabomons, you only have "
                    f"{error.amount}/10 needed identical jabomons."
                )
            elif isinstance(error, MaxRarityReachedError):
                await ctx.send(
                    f"{error.jabomon_name} is already the highest rarity and can't be "
                    "evolved further."
                )
        else:
            print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.cooldown(1, 1, type=commands.BucketType.user)
    @commands.guild_only()
    @jabomons.command()
    async def combine(self, ctx: GuildContext, *, jabomon: discord.Member):
        """
        Combine 10 of the same jabomon to receive one of the same jabomon with their combined
        highest IVs.

        This will prioritise shinies and high IV jabomons. Double shinies might get combined.

        Parameters
        ----------
        jabomon: Member
            The jabomon that you want to combine

        Example
        -------
        !jabomon combine Batman
        >>> Combindes 10 Batman jabomons and creates a new one with their combined IVs
        """
        required_jabomons_to_combine = 10
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                rarity = (await self.get_jabomon(session, jabomon_id=jabomon.id))[0].rarity
                items = await self.get_items(
                    session, inventory_id=ctx.author.id, jabomon_id=jabomon.id
                )
                if len(items) < required_jabomons_to_combine:
                    raise TooFewJabgoodersError(len(items))
                new_item, sacrifices = self.combine_items(list(items), required_jabomons_to_combine)
                combine_event = self.create_event(new_item, EventType.COMBINATION)
                new = await self.is_new_roll(
                    session, user_id=ctx.author.id, jabomon_id=new_item.jabomon_id
                )
                async with session.begin():
                    for item in sacrifices:
                        await session.delete(item)
                    session.add(new_item)
                    session.add(combine_event)
            rolled_member = ctx.guild.get_member(new_item.jabomon_id)
        self.logger.info(
            "%s - %d combined %s - %d as a %sshiny.",
            ctx.author.display_name,
            ctx.author.id,
            display_name_or_placeholder(rolled_member, str(new_item.jabomon_id)),
            new_item.jabomon_id,
            "" if new_item.shiny else "non",
        )
        embed = self.create_jabomon_embed(rarity, rolled_member, new, new_item)
        await ctx.send(f"{ctx.author.display_name} combined a new:", embed=embed)

    @combine.error
    async def combine_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send("You need to provide a jabomon that you want to combine.")
        elif isinstance(error, commands.CommandOnCooldown):
            await ctx.send(f"You are on cooldown, try again in {int(error.retry_after)} seconds.")
        elif isinstance(error, commands.NoPrivateMessage):
            await ctx.send("This command can not be used in private messages.")
        elif isinstance(error, commands.MemberNotFound):
            await ctx.send(f"Sorry, i could not find the member `{error.argument}`")
        elif isinstance(error, commands.CommandInvokeError):
            error = error.original
            if isinstance(error, TooFewJabgoodersError):
                await ctx.send(
                    "Disregarding your highest IV jabomons, you only have "
                    f"{error.amount}/10 needed identical jabomons."
                )
            else:
                print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
                traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)
        else:
            print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @jabomons.command(aliases=["show", "jabodex"])
    async def dex(self, ctx: commands.Context, rarity: Rarity | None = None):
        """
        Show your own jabodex.

        Parameters
        ----------
        rarity: Optional[Rarity]
            Restrict the jabodex to only show jabgooders of the given rarity.
            Must be one of Common, Uncommon, Rare, Epic or Legendary
        """
        async with ctx.typing(), db.async_sessionmaker() as session:
            rolls = await self.get_unique_rolls(session, ctx.author.id, rarity)
            shinies = await self.get_shinies(session, ctx.author.id, rarity)
            jabgooders = await self.count_jabgooders(session, rarity)

        color = ctx.author.color if rarity is None else self.rarity_color(rarity)
        embed = discord.Embed(
            color=color,
            title=f'Jabodex{" " + rarity.name.capitalize() if rarity is not None else ""}',
        )
        embed.set_author(name=ctx.author.display_name, icon_url=ctx.author.display_avatar)
        embed.add_field(name="Unique Jabgooders", value=f"{len(rolls)}/{jabgooders}")
        embed.add_field(name="Shinies", value=shinies)
        await ctx.send(embed=embed)

    @commands.guild_only()
    @jabomons.command(aliases=["inv"])
    async def inventory(self, ctx: GuildContext):
        """
        Show a more detailed view of your inventory
        """
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                jabo_stats = await self.count_jabomons(session, inventory_id=ctx.author.id)
                maxed_jabomons = await self.maxed_jabomons(session, inventory_id=ctx.author.id)
            rows = [
                (
                    display_name_or_placeholder(
                        ctx.guild.get_member(jabomon.jabomon_id), str(jabomon.jabomon_id)
                    ),
                    str(jabomon.amount),
                    # TODO: this will die if we ever remove an emote that is used for a rarity
                    self.bot.get_emoji(jabomon.max_jabkscore.value).name,  # type: ignore
                    enum_displayname(jabomon.max_quality),
                    enum_displayname(jabomon.max_rl_rank),
                    str(jabomon.jabomon_id in maxed_jabomons),
                )
                for jabomon in jabo_stats
            ]
        table = tabulate(
            rows,
            headers=(
                "Jabomon",
                "Amount",
                "Max Jabkscore",
                "Max Quality",
                "Max Rl Rank",
                "Maxed out Jabomon?",
            ),
        )
        for page in format_table_message(table, f"Inventory for {ctx.author.display_name}"):
            await ctx.send(page)

    @commands.guild_only()
    @jabomons.command()
    async def rolls(self, ctx: GuildContext, rarity: Rarity | None):
        """
        Show a list of all the jabgooders you have rolled and how often you rolled them.

        Parameters
        ----------
        rarity: Optional[Rarity]
            Restrict the list to only show jabgooders of the given rarity.
            Must be one of Common, Uncommon, Rare, Epic or Legendary
        """
        # TODO: implement this to show items instead of rolls?
        # prolly wanna think about when to show which stats, i guess this can go but rolls should
        # stay
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                rolls = await self.get_unique_rolls(session, ctx.author.id, rarity)
            members = [
                (display_name_or_placeholder(ctx.guild.get_member(rid), str(rid)), str(count))
                for rid, count in rolls
            ]
            table = tabulate(members, headers=("Jabgooder", "Count"))
        for page in format_table_message(table, f"Jabodex list for {ctx.author.display_name}"):
            await ctx.send(page)

    @commands.guild_only()
    @jabomons.command(aliases=["lb", "sb", "scoreboard"])
    async def leaderboard(self, ctx: GuildContext):
        """Show a leaderboard of users who have rolled the most."""
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                rolls = (await self.get_jabodex_roll_leaderboard(session))[:15]
            members = [
                (display_name_or_placeholder(ctx.guild.get_member(rid), str(rid)), str(count))
                for rid, count in rolls
            ]
            table = tabulate(members, headers=("Member", "Rolls"))
        for page in format_table_message(
            table, f"Leaderboard of most rolls: {ctx.author.display_name}"
        ):
            await ctx.send(page)

    @commands.guild_only()
    @commands.is_owner()
    @jabomons.command()
    async def maxdex(self, ctx: GuildContext):
        """Show a leaderboard of people with the most unique jabomons."""
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                jabodexes = (await self.get_unique_jabomons_leaderboard(session))[:15]
            members = [
                (display_name_or_placeholder(ctx.guild.get_member(rid), str(rid)), count)
                for rid, count in jabodexes
            ]
            table = tabulate(members, headers=("Member", "Unique Jabomons"))
        for page in format_table_message(
            table, f"Leaderboard of most unique jabomons: {ctx.author.display_name}"
        ):
            await ctx.send(page)

    @commands.guild_only()
    @commands.is_owner()
    @jabomons.command()
    async def add(self, ctx: commands.Context, member: discord.Member):
        async with db.async_sessionmaker() as session, session.begin():
            session.add(Jabomon(id=member.id, rarity=Rarity.LEGENDARY.value))
            session.add(User(id=member.id))
        self.logger.info(
            "Manually added new legendary jabomon and user for %s - %d.", str(member), member.id
        )
        await ctx.send(f"Added new legendary jabomon and user for {member}.")

    @commands.guild_only()
    @jabomons.command()
    async def stats(
        self,
        ctx: commands.Context,
        roller: discord.Member | None,
        rolled: discord.Member | None,
    ):
        """
        Show the amount of times that the roulette has been rolled.

        Parameters
        ----------
        roller: Optional[Member]
            Restrict the count to only rolls *by* the roller.

        rolled: Optional[Member]
            Restrict the count to only rolls where the rolled was rolled.
        """
        async with ctx.typing(), db.async_sessionmaker() as session:
            cnt = await self.count_rolls(session, roller=roller, rolled=rolled)
        track_start = "<t:1641652925:F>"
        if roller is None and rolled is None:
            return await ctx.send(f"There have been `{cnt}` tracked rolls since {track_start}.")
        if rolled is None:
            return await ctx.send(
                f"**{display_name_or_placeholder(roller)}** has `{cnt}` tracked rolls since "
                f"{track_start}."
            )
        if roller is None:
            return await ctx.send(
                f"**{rolled.display_name}** has been rolled `{cnt}` times since {track_start}."
            )
        return await ctx.send(
            f"**{roller.display_name}** has rolled **{rolled.display_name}** "
            f"`{cnt}` times since {track_start}."
        )

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        if member.guild.id != self.bot.config.ids.guild_id:
            return
        async with db.async_sessionmaker() as session, session.begin():
            session.add(Jabomon(id=member.id, rarity=Rarity.LEGENDARY.value))
            session.add(User(id=member.id))
        self.logger.info(
            "%s - %d just joined, created new legendary jabomon and user.", str(member), member.id
        )


async def setup(bot: Bot):
    await db.create_schema(JABOMONS_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(Jabomons(bot))
