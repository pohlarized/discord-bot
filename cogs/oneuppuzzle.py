from __future__ import annotations

import datetime
import logging
import re
import sys
import traceback
from collections import defaultdict
from io import BytesIO
from typing import TYPE_CHECKING

import discord
import pytesseract
from discord.ext import commands
from oneuppuzzle_solver import Game
from PIL import Image
from pydantic import BaseModel, Field
from sqlalchemy import select
from tabulate import tabulate

import db
from db.oneuppuzzle import ONEUPPUZZLE_SCHEMA_NAME, Base, CacheUpdate, Puzzle, Result
from utils import format_table_message
from utils.errors import KnownGuildNotFoundError
from utils.numbers import format_timedelta

if TYPE_CHECKING:
    from collections.abc import Sequence

    from sqlalchemy.ext.asyncio import AsyncSession

    from bot import Bot
    from utils.context import GuildContext

ONEUPPUZZLE_PUZZLE_NUMBER_REGEX = r"#(-?\d+)"
ONEUPPUZZLE_HOUR_REGEX = r"(\d+)\s*hour"
ONEUPPUZZLE_MINUTE_REGEX = r"(\d+)\s*minute"
ONEUPPUZZLE_SECOND_REGEX = r"(\d+)\s*second"


def extract_time_value(matches: list[str]) -> int:
    """
    Given a list of regular expression matches that may or may not be present, this either returns
    the matched time as an int, or returns the default 0.

    Params
    ------
    matches: list[str]
        The re.findall matches lsit

    Returns
    -------
    int
        The parsed number, or 0 as default.
    """
    return int(matches[0]) if matches else 0


class FirebaseStringValue(BaseModel):
    string_value: str = Field(alias="stringValue")


class PuzzleFields(BaseModel):
    data: FirebaseStringValue
    order: FirebaseStringValue


class PuzzleDocument(BaseModel):
    name: str
    fields: PuzzleFields
    create_time: datetime.datetime = Field(alias="createTime")
    update_time: datetime.datetime = Field(alias="updateTime")

    def into_puzzle(self) -> Puzzle:
        return Puzzle(
            id=self.name.split("/")[-1],
            number=int(self.fields.order.string_value),
            data=self.fields.data.string_value,
            create_time=self.create_time.replace(tzinfo=None),
            update_time=self.update_time.replace(tzinfo=None),
        )


class FirebaseResponse(BaseModel):
    next_page_token: str | None = Field(default=None, alias="nextPageToken")
    documents: list[PuzzleDocument] | None = Field(default=None)


class OneuppuzzleApi:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.api_key = bot.config.oup_api_key
        self.collection_id = bot.config.oup_collection_id
        self.project_id = bot.config.oup_project_id
        self.firestore_api_url = bot.config.oup_firestore_api_url

    async def make_request(self, endpoint: str, params: dict) -> FirebaseResponse:
        async with self.bot.aiohttp_session.get(
            f"{self.firestore_api_url}/{endpoint}", params=params
        ) as response:
            return FirebaseResponse(**(await response.json()))

    async def get_puzzles(self) -> list[Puzzle]:
        endpoint = f"projects/{self.project_id}/databases/(default)/documents/{self.collection_id}"
        params = {"key": self.api_key, "pageSize": 500}
        puzzle_documents = []
        response = await self.make_request(endpoint, params)
        if response.documents is not None:
            puzzle_documents += response.documents
        while response.next_page_token is not None:
            params["pageToken"] = response.next_page_token
            response = await self.make_request(endpoint, params)
            if response.documents is not None:
                puzzle_documents += response.documents
        return [puzzle_document.into_puzzle() for puzzle_document in puzzle_documents]


class OneuppuzzleCache:
    def __init__(self, bot: Bot):
        self.api = OneuppuzzleApi(bot)
        self.update_interval = datetime.timedelta(days=1)

    async def _update_cache_date(self, session: AsyncSession, date: datetime.datetime):
        async with session.begin():
            latest_update = (await session.execute(select(CacheUpdate))).scalars().first()
            if latest_update is None:
                latest_update = CacheUpdate(date=date)
            else:
                latest_update.date = date
            session.add(latest_update)

    async def _latest_update(self, session: AsyncSession) -> datetime.datetime:
        async with session.begin():
            latest_update = (await session.execute(select(CacheUpdate))).scalars().first()
            if latest_update is None:
                return datetime.datetime(1970, 1, 1, tzinfo=None)  # noqa: DTZ001 we want a naive dt here for the DB
            return latest_update.date

    async def _update_puzzle(self, session: AsyncSession, puzzle: Puzzle):
        stmt = select(Puzzle).where(Puzzle.number == puzzle.number)
        async with session.begin():
            cached_puzzle = (await session.execute(stmt)).scalars().first()
        if cached_puzzle is None:
            async with session.begin():
                session.add(puzzle)
        elif cached_puzzle.update_time < puzzle.update_time:
            async with session.begin():
                cached_puzzle.data = puzzle.data
                cached_puzzle.id = puzzle.id
                cached_puzzle.update_time = puzzle.update_time
                session.add(cached_puzzle)

    async def refresh_puzzles(self, session: AsyncSession):
        puzzles = await self.api.get_puzzles()
        for puzzle in puzzles:
            await self._update_puzzle(session, puzzle)
        await self._update_cache_date(session, datetime.datetime.now(tz=None))  # noqa: DTZ005 we want a naive dt here for the DB

    async def _puzzle_from_db(self, session: AsyncSession, number: int) -> Puzzle | None:
        stmt = select(Puzzle).where(Puzzle.number == number)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def puzzle(self, session: AsyncSession, number: int) -> Puzzle | None:
        if (
            datetime.datetime.now(tz=None) - (await self._latest_update(session))  # noqa: DTZ005 we want a naive dt here for the DB
        ) >= self.update_interval:
            await self.refresh_puzzles(session)
        return await self._puzzle_from_db(session, number)


class OneuppuzzleCog(commands.Cog):
    """
    Cog for automation of the oneuppuzzle joustle
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.oneuppuzzle_cache = OneuppuzzleCache(bot)
        self.logger = logging.getLogger(__name__)

    async def results_for_game(self, session: AsyncSession, game_id: int) -> Sequence[Result]:
        stmt = select(Result).where(Result.game_id == game_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def results_for_games(
        self, session: AsyncSession, min_game_id: int, max_game_id: int
    ) -> Sequence[Result]:
        stmt = select(Result).where(Result.game_id >= min_game_id, Result.game_id <= max_game_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def results_for_user(self, session: AsyncSession, user_id: int) -> Sequence[Result]:
        stmt = select(Result).where(Result.user_id == user_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_result_from_db(
        self, session: AsyncSession, user_id: int, game_id: int
    ) -> Result | None:
        stmt = select(Result).where(Result.user_id == user_id, Result.game_id == game_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def add_result_to_db(
        self, session: AsyncSession, user_id: int, game_id: int, time_seconds: int
    ) -> Result:
        result = await self.get_result_from_db(session, user_id, game_id)
        async with session.begin():
            if result is None:
                result = Result(user_id=user_id, game_id=game_id, time_seconds=time_seconds)
            else:
                result.time_seconds = time_seconds
            session.add(result)
        return result

    @commands.group(aliases=["oup"])
    async def oneuppuzzle(self, ctx: commands.Context):
        """Group for oneuppuzzle commands"""
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @oneuppuzzle.command(aliases=["l"])
    async def link(self, ctx: commands.Context, number: int):
        """
        Provide the link for the oneuppuzzle with the given number
        """
        await ctx.send("Due to changes by the author, oneuppuzzle links are no longer available.")
        return
        async with ctx.typing(), db.async_sessionmaker() as session:
            puzzle = await self.oneuppuzzle_cache.puzzle(session, number)
        if puzzle is None:
            await ctx.send("Could not find puzzle with the given number.")
        else:
            await ctx.send(f"<{puzzle.link}>")

    @oneuppuzzle.command()
    async def solve(self, ctx: commands.Context, number: int):
        """
        Get the solution to a specific oneuppuzzle.
        """
        await ctx.send(
            "Due to changes by the author, oneuppuzzle solutions are no longer available."
        )
        return
        async with ctx.typing(), db.async_sessionmaker() as session:
            puzzle = await self.oneuppuzzle_cache.puzzle(session, number)
        if puzzle is None:
            await ctx.send(f"Could not find puzzle with number {number}.")
            return
        game = Game(puzzle.data)
        if not game.solve():
            game.solve_with_guesses()
        await ctx.send(f"Solution to oneuppuzzle {number}:\n||```\n{game}\n```||")

    @oneuppuzzle.command(aliases=["r"])
    async def results(self, ctx: commands.Context):
        """
        Show a list of your own oneuppuzzle results.
        """
        async with db.async_sessionmaker() as session:
            results = await self.results_for_user(session, ctx.author.id)
        headers = ("Game ID", "Time")
        rows = sorted(
            [
                (result.game_id, format_timedelta(datetime.timedelta(seconds=result.time_seconds)))
                for result in results
            ],
            key=lambda x: x[0],
        )
        table = tabulate(rows, headers=headers)
        for message in format_table_message(table, f"Results for oneuppuzzle player {ctx.author}"):
            await ctx.send(message)

    @oneuppuzzle.command(aliases=["g", "p", "game"])
    async def puzzle(self, ctx: commands.Context, puzzle_number: int):
        """
        Show a scoreboard of people who submitted a result for a given puzzle.

        Params
        ------
        puzzle_number: int
            The number of the puzzle you want to look up
        """
        username_for_id_map = {}
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)

        def username_for_id(user_id: int) -> str:
            if user_id not in username_for_id_map:
                member = guild.get_member(user_id)
                username_for_id_map[user_id] = str(member)
            return username_for_id_map[user_id]

        async with db.async_sessionmaker() as session:
            results = await self.results_for_game(session, puzzle_number)
        headers = ("User", "Time")
        rows = [
            (
                username_for_id(result.user_id),
                format_timedelta(datetime.timedelta(seconds=result.time_seconds)),
            )
            for result in sorted(results, key=lambda result: result.time_seconds)
        ]
        table = tabulate(rows, headers=headers)
        for message in format_table_message(table, f"Results for oneuppuzzle #{puzzle_number}"):
            await ctx.send(message)

    async def submit_attachment(
        self,
        attachment: discord.Attachment,
        author: discord.User | discord.Member,
    ) -> Result | None:
        if attachment.content_type is None or not attachment.content_type.startswith("image"):
            return None
        content = BytesIO(await attachment.read())
        text = pytesseract.image_to_string(Image.open(content))
        puzzle_no = int(re.findall(ONEUPPUZZLE_PUZZLE_NUMBER_REGEX, text)[0])
        hour_matches = re.findall(ONEUPPUZZLE_HOUR_REGEX, text)
        minute_matches = re.findall(ONEUPPUZZLE_MINUTE_REGEX, text)
        second_matches = re.findall(ONEUPPUZZLE_SECOND_REGEX, text)
        hours = extract_time_value(hour_matches)
        minutes = extract_time_value(minute_matches)
        seconds = extract_time_value(second_matches)
        total_seconds = 60 * 60 * hours + 60 * minutes + seconds
        if total_seconds == 0:
            return None
        async with db.async_sessionmaker() as session:
            return await self.add_result_to_db(session, author.id, puzzle_no, total_seconds)

    def create_result_embed(
        self, result: Result, author: discord.User | discord.Member
    ) -> discord.Embed:
        embed = discord.Embed(color=author.color, title=f"Oneuppuzzle #{result.game_id}")
        embed.set_author(name=str(author), icon_url=author.display_avatar.url)
        embed.description = format_timedelta(datetime.timedelta(seconds=result.time_seconds))
        return embed

    @commands.guild_only()
    @commands.is_owner()
    @oneuppuzzle.command(aliases=["s"])
    async def submit(self, ctx: GuildContext):
        """
        Submit your results.
        This can either be done by attaching a screenshot (or screenshots) to the message executing
        this command, or by responding to a message containing screenshots as an attachment.
        Only attachments count, posting a link to a screenshot (also in the message you may be
        responding to) is not counted by the bot.

        If result parsing is successful, an embed with the score is sent.
        """
        has_sent = False
        message = ctx.message
        if not message.attachments:
            if ctx.message.reference and ctx.message.reference.message_id:
                message = ctx.message.reference.cached_message
                if not message:
                    message = await ctx.channel.fetch_message(ctx.message.reference.message_id)
            else:
                await ctx.send(
                    "Missing attached screenshot, or reply to message with attached screenshot."
                )
                return
        for attachment in message.attachments:
            result = await self.submit_attachment(attachment, message.author)
            if result:
                has_sent = True
                await ctx.send(embed=self.create_result_embed(result, message.author))
        if not has_sent:
            await ctx.send("Could not extract any data from the provided content.")
            return

    async def get_username(self, discord_id: int) -> str:
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
        user = guild.get_member(discord_id)
        if user is None:
            user = self.bot.get_user(discord_id)
        if user is None:
            return f"Unknown user {discord_id}"
        return f"{user.display_name}"

    @oneuppuzzle.command(hidden=True)
    async def score(
        self, ctx: commands.Context, min_puzzle_no: int, max_puzzle_no: int, min_players: int = 3
    ):
        """
        Show scores for select range of oneuppuzzles.

        For a single game, the last place gets a single point, the second-to-last place gets two
        points and so on.
        Only games with at least {min_players} players are accounted for.
        When times are tied, each player is given the *highest* number of points for that time.

        Parameters
        ----------
        min_puzzle_no: int
            The lower bound to include.

        max_puzzle_no: int
            The upper bound to include.

        min_players: int
            Minimum amount of players to count a game.
        """
        async with db.async_sessionmaker() as session:
            results = await self.results_for_games(session, min_puzzle_no, max_puzzle_no)
        times_per_game = defaultdict(list)
        for result in results:
            times_per_game[result.game_id].append(result.time_seconds)
        for times in times_per_game.values():
            times.sort()
        scores_per_player = defaultdict(list)
        for result in results:
            game_times = times_per_game[result.game_id]
            no_players = len(game_times)
            if no_players >= min_players:
                scores_per_player[result.user_id].append(
                    no_players - game_times.index(result.time_seconds)
                )
        leaderboard = sorted(
            [
                ((await self.get_username(user_id)), sum(scores))
                for user_id, scores in scores_per_player.items()
            ],
            key=lambda x: x[1],
            reverse=True,
        )
        table = tabulate(leaderboard, headers=("Name", "Score"))
        for message in format_table_message(
            table,
            f"Results for oneuppuzzles {min_puzzle_no} through {max_puzzle_no}",
            f"Minimum amount of players per game: `{min_players}`",
        ):
            await ctx.send(message)

    @submit.error
    async def oneuppuzzle_handler(self, ctx: commands.Context, error: BaseException):
        print("Invoking handler")
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        if isinstance(error, ValueError):
            await ctx.send("Failed to parse time from screenshot.")
        elif isinstance(error, IndexError):
            await ctx.send("Failed to parse game ID from screenshot.")
        elif isinstance(error, commands.NoPrivateMessage):
            await ctx.send("This command may not be used in DMs.")
        else:
            print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.channel.id == self.bot.config.ids.oneuppuzzle_channel:
            for attachment in message.attachments:
                try:
                    result = await self.submit_attachment(attachment, message.author)
                    if result:
                        self.logger.info(
                            "%s has submitted a result for oneuppuzzle %i",
                            message.author,
                            result.game_id,
                        )
                        await message.channel.send(
                            embed=self.create_result_embed(result, message.author)
                        )
                except ValueError:
                    self.logger.warning(
                        "Failed to parse oneuppuzzle message with id %d", message.id
                    )


async def setup(bot: Bot):
    await db.create_schema(ONEUPPUZZLE_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(OneuppuzzleCog(bot))
