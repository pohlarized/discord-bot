import logging
import re
from collections.abc import Sequence

import discord
from discord.ext import commands
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.commands import CUSTOM_COMMANDS_SCHEMA_NAME, Base, Command
from db.discord_rewind import CommandExecuted, CommandType
from utils.context import GuildContext

MAX_COMMAND_NAME_LENGTH = 32
MAX_COMMAND_TEXT_LENGTH = 1200


class CustomCommandsCogs(commands.Cog):
    """
    Cog for custom commands

    Allows users with one of the permitted_roles to
    create AND delete custom commands. The bot does
    not keep track of who created a command, so anyone
    with the role(s) can potentially delete anyones
    commands.

    A custom commands is just a simple text response to a
    message starting with '!'.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.permitted_roles = [self.bot.config.ids.knight_role_id]  # list of role ids
        self.excluded_users = [self.bot.config.ids.no_command_user_id]

    async def cog_unload(self):
        self.logger.debug("Closing db session...")

    # check
    def has_permissions(self, ctx: GuildContext) -> bool:
        """
        Check whether the author has permission to interact with custom
        commands

        Parameters
        ----------
        author : discord.Member()
            the author of the message
        """
        if set(self.permitted_roles).intersection({role.id for role in ctx.author.roles}):
            return ctx.author.id not in self.excluded_users
        return False

    async def add_command_executed(
        self,
        session: AsyncSession,
        message: discord.Message,
        command_name: str,
        command_type: CommandType,
    ) -> None:
        cmd_exec = CommandExecuted(
            channel_id=message.channel.id,
            author_id=message.author.id,
            message_id=message.id,
            command_name=command_name,
            success=True,
            type=command_type,
            timestamp=message.created_at.replace(tzinfo=None),
        )
        async with session.begin():
            session.add(cmd_exec)

    async def add_command(self, session: AsyncSession, name: str, content: str) -> None:
        cmd = Command(name=name, content=content)
        async with session.begin():
            session.add(cmd)

    async def get_command(self, session: AsyncSession, name: str) -> Command | None:
        stmt = select(Command).where(Command.name == name)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def del_command(self, session: AsyncSession, name: str) -> None:
        cmd = await self.get_command(session, name)
        async with session.begin():
            await session.delete(cmd)

    async def edit_command(self, session: AsyncSession, name: str, content: str) -> bool:
        cmd = await self.get_command(session, name)
        if cmd is None:
            return False
        async with session.begin():
            cmd.content = content
            session.add(cmd)
        return True

    async def get_commands_containing(self, session: AsyncSession, text: str) -> Sequence[Command]:
        stmt = select(Command).where(Command.name.contains(text))
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_all_commands(self, session: AsyncSession) -> Sequence[Command]:
        async with session.begin():
            return (await session.execute(select(Command))).scalars().all()

    @commands.guild_only()
    @commands.group(name="commands", aliases=["command"])
    async def custom_commands(self, ctx: GuildContext):
        """
        Group for command commands (xD)
        Can only be used by knights except scheist.
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @commands.guild_only()
    @custom_commands.command(name="add")
    async def commands_add(self, ctx: GuildContext, comm: str, *, text: str):
        """
        Add a custom command.

        Parameters
        ----------
        comm : str
            the name of the command you want to add
        text : str
            the text the command should return

        Example
        -------
        >>> !commands add dexzy @dexzy
        Creates a command named "dexzy" which returns the text "@dexzy".
        That command can be used by typing "!dexzy"
        """
        if not self.has_permissions(ctx):
            return await ctx.send("Only proper Knights are allowed to add commands.")
        async with db.async_sessionmaker() as session:
            if not re.match(r"^[A-Za-z0-9]+[A-Za-z0-9_]*[A-Za-z0-9]+$", comm):
                return await ctx.send(
                    "You're only allowed to use **letters**, "
                    "**numbers** and **underscores** in your "
                    "command.\nYour command can not start or "
                    "end with an underscore."
                )
            if len(comm) > MAX_COMMAND_NAME_LENGTH:
                return await ctx.send(
                    f"The maximum command length is **{MAX_COMMAND_NAME_LENGTH} characters**."
                )
            if len(text) > MAX_COMMAND_TEXT_LENGTH:
                return await ctx.send(
                    f"The maximum text length for custom commands is **{MAX_COMMAND_TEXT_LENGTH}"
                    f"characters**."
                )
            if (await self.get_command(session, comm)) is not None:
                return await ctx.send(f"The command `{comm}` already exists.")
            if comm in [command.name for command in self.bot.commands]:
                return await ctx.send(f"You can't overwrite the hardcoded command `{comm}`.")
            await self.add_command(session, comm, text)
        self.logger.info(
            "Command '%s' added to server %s - %d by %s - %d",
            comm,
            ctx.guild.name,
            ctx.guild.id,
            ctx.author,
            ctx.author.id,
        )
        return await ctx.send(f"Successfully added the command `{comm}`.")

    @commands.guild_only()
    @custom_commands.command(name="remove", aliases=["delete", "del"])
    async def commands_remove(self, ctx: GuildContext, comm: str):
        """
        Remove a custom command.

        Parameters
        ----------
        comm : str
            the name of the command you want to remove

        Example
        -------
        >>> !commands remove dexzy
        Removes the custom command named "dexzy"
        """
        if ctx.guild is None:
            raise commands.NoPrivateMessage
        if not self.has_permissions(ctx):
            return await ctx.send("Only proper Knights are allowed to remove commands.")
        async with db.async_sessionmaker() as session:
            if (await self.get_command(session, comm)) is None:
                await ctx.send_help(ctx.command)
                return await ctx.send(f"Couldn't find the custom command `{comm}`.")
            await self.del_command(session, comm)
        self.logger.info(
            "Command '%s' removed from server %s - %d by %s - %d",
            comm,
            ctx.guild.name,
            ctx.guild.id,
            ctx.author,
            ctx.author.id,
        )
        return await ctx.send(f"Successfully deleted the command `{comm}`.")

    @commands.guild_only()
    @custom_commands.command(name="edit")
    async def commands_edit(self, ctx: GuildContext, comm: str, *, text: str):
        """
        Edit an existing custom command
        Parameters
        ----------
        comm : str
            the name of the command you want to edit
        text : str
            the new text that the command should return

        Example
        -------
        >>> !commands edit dexzy haHAA
        Makes the existing command "dexzy" return the text "haHAA"
        """
        async with db.async_sessionmaker() as session:
            if not self.has_permissions(ctx):
                return await ctx.send("Only proper Knights are allowed to add commands.")
            if len(text) > MAX_COMMAND_TEXT_LENGTH:
                await ctx.send_help(ctx.command)
                return await ctx.send(
                    f"The maximum text length for a custom command is **{MAX_COMMAND_TEXT_LENGTH}"
                    f"characters**."
                )
            if not await self.edit_command(session, comm, text):
                return await ctx.send(f"Couln't find the custom command {comm}")
        self.logger.info(
            "Command '%s' edited in server %s - %d by %s - %d.",
            comm,
            ctx.guild.name,
            ctx.guild.id,
            ctx.author,
            ctx.author.id,
        )
        return await ctx.send(f"Successfully edited the command `{comm}`")

    @commands.guild_only()
    @custom_commands.command(name="list")
    async def commands_list(self, ctx: GuildContext):
        """
        Show a list of all custom commands
        """
        async with db.async_sessionmaker() as session:
            all_custom_commands = await self.get_all_commands(session)
        if len(all_custom_commands) == 0:
            return await ctx.send("There are no custom commands yet.")
        answer = "```\n"
        answer += "\n".join(c.name for c in all_custom_commands)
        answer += "```"
        return await ctx.send(answer)

    @commands.guild_only()
    @custom_commands.command(name="search")
    async def commands_search(self, ctx: GuildContext, text: str):
        """
        Show a list of commands that contain some text

        Parameters
        ----------
        text : str
            the text that you want to search for

        Example
        -------
        >>> !commands search dex
        Returns a list of custom commands containing 'dex'
        """
        async with db.async_sessionmaker() as session:
            matching_commands = await self.get_commands_containing(session, text)
        if len(matching_commands) == 0:
            await ctx.send("There are no custom commands matching that query.")
            return
        answer = "```\n"
        answer += "\n".join(c.name for c in matching_commands)
        answer += "```"
        await ctx.send(answer)

    # aliases
    @commands.command(help=commands_add.help, hidden=True)
    async def addcom(self, ctx: commands.Context, comm: str, *, text: str):
        await ctx.invoke(self.commands_add, comm=comm, text=text)

    @commands.command(help=commands_remove.help, hidden=True)
    async def delcom(self, ctx: commands.Context, comm: str):
        await ctx.invoke(self.commands_remove, comm=comm)

    @commands.command(help=commands_edit.help, hidden=True)
    async def editcom(self, ctx: commands.Context, comm: str, *, text: str):
        await ctx.invoke(self.commands_edit, comm=comm, text=text)

    # internal
    async def process_commands(self, session: AsyncSession, message: discord.Message):
        command = await self.get_command(session, message.content[1:])
        if command is None:
            return
        channel = message.channel
        await channel.send(command.content)
        self.logger.info(
            "%s - %d used the custom command %s in channel %s - %d",
            message.author,
            message.author.id,
            message.content,
            message.channel,
            message.channel.id,
        )

    async def check_valid_custom_command(
        self, session: AsyncSession, message: discord.Message
    ) -> bool:
        """
        Check whether a message was a valid custom command
        """
        if message.author.bot:
            return False
        if len(message.content) < 2:  # noqa: PLR2004 we need at least a ! and one more char
            return False
        # TODO: this should check for the actual self.bot.command_prefix
        if not message.content.startswith("!"):
            return False
        return (await self.get_command(session, message.content[1:])) is not None

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        """
        Look for a custom command call in the message, and if none was
        received, the bots process_commands() routine will be called.
        """
        async with db.async_sessionmaker() as session:
            if await self.check_valid_custom_command(session, message):
                await self.add_command_executed(
                    session, message, message.content[1:], CommandType.CUSTOM
                )
                await self.process_commands(session, message)
                return
        ctx = await self.bot.get_context(message)
        if ctx.valid:
            await self.bot.invoke(ctx)
            if ctx.command is not None:
                await self.add_command_executed(
                    session, message, ctx.command.qualified_name, CommandType.CLASSIC
                )


async def setup(bot: Bot):
    await db.create_schema(CUSTOM_COMMANDS_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(CustomCommandsCogs(bot))
