import logging

import discord
from discord.ext import commands
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.reaction_roles import REACTION_ROLES_SCHEMA_NAME, Base, ReactionRoleMessage
from utils.context import GuildContext
from utils.errors import (
    KnownChannelNotFoundError,
    KnownGuildNotFoundError,
    UnexpectedChannelTypeError,
)


class ReactionRoleCog(commands.Cog):
    """
    Cog for adding roles by reaction

    You can link messages in the reaction_channel to role ids,
    and the bot will automatically assign the linked role to
    everyone that reacts to the message with a predefined reaction.

    Also contains a handy utility to list all roles with their ids,
    or to search for a role by name.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    async def get_linked_message(
        self, session: AsyncSession, message_id: int
    ) -> ReactionRoleMessage | None:
        stmt = select(ReactionRoleMessage).where(ReactionRoleMessage.message_id == message_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def link_message_to_role(
        self, session: AsyncSession, message: discord.Message, role: discord.Role
    ):
        reaction_role_message = await self.get_linked_message(session, message.id)
        async with session.begin():
            if reaction_role_message is None:
                reaction_role_message = ReactionRoleMessage(message_id=message.id, role_id=role.id)
            else:
                reaction_role_message.message_id = message.id
                reaction_role_message.role_id = role.id
            session.add(reaction_role_message)
        self.logger.info(
            "Linked message %s - %d to role %s - %d",
            message.content,
            message.id,
            role.name,
            role.id,
        )

    async def unlink_message_from_role(self, session: AsyncSession, message_id: int):
        reaction_role_message = await self.get_linked_message(session, message_id)
        if reaction_role_message is not None:
            async with session.begin():
                await session.delete(reaction_role_message)

    async def is_linked_message(self, session: AsyncSession, message_id: int) -> bool:
        return await self.get_linked_message(session, message_id) is not None

    async def create_reaction_role_message(self, role_name: str) -> discord.Message:
        channel = self.bot.get_channel(self.bot.config.ids.reaction_roles_channel)
        if channel is None:
            raise KnownChannelNotFoundError(self.bot.config.ids.reaction_roles_channel)
        if not isinstance(channel, discord.TextChannel):
            raise UnexpectedChannelTypeError(channel.id, type(channel))
        msg = await channel.send(role_name)
        self.logger.info("Created new role msg %s - %d", msg.content, msg.id)
        return msg

    @commands.guild_only()
    @commands.group(aliases=["roles"])
    @commands.has_guild_permissions(manage_roles=True)
    async def role(self, ctx: GuildContext):
        """
        Group for role commands

        Can only be used by admins and the bot owner
        Can also only be used from within the JabKingdom
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @commands.guild_only()
    @role.command(name="add", aliases=["new"])
    async def cmd_add_role(self, ctx: GuildContext, *, role_name: str):
        """
        Add a role and reactable message to gain that role in #set-your-role

        Parameters
        ----------
        role_name: str
            The name of the role you want to add

        Example
        -------
        >>> !role add Track Mania
        Adds a new role with the name "Track Mania" and a corresponding message
        in the #set-your-role channel, and any user who reacts to the message
        will get the "Track Mania" role.
        """
        role_name = role_name.strip('"')
        try:
            role = await ctx.guild.create_role(name=role_name, mentionable=True)
            msg = await self.create_reaction_role_message(role_name)
            await msg.add_reaction("🤔")
            async with db.async_sessionmaker() as session:
                await self.link_message_to_role(session, msg, role)
        except Exception as e:
            await ctx.send(f"Operation failed: {e}")

    @commands.guild_only()
    @role.command(name="link", aliases=["link_message_to_role", "lnk"])
    async def cmd_link_message_to_role(
        self, ctx: GuildContext, message: discord.Message, role: discord.Role
    ):
        """
        Link a mesasge to a role

        Parameters
        ----------
        message: Message
            Identifier of the message to link (link or channelid-messageid tuple)
        role: Role
            Identifier of the role to link (ID, mention, name)

        Example
        -------
        >>> !role link 23450987388 1232432138320999
        Links the message with the id 23450987388 to the role with the id
        1232432138320999
        """
        async with db.async_sessionmaker() as session:
            await self.link_message_to_role(session, message, role)
        await message.add_reaction("🤔")
        self.logger.info(
            "Linked message %s - %s to role %s - %s",
            message.content,
            message.id,
            role.name,
            role.id,
        )
        await ctx.send(f"Successfully linked message `{message.content}` to role `{role.name}`.")

    @commands.guild_only()
    @role.command(aliases=["ids"])
    async def allids(self, ctx: GuildContext):
        """
        Show all roles with their ids.
        """
        response = (
            "```\n" + "\n".join(f"{role.id}\t{role.name}" for role in ctx.guild.roles) + "\n```"
        )
        return await ctx.send(response)

    @commands.guild_only()
    @role.command()
    async def search(self, ctx: GuildContext, rolename: str):
        """
        Search for a role containing a specific text

        Parameters
        ----------
        rolename : str
            the text you want to be contained in the word

        Example
        -------
        >>> !role search golf
        Shows you all roles and their ids which have "golf" in their name
        (not case sensitive)
        """
        role_objects = ctx.guild.roles
        matching_roles = [
            (i.name, i.id) for i in role_objects if rolename.lower() in i.name.lower()
        ]
        if len(matching_roles) == 0:
            await ctx.send("Couldn't find a role that has this text in it.")
        else:
            answer = (
                "```\n" + "\n".join(f"{role[1]}\t{role[0]}" for role in matching_roles) + "\n```"
            )
            await ctx.send(answer)

    # events

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        """
        Test whether the deleted message was linked to a role

        If so, delete the link from the internal database.
        """
        async with db.async_sessionmaker() as session:
            if await self.is_linked_message(session, payload.message_id):
                await self.unlink_message_from_role(session, payload.message_id)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        """
        Test whether the reaction was on a message thats linked to a role

        If so, add the role to the user.
        """
        # NOTE: this will break if multiple servers use the feature
        async with db.async_sessionmaker() as session:
            if (
                message := await self.get_linked_message(session, payload.message_id)
            ) is not None and payload.user_id != self.bot.user.id:
                guild = self.bot.get_guild(self.bot.config.ids.guild_id)
                if guild is None:
                    raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
                member = guild.get_member(payload.user_id)
                if member is None:
                    return
                role = discord.utils.get(guild.roles, id=message.role_id)
                if role is None:
                    return
                await member.add_roles(role)
                self.logger.info(
                    "Added role %s - %s to member %s - %s",
                    role.name,
                    message.role_id,
                    member,
                    member.id,
                )

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        """
        Tests whether the reaction was on a message thats linked to a role

        If so, remove the role from the user.
        """
        async with db.async_sessionmaker() as session:
            if (
                message := await self.get_linked_message(session, payload.message_id)
            ) is not None and payload.user_id != self.bot.user.id:
                guild = self.bot.get_guild(self.bot.config.ids.guild_id)
                if guild is None:
                    raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
                member = guild.get_member(payload.user_id)
                if member is None:
                    return
                role = discord.utils.get(guild.roles, id=message.role_id)
                if role is None:
                    return
                await member.remove_roles(role)
                self.logger.info(
                    "Removed role %s - %s from member %s - %s",
                    role.name,
                    message.role_id,
                    member,
                    member.id,
                )


async def setup(bot: Bot):
    await db.create_schema(REACTION_ROLES_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(ReactionRoleCog(bot))
