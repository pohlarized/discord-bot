import datetime
import logging

from discord import TextChannel
from discord.ext import commands, tasks

from bot import Bot
from utils.errors import KnownChannelNotFoundError, UnexpectedChannelTypeError


class YearProgressCog(commands.Cog):
    """
    Cog to print the year progress
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        # self.year_progress_task.start()

    async def cog_unload(self):
        self.year_progress_task.cancel()

    def today_utc(self) -> datetime.date:
        return datetime.datetime.now(datetime.UTC).date()

    def seconds_in_current_year(self) -> float:
        current_year = self.today_utc().year
        return (
            datetime.datetime(current_year + 1, 1, 1, tzinfo=datetime.UTC)
            - datetime.datetime(current_year, 1, 1, tzinfo=datetime.UTC)
        ).total_seconds()

    def current_year_progress(self) -> float:
        now = datetime.datetime.now(datetime.UTC)
        seconds_in_current_year = self.seconds_in_current_year()
        seconds_since_year_start = (
            now - datetime.datetime(now.year, 1, 1, tzinfo=datetime.UTC)
        ).total_seconds()
        return seconds_since_year_start / seconds_in_current_year

    def progress_bar(self, progress: float, width: int = 100) -> str:
        rounded_percent = round(progress, ndigits=4)
        full_bars = int(rounded_percent * width)
        return "▓" * full_bars + "░" * (width - full_bars)

    @commands.command(name="year_progress", aliases=["yp"])
    async def cmd_year_progress(
        self, ctx: commands.Context, width: commands.Range[int, 1, 1000] = 25
    ):
        """
        Show the current year progress as a progress bar

        Params
        ------
        width: int (default 25)
            A range between 1 and 1000 determining the length of the progress bar
        """
        year_progress = self.current_year_progress()
        msg = f"Year progress:\n{self.progress_bar(year_progress, width)} {year_progress:.2%}"
        await ctx.send(msg)

    @tasks.loop(time=[datetime.time(hour=6, tzinfo=datetime.UTC)])
    async def year_progress_task(self):
        channel = self.bot.get_channel(self.bot.config.ids.bot_channel_id)
        if channel is None:
            raise KnownChannelNotFoundError(self.bot.config.ids.bot_channel_id)
        if not isinstance(channel, TextChannel):
            raise UnexpectedChannelTypeError(channel.id, type(channel))
        year_progress = self.current_year_progress()
        msg = f"Year progress:\n{self.progress_bar(year_progress, 20)} {year_progress:.2%}"
        await channel.send(msg)

    @year_progress_task.before_loop
    async def year_progress_task_before(self):
        # TODO: check whether previous message was sent, and if not, resend it
        await self.bot.wait_until_ready()


async def setup(bot: Bot):
    await bot.add_cog(YearProgressCog(bot))
