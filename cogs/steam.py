from __future__ import annotations

import enum
import json
import logging
import random
import re
import sys
import traceback
from functools import cached_property
from http import HTTPStatus
from typing import TYPE_CHECKING

from discord.ext import commands

if TYPE_CHECKING:
    from aiohttp import ClientSession

    from bot import Bot
    from utils.context import BotContext

# reference: https://developer.valvesoftware.com/wiki/SteamID


STEAMID_64_MAX_BITS = 0x110000100000000


class InvalidSteamIDError(commands.BadArgument):
    pass


class CustomIDNotFoundError(InvalidSteamIDError):
    pass


class NoCustomIDSetError(InvalidSteamIDError):
    pass


class NonExistantID64Error(InvalidSteamIDError):
    pass


class SteamIdKind(enum.Enum):
    INVALID = 0
    ID64 = 1
    ID32 = 2
    ID3 = 3
    VANITY = 4


class SteamId:
    def __init__(self, steam64id: int):
        self.steam64id = steam64id

    @cached_property
    def add_to_friends_link(self) -> str:
        return f"steam://friends/add/{self.steam64id}"

    @cached_property
    def profile_link(self) -> str:
        return f"https://steamcommunity.com/profiles/{self.steam64id}"

    @cached_property
    def steam32id(self) -> str:
        steam_x = self.steam64id >> 56
        steam_y = self.steam64id & 1
        steam_z = (self.steam64id & 0xFFFFFFFF) >> 1
        return f"STEAM_{steam_x}:{steam_y}:{steam_z}"

    @cached_property
    def steam3id(self) -> str:
        type_to_letter = "IUMGAPCgT a"  # type as int is the index to this string, e.g. 0 => I
        account_type = (self.steam64id >> 52) & 0xF
        steam_w = self.steam64id & 0xFFFFFFFF
        return f"[{type_to_letter[account_type]}:1:{steam_w}]"

    async def vanity_url(self, aiohttp_session: ClientSession, steam_api_key: str) -> str:
        url = (
            "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/"
            f"v0002/?key={steam_api_key}&steamids={self.steam64id}"
        )
        async with aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
            if len(data["response"]["players"]) == 0:
                raise NonExistantID64Error(str(self.steam64id))
            profileurl = data["response"]["players"][0]["profileurl"].split("/")[-2]
            if str(profileurl) != str(self.steam64id):
                return profileurl
            raise NoCustomIDSetError(str(self.steam64id))

    async def nickname(self, aiohttp_session: ClientSession, steam_api_key: str) -> str:
        url = (
            "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/"
            f"v0002/?key={steam_api_key}&steamids={self.steam64id}"
        )
        async with aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
            if len(data["response"]["players"]) == 0:
                raise NonExistantID64Error(str(self.steam64id))
            return data["response"]["players"][0]["personaname"]

    def random_steam64id(self, additional_bits: int) -> int:
        important = self.steam64id & 0xFFF00000FFFFFFFF
        middle_bits = random.randrange(0x100000) << 32
        outer_bits = random.randrange(2**additional_bits) << 64
        return important | middle_bits | outer_bits

    @staticmethod
    def sanitize_steam64id(steam64id: str) -> int:
        """
        Only the last 32bit identify you, and valve allows some link shenanigans
        """
        return (int(steam64id) & 0xFFFFFFFF) | STEAMID_64_MAX_BITS

    @classmethod
    def check_id_kind(cls, steam_id: str) -> SteamIdKind:
        """
        Chek what format a steamid is in

        Parameters
        ----------
        steam_id : str
            the steamid you want to check

        Returns
        -------
        SteamIdKind
            Enum variant of the SteamIdKind
        """
        id_format = SteamIdKind.INVALID
        if steam_id.isdigit():
            maybe_id64 = cls.sanitize_steam64id(steam_id)
            if maybe_id64 & STEAMID_64_MAX_BITS == STEAMID_64_MAX_BITS:
                id_format = SteamIdKind.ID64
        elif len(re.findall(r"STEAM_0:[0-1]:\d+", steam_id)) > 0:
            id_format = SteamIdKind.ID32
        elif len(re.findall(r"\[U:1:\d+\]", steam_id)) > 0:
            id_format = SteamIdKind.ID3
        elif steam_id.replace("-", "a").replace("_", "a").isalnum():
            id_format = SteamIdKind.VANITY
        return id_format

    @staticmethod
    def _steam32id_to_steam64id(steam32id: str) -> int:
        """
        Convert a steam32id to a steam64id

        Parameters
        ----------
        steamid : str
            the users steamid32

        Returns
        -------
        int
            the users steamid64
        """
        steam_y, steam_z = map(int, steam32id.strip(" STEAM_").split(":")[1:])
        return (steam_z << 1) | steam_y | 0x110000100000000

    @staticmethod
    def _steam3id_to_steam64id(steam3id: str) -> int:
        """
        Convert a steam3id to a steam64id.

        Parameters
        ----------
        steamid : str
            the users steam3id

        Returns
        -------
        int
            the users steamid64
        """
        steam32 = int(steam3id.strip().split(":")[-1][:-1])
        return steam32 | 0x110000100000000

    @staticmethod
    async def _vanity_id_to_steam64id(ctx: BotContext, vanity_id: str) -> int:
        """
        Convert a vanity ID to a steam64id

        Parameters
        ----------
        vanity_id : str
            The user's vanity ID.

        Returns
        -------
        int
            the users steamid64
        """
        url = (
            "http://api.steampowered.com/ISteamUser/ResolveVanityURL/"
            f"v0001/?key={ctx.bot.config.steam_api_key}&vanityurl={vanity_id}"
        )
        async with ctx.bot.aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
        if data["response"]["success"] == 1:
            steamid64 = data["response"]["steamid"]
        else:
            raise CustomIDNotFoundError(vanity_id)
        return int(steamid64)

    @classmethod
    async def convert(cls, ctx: BotContext, steam_id: str) -> SteamId:
        if "steamcommunity.com/" in steam_id:
            steam_id = steam_id.strip().strip("/").split("/")[-1]
        id_format = cls.check_id_kind(steam_id)
        if id_format == SteamIdKind.INVALID:
            raise InvalidSteamIDError(steam_id)
        if id_format == SteamIdKind.ID64:
            steam64id = cls.sanitize_steam64id(steam_id)
        elif id_format == SteamIdKind.ID32:
            steam64id = cls._steam32id_to_steam64id(steam_id)
        elif id_format == SteamIdKind.ID3:
            steam64id = cls._steam3id_to_steam64id(steam_id)
        else:
            steam64id = await cls._vanity_id_to_steam64id(ctx, steam_id)
        return SteamId(steam64id)


class SteamCog(commands.Cog):
    """
    Cog for steam commands

    Some handy steam id conversion tools and quick
    access to some APIs by any steam id type.
    APIs supported atm:
        - etf2l
        - logs.tf
        - rocketleague.tracker.network
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    async def player_summary(self, steamid64: int) -> dict:
        """
        Query the steam API for the player summary of a player
        """
        url = (
            "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/"
            f"?key={self.bot.config.steam_api_key}&steamids={steamid64}"
        )
        async with self.bot.aiohttp_session.get(url) as resp:
            data = json.loads(await resp.text())
        return data["response"]["players"][0]

    @commands.group()
    async def steam(self, ctx: commands.Context):
        """
        Group for steam commands
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @steam.command()
    async def userinfo(self, ctx: commands.Context, steam_id: SteamId):
        """
        Show a message with lots of useful info about the user.

        Parameters
        ----------
        steam_id : SteamID
            Any steam id of the user you want to look up.

        Example
        -------
        >>> !steam userinfo St4ck
        Gives you info about the steam user with the id "St4ck"
        """
        steam_id64 = steam_id.steam64id
        steam_id32 = steam_id.steam32id
        steam3id = steam_id.steam3id
        try:
            custom_id = await steam_id.vanity_url(
                self.bot.aiohttp_session, self.bot.config.steam_api_key
            )
        except NoCustomIDSetError:
            custom_id = "No custom id set."
        nickname = await steam_id.nickname(self.bot.aiohttp_session, self.bot.config.steam_api_key)
        await ctx.send(
            f"```|   nickname: {nickname}\n"
            f"|  steamid64: {steam_id64}\n"
            f"|  steamid32: {steam_id32}\n"
            f"|   steam3id: {steam3id}\n"
            "| profileurl: https://steamcommunity.com/profiles/"
            f"{steam_id64}\n"
            f"|   customID: {custom_id}```"
        )
        self.logger.info(
            "%s - %s looked up a steam user with the ID %s", ctx.author, ctx.author.id, steam_id64
        )

    @steam.command()
    async def link(self, ctx: commands.Context, steam_id: SteamId):
        """
        Show a link to the users profiles

        Parameters
        ----------
        steam_id : SteamId
            Any steam id of the user you want to get the link for.

        Example
        -------
        >>> !steam link STEAM_0:1:31574593
        Gives you a link to the steam user with the id "STEAM_0:1:31574593"
        """
        await ctx.send(steam_id.profile_link)
        self.logger.info(
            "%s - %s searched for the link of user %s",
            ctx.author,
            ctx.author.id,
            steam_id.steam64id,
        )

    @steam.command()
    async def snipe(self, ctx: commands.Context, steam_id: SteamId):
        """
        Get a join link to the server the player is currently playing on
        """
        player_data = await self.player_summary(steam_id.steam64id)
        if "gameserverip" not in player_data:
            return await ctx.send("User is currently not in game or has their server IP private.")
        return await ctx.send(f"steam://connect/{player_data['gameserverip']}")

    @steam.command()
    async def logs(self, ctx: commands.Context, steam_id: SteamId):
        """
        Show a link to the users logs.tf profile

        Parameters
        ----------
        steam_id : SteamId
            Any steam id of the user you want to get the logs.tf profile for.

        Example
        -------
        >>> !steam logs [U:1:63149187]
        Gives you a link to the logs.tf profile of the steam user with the id
        [U:1:63149187]
        """
        await ctx.send(f"https://logs.tf/profile/{steam_id.steam64id}")
        self.logger.info(
            "%s - %s searched for the logs of user with the ID %s",
            ctx.author,
            ctx.author.id,
            steam_id.steam64id,
        )

    @steam.command()
    async def etf2l(self, ctx: commands.Context, steam_id: SteamId):
        """
        Return a link to that users etf2l profile
        Parameters
        ----------
        steam_id : SteamId
            Any steam id of the user you want to get the etf2l profile for.

        Example
        -------
        >>> !steam etf2l St4ck
        Sends you a link to the etf2l profile for St4ck
        """
        url = f"http://api-v2.etf2l.org/player/{steam_id.steam64id}"
        async with self.bot.aiohttp_session.get(url) as resp:
            if not resp.ok:
                await ctx.send("Seems like the etf2l API is currently unreachable.")
                return
            data = json.loads(await resp.text())
            if data["status"]["code"] != HTTPStatus.OK:
                response = data["status"]["message"]
                await ctx.send(f"etf2l error: {response}")
                return
            player_id = data["player"]["id"]
            await ctx.send(f"http://etf2l.org/forum/user/{player_id}")
            self.logger.info(
                "%s - %s looked up the etf2l profile for ID %s",
                ctx.author,
                ctx.author.id,
                steam_id.steam64id,
            )

    @steam.command(name="add")
    async def steam_add(self, ctx: commands.Context, steam_id: SteamId):
        """
        Show a steam add link for the given steam id

        Parameters
        ----------
        steam_id : SteamId
            The steam id of the user you want to add.

        Example
        -------
        >>> !steam add St4ck
        Shows you a link to directly add the steam user with the id "St4ck"
        """
        await ctx.send(steam_id.add_to_friends_link)
        self.logger.info(
            "%s - %s looked up a steam friend link with the ID %s",
            ctx.author,
            ctx.author.id,
            steam_id.steam64id,
        )

    @steam.command(aliases=["integrity", "integrity_check", "verify"])
    async def check_integrity(self, ctx: commands.Context, steam_id64: str):
        """
        Check whether the steamid64 you have has been fiddled with,
        even if it links to the correct account.

        Parameters
        ----------
        steam_id64 : str
            the steamid64 you want to check

        Example
        -------
        >>> !steam check_integrity 76561198023414915
        This id is good :thumbsup:
        """
        id_type = SteamId.check_id_kind(steam_id64)
        if id_type != SteamIdKind.ID64:
            await ctx.send_help(ctx.command)
            return await ctx.send("Invalid ID format! I need an ID64.")
        real_id64 = SteamId.sanitize_steam64id(steam_id64)
        self.logger.info(
            "%s - %s checked the integrity of the ID %s", ctx.author, ctx.author.id, steam_id64
        )

        if str(steam_id64) != str(real_id64):
            return await ctx.send(
                f":no_entry_sign: The ID `{steam_id64}` is "
                f"**NOT** valid. The real ID is "
                f"`{real_id64}` :no_entry_sign:"
            )
        return await ctx.send(f"The ID `{steam_id64}` is valid. :white_check_mark:")

    # additional bits may at most be 1900 decimal places, so at most math.log2(10 ** 1900) ~= 6311
    @steam.command(aliases=["randomize", "randomise", "randomise_link"], hidden=True)
    async def randomize_link(
        self,
        ctx: commands.Context,
        steam_id: SteamId,
        additional_bits: commands.Range[int, 0, 6311] = 0,
    ):
        """
        Gives you a random link to your profile

        Parameters
        ----------
        steam_id : SteamId
            The steamid you want a link to.
        additional_bits : int
            The number of bits you want to add to the usual 64bit steam ID.

        Example
        -------
        >>> !steam randomize_link St4ck
        https://steamcommunity.com/profiles/78486846610576515
        """
        random_id = steam_id.random_steam64id(additional_bits)
        link = f"https://steamcommunity.com/profiles/{random_id}"
        await ctx.send(link)
        self.logger.info(
            "%s - %s randomized a steam link with the ID %s",
            ctx.author,
            ctx.author.id,
            steam_id.steam64id,
        )

    @commands.command(help=steam_add.help, hidden=True)
    async def add(self, ctx: commands.Context, steam_id: SteamId):
        await ctx.invoke(self.steam_add, steam_id=steam_id)

    @userinfo.error
    @link.error
    @snipe.error
    @logs.error
    @etf2l.error
    @steam_add.error
    @check_integrity.error
    @randomize_link.error
    async def error_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        elif isinstance(error, commands.MissingRequiredArgument):
            await ctx.send_help(ctx.command)
            await ctx.send("You need to specify the steamID")
        elif isinstance(error, InvalidSteamIDError):
            await ctx.send_help(ctx.command)
            await ctx.send(f"I do not recognize the steamID format of `{error}`")
        elif isinstance(error, CustomIDNotFoundError):
            await ctx.send(f"Could not find anyone with the vanityID `{error}`")
        elif isinstance(error, NonExistantID64Error):
            await ctx.send(f"Could not find a player with the steamID64 `{error}`")
        elif isinstance(error, commands.BadArgument):
            if isinstance(error, commands.RangeError):
                await ctx.send(f"Argument out of range: {error}")
            else:
                await ctx.send(f"Bad argument: {error}")
        else:
            print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)


async def setup(bot: Bot):
    await bot.add_cog(SteamCog(bot))
