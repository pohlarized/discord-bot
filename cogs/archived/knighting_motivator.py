import asyncio
import datetime
import discord
import json
import logging
import math
import os
from discord.ext import commands

import ids

PATH = 'data/knighting_motivator/'
NAME = 'activity.json'
FULL_PATH = PATH + NAME

async def is_good_boi(ctx):
    return ctx.author.id in [ctx.bot.owner_id, ids.KING_ID]

class KnightingMotivator(commands.Cog):
    """
    Tracks activity of select users, and mutes them if they reach a certain threshold.
    """
    def __init__(self, bot):
        self.bot = bot
        self.subjects = []
        self.tracking = {}
        self.mute_role_id = 0
        self.jabk_id = 0
        self.quota = 500
        self.day = datetime.datetime.now().day
        self.logger = logging.getLogger(__name__)
        self.do_loops = True
        self.save_loop = self.bot.loop.create_task(self.looped_save_json())
        self.reset_loop = self.bot.loop.create_task(self.looped_reset_messagecount())
        self.load_json()

    def cog_unload(self):
        self.save_json()
        self.save_loop.cancel()
        self.reset_loop.cancel()
        self.do_loops = False

    @commands.group(aliases=['km'])
    async def knighting_motivator(self, ctx):
        """Group for knighting_motivator commands"""
        pass

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def save(self, ctx):
        """Save the current data to the json file"""
        self.save_json()
        await ctx.send('Saved json.')

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def reset(self, ctx):
        """Reset the wordcount for all subjects"""
        for subject in self.subjects:
            self.tracking[str(subject)] = 0
        return await ctx.send('Successfully reset all subjects messages for today.')

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def show_tracking(self, ctx):
        """Shows the amount of messages sent by all subjects today"""
        return await ctx.send(self.tracking)

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def show_subjects(self, ctx):
        """Shows all subjects"""
        return await ctx.send(self.subjects)

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def show_quota(self, ctx):
        return await ctx.send(self.quota)

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def set_mute_role(self, ctx, role_id):
        """Sets the mute role"""
        if not role_id.isdecimal():
            return await ctx.send('The role_id must be an integer')
        mute_role_id = int(role_id)
        mute_role = discord.utils.get(ctx.guild.roles, id=mute_role_id)
        #TODO: Does this error or return None?
        if mute_role is None:
            return await ctx.send('This role_id does not correspond to a role on this server.')
        self.mute_role_id = mute_role_id
        self.save_json()
        return await ctx.send(f'Successfully changed mute role to {mute_role}')

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def add_subject(self, ctx, user_id):
        """Adds a subject to be limited"""
        if not user_id.isdecimal():
            return await ctx.send('The user_id must be an integer')
        user_id_int = int(user_id)
        if user_id_int in self.subjects:
            return await ctx.send('This user is already on the subject list.')
        user = ctx.guild.get_member(user_id_int)
        if user is None:
            return await ctx.send('This user_id does not correspond to a user on this server.')
        self.subjects.append(user_id_int)
        self.save_json()
        return await ctx.send(f'Successfully added {user} to the subject list.')

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def remove_subject(self, ctx, user_id):
        if not user_id.isdecimal():
            return await ctx.send('Invalid user_id')
        user_id_int = int(user_id)
        if not user_id_int in self.subjects:
            return await ctx.send('This user is not on the subject list')
        self.subjects.remove(user_id_int)
        del(self.tracking[user_id])
        self.save_json()
        return await ctx.send(f'Successfully removed the user from the subject list.')

    @knighting_motivator.command()
    @commands.check(is_good_boi)
    async def set_quota(self, ctx, quota):
        if not quota.isdecimal():
            return await ctx.send('The quota must be a number')
        real_quota = int(quota)
        if real_quota > 10000 or real_quota <= 0:
            return await ctx.send('The quota must be between 0 and 10000')
        self.quota = real_quota
        self.save_json()
        return await ctx.send(f'Successfully set the quota to {quota}')

    @commands.Cog.listener()
    async def on_message(self, message):
        ctx = await self.bot.get_context(message)
        if ctx.author.id in self.subjects:
            if not str(ctx.author.id) in self.tracking:
                self.tracking[str(ctx.author.id)] = 0
            self.tracking[str(ctx.author.id)] += 1
            if self.tracking[str(ctx.author.id)] >= self.quota:
                await self.mute_user(ctx.author.id)
                await message.channel.send(f'{message.author.mention} you have reached your daily message quota.')
            elif self.tracking[str(ctx.author.id)] == int(0.75 * self.quota):
                await message.channel.send(f'{message.author.mention} you have reached 75% of your daily message quota')
            elif self.tracking[str(ctx.author.id)] == int(0.5 * self.quota):
                await message.channel.send(f'{message.author.mention} you have reached 50% of your daily message quota')
            elif self.tracking[str(ctx.author.id)] == int(0.25 * self.quota):
                await message.channel.send(f'{message.author.mention} you have reached 25% of your daily message quota')

    async def mute_user(self, user_id):
        jabk = self.bot.get_guild(self.jabk_id)
        mute_role = discord.utils.get(jabk.roles, id=self.mute_role_id)
        user = jabk.get_member(user_id)
        await user.add_roles(mute_role)
        self.logger.info('Successfully muted %s - %d for reaching their daily quota.' %
                          (str(user), user_id))

    async def unmute_user(self, user_id):
        jabk = self.bot.get_guild(self.jabk_id)
        mute_role = discord.utils.get(jabk.roles, id=self.mute_role_id)
        user = jabk.get_member(user_id)
        await user.remove_roles(mute_role)
        self.logger.info('Successfully unmuted %s - %d for the new day.' %
                         (str(user), user_id))

    async def looped_save_json(self):
        while self.do_loops:
            await asyncio.sleep(120)
            self.save_json()

    async def looped_reset_messagecount(self):
        while self.do_loops:
            await asyncio.sleep(30)
            today = datetime.datetime.now().day
            if today != self.day:
                for subject in self.subjects:
                    await self.unmute_user(subject)
                    self.tracking[str(subject)] = 0
                self.day = today
                self.save_json()
                self.logger.info('Reset all messagecounts and unmuted all subjects.')


    def load_json(self):
        """
        Reload the data from json
        """
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couldn't find %s directory, created new one." % PATH)
        if os.path.isfile(FULL_PATH):
            with open(FULL_PATH, "r") as fp:
                data = json.load(fp)
            self.tracking = data['tracking']
            self.day = data['day']
            self.subjects = data['subjects']
            self.mute_role_id = data['mute_role_id']
            self.jabk_id = data['jabk_id']
            self.quota = data['quota']
        else:
            self.save_json()
            self.logger.warning("Couldn't find %s, created new one." % FULL_PATH)

    def save_json(self):
        """
        Save the current data to json
        """
        data = {
                'tracking': self.tracking,
                'day': self.day,
                'subjects': self.subjects,
                'mute_role_id': self.mute_role_id,
                'jabk_id': self.jabk_id,
                'quota': self.quota
                }
        with open(FULL_PATH, "w") as fp:
            json.dump(data, fp, indent=2)


async def setup(bot):
    await bot.add_cog(KnightingMotivator(bot))
