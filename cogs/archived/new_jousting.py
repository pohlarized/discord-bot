import logging
import yaml
import os
import re

import discord
from discord.ext import commands

import ids


PATH = 'data/joustingtwo/'
FILE_NAME = 'reaction_channels.yaml'
FILE_PATH = PATH + FILE_NAME


class JoustingTwoCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        # msgid -> channelid
        self.reaction_messages = {}
        self.load_data()

    def check_channel_name(self, name):
        match = re.match(r'[A-Za-z0-9_-]+', name)
        if not match:
            return False
        if match[0] != name:
            return False
        if name.startswith('-'):
            return False
        if name[-1] == '-':
            return False
        if '--' in name:
            return False
        return True

    async def fail(self, message):
        mymsg = await message.channel.send(f'{message.author.mention} That channel name is invalid!')
        await message.delete(delay=5)
        await mymsg.delete(delay=5)

    async def create_jousting_channel(self, name, guild, creator):
        everyone_perms = discord.PermissionOverwrite(read_messages=False)
        council_perms = discord.PermissionOverwrite(read_messages=True)
        bot_perms = discord.PermissionOverwrite(manage_roles=True, manage_messages=True, read_messages=True)

        allowed_role = guild.get_role(ids.COUNCIL_ROLE)
        default_role = guild.default_role
        bot = guild.get_role(ids.STATE_SECRETARY_ROLE)
        overwrites = {
                default_role: everyone_perms,
                allowed_role: council_perms,
                creator: council_perms,
                bot: bot_perms
        }

        category = self.bot.get_channel(ids.GAMING_CATEGORY)
        channel = await category.create_text_channel(name,
                                                     overwrites=overwrites)
        return channel

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.channel.id != ids.TOURNAMENT_SUGGESTIONS:
            return
        if message.author.id == self.bot.user.id:
            return

        channel_name = message.content.strip()
        if not self.check_channel_name(channel_name):
            return await self.fail(message)

        channel = await self.create_jousting_channel(channel_name,
                                                     message.guild,
                                                     message.author)
        await message.add_reaction('🤔')
        self.reaction_messages[message.id] = channel.id
        self.save_data()

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload):
        # if the deleted message was a message linked in the
        # reaction messages, delete the link
        if(payload.message_id) in self.reaction_messages:
            del(self.reaction_messages[payload.message_id])
            self.save_data()
            # TODO: log this i guess..

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        msgid = payload.message_id
        userid = payload.user_id
        if msgid in self.reaction_messages and userid != self.bot.user.id:
            server = self.bot.get_guild(ids.GUILD_ID)
            member = server.get_member(userid)
            channel = server.get_channel(self.reaction_messages[msgid])
            await channel.set_permissions(member, read_messages=True)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        msgid = payload.message_id
        userid = payload.user_id
        if msgid in self.reaction_messages and userid != self.bot.user.id:
            server = self.bot.get_guild(ids.GUILD_ID)
            member = server.get_member(userid)
            channel = server.get_channel(self.reaction_messages[msgid])
            await channel.set_permissions(member, overwrite=None)

    def save_data(self):
        """
        Save the data
        """
        with open(FILE_PATH, 'w') as fp:
            yaml.safe_dump(self.reaction_messages, fp)

    def load_data(self):
        """
        Load the data
        """
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couldn't find %s directory, created new one."
                                % PATH)
        if os.path.isfile(FILE_PATH):
            with open(FILE_PATH, 'r') as fp:
                self.reaction_messages = yaml.safe_load(fp)
        else:
            self.save_data()
            self.logger.warning("Couldn't find %s, created new one."
                                % FILE_PATH)


async def setup(bot):
    await bot.add_cog(JoustingTwoCog(bot))
