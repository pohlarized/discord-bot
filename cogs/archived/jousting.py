import aiohttp
import logging
import os
import random
import yaml

from discord.ext import commands

import secrets
import ids

PATH = "data/jousting"


async def is_admin(ctx):
    return ctx.author.id in [ctx.bot.owner_id, ids.KING_ID]

class ChallongeException(Exception):
    pass


class Challonge:
    FILENAME = "/challonge_data.yml"
    FILE_PATH = PATH + FILENAME

    def __init__(self, api_key, tournament=None, aiohttp_session=None):
        self.logger = logging.getLogger(__name__)
        self.api_key = api_key
        self.tournament = tournament
        self.base_url = f"https://api.challonge.com/v1/tournaments/{tournament}"
        self.session = aiohttp_session
        self.participants = {}
        self.init_aiohttp_session()
        self.load_data()

    def init_aiohttp_session(self):
        if self.session is None:
            self.session = aiohttp.ClientSession()

    def set_tournament(self, tournament):
        self.tournament = tournament

    async def send_get(self, endpoint):
        url = f"{self.base_url}{endpoint}.json?api_key={self.api_key}"
        async with self.session.get(url) as resp:
            if resp.status != 200:
                raise ChallongeException(resp.status)
            data = await resp.json()
        return data

    async def send_put(self, endpoint, params: dict):
        url = f"{self.base_url}{endpoint}.json?api_key={self.api_key}"
        async with self.session.put(url, params=params) as resp:
            if resp.status == 422:
                data = await resp.json()
                raise ChallongeException("ValidationError", data)

            if resp.status != 200:
                raise ChallongeException(resp.status)
            data = await resp.json()
        return data

    async def init_participants(self, tournament=None):
        if tournament is not None:
            self.set_tournament(tournament)
        participants = await self.send_get("/participants")
        self.participants = {}
        for participant in participants:
            name = participant["participant"]["name"]
            player_id = str(participant["participant"]["id"])
            self.participants[player_id] = name
            self.participants[name] = player_id
        self.save_data()
        return len(participants)

    async def get_match(self, match_id):
        resp = await self.send_get(f"/matches/{match_id}")
        return resp

    async def get_rounds(self):
        resp = await self.send_get("/matches")
        return resp

    async def get_round(self, idx):
        rounds = await self.get_rounds()
        idx_rounds = [x for x in rounds if x["match"]["round"] == idx]
        return idx_rounds

    def get_player_names(self, match):
        player1_id = str(match["match"]["player1_id"])
        player2_id = str(match["match"]["player2_id"])
        return [self.participants[player1_id], self.participants[player2_id]]

    async def get_matchups(self, idx):
        rnd = await self.get_round(idx)
        matchups = []
        for match in rnd:
            matchup = {
                "id": str(match["match"]["id"]),
                "participants": self.get_player_names(match)
            }
            matchups.append(matchup)
        return matchups

    async def post_score(self, match_id, score):
        """
        score format:
        Dict("player1name": score, "player2name": score)
        """
        payload = {}
        endpoint = f"/matches/{match_id}"
        match = await self.get_match(match_id)
        player1, player2 = self.get_player_names(match)
        payload["match[scores_csv]"] = f"{score[player1]}-{score[player2]}"
        winner_name = player1 if score[player1] > score[player2] else player2
        if score[player1] == score[player2]:
            winner_name = random.choice([player1, player2])
        winner_id = self.participants[winner_name]
        payload["match[winner_id]"] = winner_id
        resp = await self.send_put(endpoint, payload)

    def load_data(self):
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couldn't find %s directory, created new one."
                                % PATH)
        if os.path.isfile(self.FILE_PATH):
            with open(self.FILE_PATH, "r") as fp:
                self.participants = yaml.safe_load(fp)
        else:
            self.save_data()
            self.logger.warning("Couldn't find %s, created new one."
                                % self.FILE_PATH)

    def save_data(self):
        with open(self.FILE_PATH, "w") as fp:
            yaml.safe_dump(self.participants, fp)


class JoustingCog(commands.Cog):
    FILENAME = "/jousting_data.yml"
    FILE_PATH = PATH + FILENAME

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.challonge = Challonge(secrets.CHALLONGE_API_KEY,
                                   "jabkingdom-JabKemoteWars2",
                                   bot.aiohttp_session)
        self.channel_id = ids.JOUSTING_STADIUM
        self.matchups = {}
        self.round = 1
        self.load_data()

    @commands.group(aliases=["j"])
    async def jousting(self, ctx):
        """
        Group for jousting commands
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @jousting.command()
    @commands.check(is_admin)
    async def get_matchups(self, ctx, rnd):
        matchups = await self.challonge.get_matchups(int(rnd))
        await ctx.send(len(matchups))

    async def rollout(self, rnd):
        self.logger.info(rnd)
        matchups = await self.challonge.get_matchups(rnd)
        if len(matchups) == 0:
            self.logger.info("No matchups.")
            return -1
        channel = self.bot.get_channel(self.channel_id)
        if rnd == 0:
            await channel.send(f"**CUSTOM EMOTE JOUSTING THIRD PLACE MATCH**")
        else:
            await channel.send(f"**CUSTOM EMOTE JOUSTING ROUND {rnd}**:")
        self.matchups = {}
        for matchup in matchups:
            player1 = matchup["participants"][0]
            player2 = matchup["participants"][1]
            match_id = matchup["id"]
            msg = await channel.send(f"{player1} {player2}")
            await msg.add_reaction(player1)
            await msg.add_reaction(player2)
            self.matchups[match_id] = str(msg.id)
        self.save_data()
        return 1

    async def collect_scores(self):
        channel = self.bot.get_channel(self.channel_id)
        scores = {}
        for match_id, msg_id in self.matchups.items():
            message = await channel.fetch_message(int(msg_id))
            reactions = message.reactions
            score = {}
            for reaction in reactions:
                score[str(reaction.emoji)] = reaction.count - 1
            scores[match_id] = score
        return scores

    @jousting.command()
    @commands.check(is_admin)
    async def init(self, ctx):
        await ctx.send("Initializing tournament. This can take a while.")
        parts = await self.challonge.init_participants()
        self.round = 1
        await self.rollout(self.round)
        self.save_data()
        return await ctx.send(f"successfully initialized {parts} participants")

    @jousting.command(aliases=["next"])
    @commands.check(is_admin)
    async def nextround(self, ctx):
        await ctx.send("Rolling out next round. This can take a while.")
        # collect scores from previous round
        scores = await self.collect_scores()
        for match_id, score in scores.items():
            self.logger.info("Posting match %s" % match_id)
            await self.challonge.post_score(match_id, score)
        # roll out next round
        self.round += 1
        is_round = await self.rollout(self.round)
        self.save_data()
        if is_round == 1:
            return await ctx.send(f"Successfully rolled out round {self.round}")

    @jousting.command(aliases=["third"])
    @commands.check(is_admin)
    async def thirdplace(self, ctx):
        await ctx.send("Rolling out third place match.")
        await self.rollout(0)
        await ctx.send("Successfully rolled out third place match.")


    @jousting.command()
    @commands.check(is_admin)
    async def collect(self, ctx):
        scores = await self.collect_scores()
        for match_id, score in scores.items():
            self.logger.info("Posting match %s" % match_id)
            await self.challonge.post_score(match_id, score)
        # resp = "**---**\n"
        # for results in scores.values():
        #     for emote, hcore in results.items():
        #         resp += f"{emote}:\t{score}\n"
        #     resp += "**---**\n"

        # await ctx.send(resp)
        await ctx.send("Scores successfully uploaded to challonge")

    def load_data(self):
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couldn't find %s directory, created new one."
                                % PATH)
        if os.path.isfile(self.FILE_PATH):
            with open(self.FILE_PATH, "r") as fp:
                data = yaml.safe_load(fp)
            self.round = data["round"]
            self.matchups = data["matchups"]
        else:
            self.save_data()
            self.logger.warning("Couldn't find %s, created new one."
                                % self.FILE_PATH)

    def save_data(self):
        with open(self.FILE_PATH, "w") as fp:
            data = {
                "round": self.round,
                "matchups": self.matchups
            }
            yaml.safe_dump(data, fp)

async def setup(bot):
    await bot.add_cog(JoustingCog(bot))
