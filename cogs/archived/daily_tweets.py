import logging
import time
import hashlib
import datetime
import os
import yaml

import twitter

from discord.ext import commands, tasks

import secrets

PATH = "data/twitter/"
FILENAME = "twitter_data.yml"
FILE_PATH = PATH + FILENAME

class DailyTweetsCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.quotes = []
        self.last_fetched = 0
        self.load_data()
        self.fetch_tweets_task.start()

    def cog_unload(self):
        self.fetch_tweets_task.cancel()

    @commands.command()
    async def tweetoftheday(self, ctx):
        md5 = hashlib.md5()
        md5.update(datetime.datetime.now().strftime("%Y%m%d").encode("utf-8"))
        number = int(md5.hexdigest(), 16)
        index = number % len(self.quotes)
        await ctx.send(self.quotes[index])

    @commands.command(name="fetch_tweets")
    @commands.is_owner()
    async def fetch_command(self, ctx):
        await ctx.send(self.fetch_tweets())

    def fetch_tweets(self):
        api = twitter.Api(consumer_key=secrets.TWITTER_CONSUMER_KEY,
                          consumer_secret=secrets.TWITTER_CONSUMER_SECRET,
                          access_token_key=secrets.TWITTER_ACCESS_TOKEN_KEY,
                          access_token_secret=secrets.TWITTER_ACCESS_TOKEN_SECRET)
        statuses = api.GetUserTimeline(screen_name="drcheframsay", count=200)
        new_quotes = 0
        for quote in statuses:
            if quote.text not in self.quotes:
                self.quotes.append(quote.text)
                new_quotes += 1
        self.last_fetched = time.time()
        self.save_data()
        self.logger.info("Fetched %i new tweets" % new_quotes)
        return new_quotes

    @tasks.loop(minutes=1)
    async def fetch_tweets_task(self):
        """Fetch new quotes every day"""
        if (time.time() - self.last_fetched) < (3600*24):
            return
        self.fetch_tweets()

    @fetch_tweets_task.before_loop
    async def before_fetching_tweets(self):
        await self.bot.wait_until_ready()

    def load_data(self):
        """Load the data"""
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning("Couln't find %s directory, created new one."
                                % PATH)
        if os.path.isfile(FILE_PATH):
            with open(FILE_PATH, "r") as fp:
                data = yaml.safe_load(fp)
                self.quotes = data["quotes"]
                self.last_fetched = data["last_fetched"]
        else:
            self.save_data()
            self.logger.warning("Couln't find %s, created new one."
                                % FILE_PATH)

    def save_data(self):
        """Save the data"""
        data = {"quotes": self.quotes, "last_fetched": self.last_fetched}
        with open(FILE_PATH, "w") as fp:
            yaml.safe_dump(data, fp)


async def setup(bot):
    await bot.add_cog(DailyTweetsCog(bot))
