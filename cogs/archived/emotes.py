import asyncio
from discord.ext import commands
from fuzzywuzzy import fuzz
import logging
import operator
import os
import json
from random import randrange
import re
import time
import yaml

EMOTES_DATA_NAME = 'emote_data.yml'
BLACKLIST_NAME = 'blacklist.yml'
PATH = 'data/emotes/'
EMOTES_DATA_PATH = PATH + EMOTES_DATA_NAME
BLACKLIST_PATH = PATH + BLACKLIST_NAME


class EmotesCog(commands.Cog):
    """
    Cog for BetterDiscord (BD) emote stats

    Collects BD emote statistics per user. Counts the amount
    of usages of each known emote on a per user basis and allows
    to search for emotes based on various criteria.
    """
    def __init__(self, bot):
        self.bot = bot
        self.emote_dict = {}
        self.descriptor_dict = {}
        self.player_dict = {}
        self.logger = logging.getLogger(__name__)
        self.fileLogger = logging.getLogger(__name__ + ":fileOnly")
        self.load_data()
        self.redownload_loop = self.bot.loop.create_task(self.looped_redownload_emotes())
        self.save_loop = self.bot.loop.create_task(self.looped_save_data())
        self.do_loops = True

    def cog_unload(self):
        self.redownload_loop.cancel()
        self.save_loop.cancel()
        self.do_loops = False

    @commands.group(aliases=["emote"])
    async def emotes(self, ctx):
        """
        Group for all emote commands
        """
        if not ctx.invoked_subcommand:
            await self.bot.send_cmd_help(ctx)

    @emotes.command(hidden=True)
    @commands.is_owner()
    async def redownload(self, ctx):
        """
        Redownload the emotes from the interwebs.
        """
        await self.redownload_emotes()

    @emotes.command()
    async def search(self, ctx, text=None):
        """
        Show the top 20 emotes with text similar to a specific text

        Parameters
        ----------
        text : str
            the text you want the emotes to be similar to

        Example
        -------
        >>> !emotes search kapp
        Shows you the top 20 emotes similar to the text "kapp"
        """
        if text is None:
            await ctx.send("You need to specify an emote to look up!")
            return
        else:
            smolemotes = {}
            for i in self.emote_dict:
                if self.emote_dict[i] > 0:
                    if fuzz.ratio(text.lower(), i.lower()) >= 62:
                        smolemotes[i] = self.emote_dict[i]
                if text.lower() in i.lower() and i not in smolemotes:
                    smolemotes[i] = self.emote_dict[i]
            sorted_emotes = list(reversed(sorted(smolemotes.items(), key=operator.itemgetter(1))))
            answer = ""
            for i in sorted_emotes[:20]:
                answer = answer + i[0] + " "
            await ctx.send(answer)

    @emotes.command()
    async def top(self, ctx):
        """
        Show the top 20 emotes.
        """
        sorted_emotes = list(reversed(sorted(self.emote_dict.items(), key=operator.itemgetter(1))))
        answer = ""
        for i in sorted_emotes[:20]:
            answer = answer + i[0] + " "
        return await ctx.send(answer)

    @emotes.command()
    async def explore(self, ctx, text=None):
        """
        Show 10 random emotes containing a certain text

        Parameters
        ----------
        text : str
            the text you want the emotes to contain

        Example
        -------
        >>> !emotes explore kepp
        Shows you 20 random emotes containing the text "kepp"
        """
        rands = []
        emtlist = []
        ret = []
        # search without text
        if text is None:
            for i in range(20):
                num = randrange(len(self.emote_dict))
                while num in rands:
                    num = randrange(len(self.emote_dict))
                rands.append(num)
            k = 0
            for i in self.emote_dict:
                if k in rands:
                    ret.append(i)
                k += 1
        # wildcard search
        elif '*' in text:
            words = text.split('*')
            reg = r""
            smolemotes = []
            for w in words:
                if w == words[0]:
                    reg = reg + w.lower()
                else:
                    reg = reg + "\w*" + w.lower()
            for i in self.emote_dict:
                if len(re.findall(reg, i.lower())) > 0:
                    smolemotes.append(i)
            if len(smolemotes) > 20:
                for i in range(20):
                    num = randrange(len(smolemotes))
                    while num in rands:
                        num = randrange(len(smolemotes))
                    rands.append(num)
                for i in rands:
                    ret.append(smolemotes[i])
            else:
                ret = smolemotes
        # full-text search
        else:
            for i in self.emote_dict:
                if text.lower() in i.lower():
                    emtlist.append(i)
            if len(emtlist) > 20:
                for i in range(20):
                    num = randrange(len(emtlist))
                    while num in rands:
                        num = randrange(len(emtlist))
                    rands.append(num)
                for i in rands:
                    ret.append(emtlist[i])
            else:
                ret = emtlist
        if len(ret) == 0:
            await ctx.send("I couldn't find any emotes containing your text...")
        else:
            answer = ""
            for i in ret:
                answer = answer + " " + i
            await ctx.send(answer)

    @emotes.command()
    async def me(self, ctx):
        """
        Show your personal top 20 most used emotes [containing some specific text]

        Parameters
        ----------
        text : str
            The text you want the emotes to contain

        Example
        -------
        >>> !emotes me lu
        Shows your top 20 most used emotes containing the text "lu"
        """
        if str(ctx.author.id) in self.player_dict:
            sorted_emotes = list(reversed(sorted(self.player_dict[str(ctx.author.id)].items(), key=operator.itemgetter(1))))[:20]
            if len(sorted_emotes) < 1:
                await ctx.say('I don\'t have any emotes for you stored.')
                return
            else:
                answer = ""
                for emote in sorted_emotes:
                    answer += emote[0] + " "
                await ctx.send(answer)
        else:
            await ctx.send('I don\'t have any emotes for you stored.')

    # internal functions:
    def reward_emotes(self, message, author, permitted):
        """
        Add points to the emotes in the message if they pass the spam filter

        scan message for emotes
        rewards the emotes with point for each occurrence
        maximum 5 points per message to avoid spam

        Parameters
        ----------
        message : str
            the message that has been sent
        author : discord.Member
            the user who used the emote(s)
        permitted : bool
            True if the user allowed to store data about him
        """
        badwordlist = re.split('\s|\n', message)
        wordlist = []
        # filter out ""
        for w in badwordlist:
            if len(w) > 1:
                wordlist.append(w)

        normal_words = []  # list for all the non emote words in the message
        emotes = []  # list for all the emotes in the message
        descriptors = []  # list for all the descriptors in the message
        nocolonlist_all = []  # 'pre-list' for all words in the message, with filtered out descriptors

        # filter the descriptors
        for w in wordlist:
            if ":" in w:
                descriptor = w[w.index(":"):]
                if descriptor in self.descriptor_dict:
                    descriptors.append(descriptor)
                    rest = w[:w.index(":")]
                    if rest in self.emote_dict:
                        emotes.append(rest)
                    else:
                        nocolonlist_all.append(rest)
                else:
                    nocolonlist_all.append(w)
            else:
                nocolonlist_all.append(w)

        # populate emote list and normal word list
        for w in nocolonlist_all:
            if w in self.emote_dict:
                emotes.append(w)
            else:
                normal_words.append(w)

        distinct_emotes = list(set(emotes))
        distinct_descriptors = list(set(descriptors))

        if len(normal_words) > 0:
            ratio = len(emotes) / len(normal_words)
        else:
            # ratio one higher than the maximum ratio you could get in one message
            ratio = 400
        spam = False

        # test if message is spam:
        if len(distinct_emotes) > 5:
            if ratio > 2:
                spam = True
        elif ratio > 1.15:
            for w in distinct_emotes:
                occurrences = emotes.count(w)
                if occurrences > 5:
                    spam = True
            for d in distinct_descriptors:
                occurrences = descriptors.count(d)
                if occurrences > 5:
                    spam = True
        else:
            for w in distinct_emotes:
                occurrences = emotes.count(w)
                if occurrences > 9:
                    spam = True
            for d in distinct_descriptors:
                occurrences = descriptors.count(d)
                if occurrences > 9:
                    spam = True

        # actually reward the emotes
        if not spam:
            for w in distinct_emotes:
                # global:
                occurrences = emotes.count(w)
                self.emote_dict[w] += occurrences
                self.fileLogger.info(f'{w} has gained {occurrences} point{"" if occurrences == 1 else "s"}. '
                                     f'New points: {self.emote_dict[w]}')
                # player:
                if permitted:
                    if author not in self.player_dict:
                        self.player_dict[author] = {}
                    if w not in self.player_dict[author]:
                        self.player_dict[author][w] = 0
                    self.player_dict[author][w] += occurrences

            for d in distinct_descriptors:
                occurrences = descriptors.count(d)
                self.descriptor_dict[d] += occurrences
                self.fileLogger.info(f'{d} has gained {occurrences} point{"" if occurrences == 1 else "s"}. '
                                     f'New points: {self.descriptor_dict[d]}')
        else:
            self.logger.warning(f"Spam filter armed. The Spammer was: {author}")

    async def redownload_emotes(self):
        """
        Redownload the emote data

        saves the dictionaries to the files first
        redownloads all emotes from github and/or twitchemotes.com
        then reloads the dictionaries from the files

        Returns
        -------
        updatecount: int
            the number of new downloaded emotes
        """
        allcount = 0
        updatecount = 0
        # blacklist
        blacklist = []
        try:
            url = "https://raw.githubusercontent.com/Jiiks/BetterDiscordApp/master/data/emotefilter.json"
            async with self.bot.aiohttp_session.get(url) as resp:
                data = json.loads(await resp.text())
                blacklist = data["blacklist"]
                with open(BLACKLIST_PATH, "w") as fp:
                    yaml.safe_dump(data, fp)
                self.logger.info("Successfully updated blacklist")
        except:
            self.logger.info("Couldn't update blacklist")
            if os.path.isfile(BLACKLIST_PATH):
                with open(BLACKLIST_PATH, "r") as fp:
                    blacklist = yaml.safe_load(fp)["blacklist"]

        # emotes
        async with self.bot.aiohttp_session.get('https://twitchemotes.com/api_cache/v3/global.json') as resp:
            try:
                data = json.loads(await resp.text())
                for name in data:
                    allcount += 1
                    emote = data[name]["code"]
                    if emote not in self.emote_dict and emote not in blacklist:
                        self.emote_dict[emote] = 0
                        updatecount += 1
            except:
                self.logger.warning('Couldn\'t download new emotedata_global.json')
        async with self.bot.aiohttp_session.get('https://twitchemotes.com/api_cache/v3/subscriber.json'):
            try:
                data = json.loads(await resp.text())
                for channel in data:
                    for emotedata in data[channel]["emotes"]:
                        emote = emotedata["code"]
                        if emote not in self.emote_dict and emote not in blacklist:
                            self.emote_dict[emote] = 0
                            updatecount += 1
            except:
                self.logger.warning('Couldn\'t download new emotedata_subscriber.json')

        self.save_data()
        self.logger.info(f"successfully seen {allcount} emotes")
        self.logger.info(f"updated {updatecount} emotes")
        return updatecount

    def delete_userdata(self, discordid):
        """
        Delete all collected personalized data about a user

        Parameters
        ----------
            userid : str
                the user id of the user you want to delete the data from

        Returns
        -------
         1 : Deleted all personalized data
        -1 : No data about this user has been collected yet
        """
        if discordid in self.player_dict:
            del(self.player_dict[discordid])
            self.save_data()
            return 1
        else:
            return -1

    def load_data(self):
        """
        Load the data
        """
        if not os.path.isdir(PATH):
            os.makedirs(PATH)
            self.logger.warning(f'{PATH} directory not found, created new one.')
        if os.path.isfile(EMOTES_DATA_PATH):
            with open(EMOTES_DATA_PATH, "r") as pointscache:
                data = yaml.safe_load(pointscache)
                self.descriptor_dict = data["descriptors"]
                self.emote_dict = data["emotes"]
                self.player_dict = data["players"]
        else:
            self.save_data()
            self.logger.warning(f"Could not find {EMOTES_DATA_PATH}, created new one.")

    def save_data(self):
        """
        Save the data
        """
        with open(EMOTES_DATA_PATH, "w") as fp:
            yaml.safe_dump({"emotes": self.emote_dict,
                            "descriptors": self.descriptor_dict,
                            "players": self.player_dict}, fp)

    # loops:
    async def looped_save_data(self):
        """
        Save the current point cache to the file every 10 minutes
        """
        while self.do_loops:
            await asyncio.sleep(300)
            self.save_data()

    async def looped_redownload_emotes(self):
        """
        Redownload new emotes from github and/or twitchemotes.com every 24 hours
        """
        while self.do_loops:
            await asyncio.sleep(3600)
            if os.path.isfile("updatetime.txt"):
                with open("updatetime.txt", "r") as textfile:
                    old_time = float(textfile.readlines()[0])
            else:
                with open("updatetime.txt", "w") as fp:
                    fp.write(str(time.time()))
                    old_time = time.time()
                    self.logger.info("updatetime.txt not found, created new one with current time.")
            if time.time() - old_time > 86400:
                self.logger.info("*AUTOMATIC 24HRS RELOAD*")
                await self.redownload_emotes()
                with open("updatetime.txt", "w") as fp:
                    fp.write(str(time.time()))

    # events:
    @commands.Cog.listener()
    async def on_message(self, message):
        """
        Scan the message for emotes and rewards them.
        """
        if not message.author.bot and len(message.content) > 0 and message.content[0] != self.bot.command_prefix:
            self.reward_emotes(message.content, str(message.author.id), True)


async def setup(bot):
    await bot.add_cog(EmotesCog(bot))
