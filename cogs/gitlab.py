import logging
from collections.abc import Sequence
from typing import Any

import aiohttp
import discord
from discord.ext import commands
from pydantic import BaseModel

from bot import Bot

MAX_ISSUE_DESCRIPTION_LENGTH = 1_048_576


class GitLabError(Exception):
    pass


class IssueDescriptionTooLongError(GitLabError):
    pass


class NonExistantLabelsError(GitLabError):
    def __init__(self, labels: list[str], *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.labels = labels


class APIError(GitLabError):
    def __init__(self, status_code: int, raw_response_body: str = "", *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.status_code = status_code
        self.raw_response_body = raw_response_body


class IssueCommandFlags(commands.FlagConverter, delimiter=" ", prefix="--"):
    title: str
    description: str
    labels: list[str]


class GitLabLabel(BaseModel):
    id: int
    name: str


class GitLabProject(BaseModel):
    web_url: str


class CreateIssueResponse(BaseModel):
    id: int
    web_url: str
    description: str
    title: str
    labels: list[str]


def create_issue_embed(
    title: str, description: str, labels: Sequence[str], link: str
) -> discord.Embed:
    embed = discord.Embed(title=title, url=link, colour=discord.Colour.orange())
    embed.add_field(name="Description", value=description)
    embed.add_field(name="Labels", value=", ".join(f"`{label}`" for label in labels))
    return embed


class GitLab:
    def __init__(self, bot: Bot, logger):
        config = bot.config
        self.access_token = config.gitlab_personal_access_token
        self.base_url = "https://gitlab.com/api/v4"
        self.project_id = config.gitlab_project_id
        self.issue_assignee_id = config.gitlab_assignee_id
        self.session = bot.aiohttp_session
        self.labels = []
        self.logger = logger

    async def _assert_labels_exists(self, labels: Sequence[str]):
        if all(label in self.labels for label in labels):
            return
        # If not all labels exist, refresh *once* then return result
        await self.refresh_labels()
        if not all(label in self.labels for label in labels):
            raise NonExistantLabelsError([label for label in labels if label not in self.labels])

    async def refresh_labels(self):
        self.labels = await self._get_labels()

    async def _send_get(
        self, endpoint: str, params: dict[str, Any] | None = None
    ) -> aiohttp.ClientResponse:
        url = f"{self.base_url}{endpoint}"
        headers = {"PRIVATE-TOKEN": self.access_token}
        resp = await self.session.get(url, params=params, headers=headers)
        if not resp.ok:
            raise APIError(resp.status, await resp.text())
        return resp

    async def _send_post(
        self, endpoint: str, params: dict[str, Any] | None = None
    ) -> aiohttp.ClientResponse:
        url = f"{self.base_url}{endpoint}"
        headers = {"PRIVATE-TOKEN": self.access_token}
        resp = await self.session.post(url, params=params, headers=headers)
        if not resp.ok:
            raise APIError(resp.status, await resp.text())
        return resp

    async def _get_labels(self) -> list[str]:
        endpoint = f"/projects/{self.project_id}/labels"
        response = await self._send_get(endpoint)
        labels = [GitLabLabel(**element) for element in (await response.json())]
        return [label.name for label in labels]

    async def create_issue(
        self, title: str, description: str, labels: Sequence[str]
    ) -> CreateIssueResponse:
        if len(description) > MAX_ISSUE_DESCRIPTION_LENGTH:
            raise IssueDescriptionTooLongError
        await self._assert_labels_exists(
            labels
        )  # raises NonExistantLabelsError if not all labels exist
        endpoint = f"/projects/{self.project_id}/issues"
        params = {
            "assignee_id": self.issue_assignee_id,
            "description": description,
            "title": title,
            "labels": ",".join(labels),
        }
        resp = await self._send_post(endpoint, params=params)
        return CreateIssueResponse(**(await resp.json()))

    async def get_link(self) -> str:
        endpoint = f"/projects/{self.project_id}"
        resp = await self._send_get(endpoint)
        project = GitLabProject(**(await resp.json()))
        return project.web_url


class GitLabCog(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.gitlab = GitLab(bot, self.logger)

    @commands.group(aliases=["gitlab"])
    async def git(self, ctx: commands.Context):
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)
            await ctx.send(f"<{await self.gitlab.get_link()}>")

    @git.command(name="link")
    async def get_link(self, ctx: commands.Context):
        await ctx.send(f"<{await self.gitlab.get_link()}>")

    @git.command(name="labels")
    async def labels(self, ctx: commands.Context):
        """
        Show a list of labels available to the discord-bot GitLab project.
        """
        if len(self.gitlab.labels) == 0:
            await self.gitlab.refresh_labels()
        labels = ", ".join(f"`{label}`" for label in self.gitlab.labels)
        await ctx.send(f"Labels available to the discord-bot project are: {labels}.")

    @git.command(aliases=["issue"])
    @commands.is_owner()
    async def create_issue(self, ctx: commands.Context, title: str, description: str, *labels: str):
        """
        Create an issue on the GitLab tracker.

        Parameters
        ----------
        title : str
            The title of the new issue
        description : str
            The description of the new issue
        labels : str
            A space separated list of labels for the new issue.
        """
        issue = await self.gitlab.create_issue(title, description, labels)
        embed = create_issue_embed(issue.title, issue.description, issue.labels, issue.web_url)
        await ctx.send(f"Successfully created the following [issue]({issue.web_url}):", embed=embed)


async def setup(bot: Bot):
    await bot.add_cog(GitLabCog(bot))
