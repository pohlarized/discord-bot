import logging
import random
import sys
import traceback

import discord
from discord import app_commands
from discord.ext import commands

from bot import Bot

LEET = {"l": "1", "e": "3", "a": "4", "s": "5", "t": "7", "g": "9", "o": "0"}


def message_content_or_embed_desc(message: discord.Message) -> str:
    if message.content != "":
        return message.content
    if len(message.embeds) == 1:
        return message.embeds[0].description or ""
    return ""


def memefy_text(text: str) -> str:
    memewords = []
    words = text.strip().split(" ")
    letter_count = 0
    for word in words:
        memeletters = [
            letter.upper() if (i + letter_count) % 2 == 0 else letter.lower()
            for i, letter in enumerate(word)
        ]
        memewords.append("".join(memeletters))
        letter_count += len(memeletters)
    return " ".join(memewords)


def create_user_embed(author: discord.Member | discord.User, text):
    """
    Creates an embed with the users color, their avatar and name,
    and the 'text' parameter as description.
    """
    emb = discord.Embed(color=author.color)
    emb.set_author(name=str(author), icon_url=author.display_avatar.url)
    emb.description = text
    return emb


class TextTransformerCog(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        owo_ctx_menu = app_commands.ContextMenu(name="owo", callback=self.ctx_owo)
        memefy_ctx_menu = app_commands.ContextMenu(name="memefy", callback=self.ctx_memefy)
        self.bot.tree.add_command(memefy_ctx_menu)
        self.bot.tree.add_command(owo_ctx_menu)
        self.logger = logging.getLogger(__name__)

    async def ctx_memefy(self, interaction: discord.Interaction, message: discord.Message):
        text = message_content_or_embed_desc(message)
        memefied_text = memefy_text(text)
        await interaction.response.send_message(
            embed=create_user_embed(message.author, memefied_text)
        )

    @commands.hybrid_command()
    async def memefy(self, ctx: commands.Context, *, text: str):
        """
        Distort your message in a way, that every second letter is uppercase,
        and the other letters are lowercase.

        Parameters
        ----------
        text : str
            the text you want to memefy

        Example
        -------
        >>> !memefy henlo
        Says the message "HeNlO" to the channel the command was used in
        """
        response = memefy_text(text)
        await ctx.send(embed=create_user_embed(ctx.author, response))
        self.logger.info(
            "%s - %s memefied in channel %s - %s. Content: %s",
            ctx.author,
            ctx.author.id,
            ctx.channel,
            ctx.channel.id,
            response,
        )

    @memefy.error
    async def memefy_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send_help(ctx.command)
            await ctx.send("You need to specify a text to memefy!")
        else:
            print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command()
    async def monospace(self, ctx: commands.Context, *, text: str):
        default = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
        mono = "𝙰𝙱𝙲𝙳𝙴𝙵𝙶𝙷𝙸𝙹𝙺𝙻𝙼𝙽𝙾𝙿𝚀𝚁𝚂𝚃𝚄𝚅𝚆𝚇𝚈𝚉𝚊𝚋𝚌𝚍𝚎𝚏𝚐𝚑𝚒𝚓𝚔𝚕𝚖𝚗𝚘𝚙𝚚𝚛𝚜𝚝𝚞𝚟𝚠𝚡𝚢𝚣𝟷𝟸𝟹𝟺𝟻𝟼𝟽𝟾𝟿𝟶"
        new_text = ""
        for letter in text:
            if letter in default:
                new_text += mono[default.index(letter)]
            else:
                new_text += letter
        return await ctx.send(new_text)

    @commands.command(name="1337")
    async def leet_command(self, ctx: commands.Context, *, text: str | None = None):
        """
        1337ify
        """
        if text is None:
            await ctx.send("1337")
            return
        for char, digit in LEET.items():
            text = text.replace(char, digit).replace(char.upper(), digit)
        emb = create_user_embed(ctx.author, text)
        await ctx.send(embed=emb)
        self.logger.info(
            "%s - %s 1337d in channel %s - %s. Content: %s",
            ctx.author,
            ctx.author.id,
            ctx.channel,
            ctx.channel.id,
            text,
        )

    @commands.command()
    async def swedishpirateifybutenglish(self, ctx: commands.Context, *, text: str | None = None):
        """
        ???
        """
        if text is None:
            await ctx.send("inoncocororrorecoctot usosagoge")
            return
        buf = ""
        if text.isupper():
            for letter in text:
                if letter in "bcdfghjklmnpqrstvwxyz".upper():
                    buf += f"{letter}O{letter}"
                else:
                    buf += letter
        else:
            for letter in text:
                if letter in "bcdfghjklmnpqrstvwxyz":
                    buf += f"{letter}o{letter}"
                elif letter in "bcdfghjklmnpqrstvwxyz".upper():
                    buf += f"{letter}o{letter.lower()}"
                else:
                    buf += letter
        await ctx.send(embed=create_user_embed(ctx.author, buf))
        self.logger.info(
            "%s - %s swedishpirateified in channel %s - %s. Content: %s",
            ctx.author,
            ctx.author.id,
            ctx.channel,
            ctx.channel.id,
            buf,
        )

    @commands.command()
    async def embolden(self, ctx: commands.Context, *, text: str):
        """
        Rewrite your message as :regional_indicator_*: emotes

        Parameters
        ----------
        text : str
            the text you want to embolden

        Example
        -------
        >>> !embolden this is very important
        Sends an embed with "this is very important" written in regional
        indicator emotes
        """
        d = {
            "0": ":zero:",
            "1": ":one:",
            "2": ":two:",
            "3": ":three:",
            "4": ":four:",
            "5": ":five:",
            "6": ":six:",
            "7": ":seven:",
            "8": ":eight:",
            "9": ":nine:",
            "#": ":hash:",
        }
        buf = ""
        alphabet = "abcdefghijklmnopqrstuvwxyz"
        text = text.lower()
        for letter in text:
            if letter in alphabet:
                buf += f":regional_indicator_{letter}:"
            elif letter in d:
                buf += d[letter]
            else:
                buf += letter
        await ctx.send(embed=create_user_embed(ctx.author, buf))
        self.logger.info(
            "%s - %s emboldened in channel %s - %s. Original content: %s",
            ctx.author,
            ctx.author.id,
            ctx.channel,
            ctx.channel.id,
            text,
        )

    @commands.command(aliases=["emboldenc"])
    async def embolden_copyable(self, ctx: commands.Context, *, text: str):
        """
        Rewrite your message as :regional_indicator_*: emotes

        Puts spaces inbetween the emotes, and double spaces inbetween words
        so you can copy it without the discord client turning it into
        country flags.

        Parameters
        ----------
        text : str
            the text you want to embolden

        Example
        -------
        >>> !embolden this is very important
        Sends an embed with "this is very important" written in regional
        indicator emotes
        """
        d = {
            "0": ":zero: ",
            "1": ":one: ",
            "2": ":two: ",
            "3": ":three: ",
            "4": ":four: ",
            "5": ":five: ",
            "6": ":six: ",
            "7": ":seven: ",
            "8": ":eight: ",
            "9": ":nine: ",
            "#": ":hash: ",
        }
        buf = ""
        alphabet = "abcdefghijklmnopqrstuvwxyz"
        text = text.lower()
        for letter in text:
            if letter in alphabet:
                buf += f":regional_indicator_{letter}: "
            elif letter in d:
                buf += d[letter]
            else:
                buf += letter
        await ctx.send(embed=create_user_embed(ctx.author, buf))
        self.logger.info(
            "%s - %s emboldenCd in channel %s - %s. Original Content: %s",
            ctx.author,
            ctx.author.id,
            ctx.channel,
            ctx.channel.id,
            text,
        )

    @commands.command()
    async def reverse(self, ctx: commands.Context, *, text: str):
        """
        Reverse the text you give it

        Parameters
        ----------
        text : str
            the text you want to revers

        Example
        -------
        >>> !reverse JabKing
        Says "gniKbaJ"
        """
        emb = discord.Embed(color=ctx.author.color)
        emb.set_author(name=str(ctx.author), icon_url=ctx.author.display_avatar.url)
        emb.description = text[::-1]
        await ctx.send(embed=emb)
        self.logger.info(
            "%s - %s reversed in channel %s - %s. Content: %s",
            ctx.author,
            ctx.author.id,
            ctx.channel,
            ctx.channel.id,
            text[::-1],
        )

    async def ctx_owo(self, interaction: discord.Interaction, message: discord.Message):
        text = message_content_or_embed_desc(message)
        max_stutters = 3  # 1 = 10%
        min_stutters = 0
        emojis = ["OwO", "omo", "umu", "UwU", ">///<", ":3", ">:3", "~", "X3"]
        # basic owoing
        text = text.replace("R", "W").replace("r", "w")
        text = text.replace("L", "W").replace("l", "w")
        # d-d-did i s-stutter?
        words = text.split(" ")
        word_amount = sum([1 for word in words if word.isalpha() and len(word) > 1])
        stutters = random.randint(min_stutters, max_stutters)
        stutters = int((stutters * 0.1) * word_amount) + 1
        stutters = 0 if word_amount == 0 else stutters
        stutter_pos = []
        while len(stutter_pos) != stutters:
            pos = random.randint(0, len(words) - 1)
            if words[pos].isalpha() and len(words[pos]) > 1 and pos not in stutter_pos:
                stutter_pos.append(pos)

        for x in stutter_pos:
            words[x] = words[x][0] + "-" + words[x]

        # select random emoji from list of emojis
        emoji = random.choice(emojis)
        # determine start or end
        pos = random.randint(0, 3)
        if not pos:
            words = [emoji] + words
        else:
            words += [emoji]

        # blush
        do_we_blush = random.randint(0, 3)
        if not do_we_blush:
            pos = random.randint(0, len(words) - 1)
            words = words[:pos] + ["*blushes*"] + words[pos:]

        text = " ".join(words)
        emb = create_user_embed(message.author, text)
        await interaction.response.send_message(embed=emb)

    @commands.command()
    async def owo(self, ctx: commands.Context, *, text: str):
        """
        OwO whats this
        """
        max_stutters = 3  # 1 = 10%
        min_stutters = 0
        emojis = ["OwO", "omo", "umu", "UwU", ">///<", ":3", ">:3", "~", "X3"]
        # basic owoing
        text = text.replace("R", "W").replace("r", "w")
        text = text.replace("L", "W").replace("l", "w")
        # d-d-did i s-stutter?
        words = text.split(" ")
        word_amount = sum([1 for word in words if word.isalpha() and len(word) > 1])
        stutters = random.randint(min_stutters, max_stutters)
        stutters = int((stutters * 0.1) * word_amount) + 1
        stutters = 0 if word_amount == 0 else stutters
        stutter_pos = []
        while len(stutter_pos) != stutters:
            pos = random.randint(0, len(words) - 1)
            if words[pos].isalpha() and len(words[pos]) > 1 and pos not in stutter_pos:
                stutter_pos.append(pos)

        for x in stutter_pos:
            words[x] = words[x][0] + "-" + words[x]

        # select random emoji from list of emojis
        emoji = random.choice(emojis)
        # determine start or end
        pos = random.randint(0, 3)
        if not pos:
            words = [emoji] + words
        else:
            words += [emoji]

        # blush
        do_we_blush = random.randint(0, 3)
        if not do_we_blush:
            pos = random.randint(0, len(words) - 1)
            words = words[:pos] + ["*blushes*"] + words[pos:]

        text = " ".join(words)
        emb = create_user_embed(ctx.author, text)
        await ctx.send(embed=emb)
        self.logger.info(
            "%s - %s owoed in channel %s - %s. Content: %s",
            ctx.author,
            ctx.author.id,
            ctx.channel,
            ctx.channel.id,
            text,
        )


async def setup(bot: Bot):
    await bot.add_cog(TextTransformerCog(bot))
