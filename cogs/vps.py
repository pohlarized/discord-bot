import logging
from collections.abc import Sequence

import psutil
from discord.ext import commands
from sqlalchemy import select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.vps import VPS_SCHEMA_NAME, Base, VpsService
from utils.paginate import paginate_codeblock


class VPSCog(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    async def get_services(self, session: AsyncSession) -> Sequence[VpsService]:
        async with session.begin():
            return (await session.execute(select(VpsService))).scalars().all()

    async def create_service(
        self, session: AsyncSession, service_name: str, description: str, connect_info: str
    ):
        async with session.begin():
            session.add(
                VpsService(name=service_name, description=description, connect_info=connect_info)
            )

    async def remove_service(self, session: AsyncSession, service_name: str) -> bool:
        """
        Returns
        -------
        bool
            True if service existed, else False
        """
        stmt = select(VpsService).where(VpsService.name == service_name)
        async with session.begin():
            service = (await session.execute(stmt)).scalars().first()
        if service is None:
            return False
        async with session.begin():
            await session.delete(service)
        return True

    @commands.group(aliases=["server"])
    async def vps(self, ctx: commands.Context):
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @vps.command()
    async def stats(self, ctx: commands.Context):
        """
        Shows you CPU and GPU statistics of the server.
        """
        async with ctx.channel.typing():
            cpu = psutil.cpu_percent(interval=2)
            mem = psutil.virtual_memory()
            overall_mem_gb = mem.total / 1e9
            used_mem_gb = mem.used / 1e9
        await ctx.send(
            f"CPU-Load: {cpu:.2f}%\t{used_mem_gb:.2f}GB of {overall_mem_gb:.2f}GB RAM in use."
        )

    @vps.command(aliases=["add"])
    async def add_server(
        self, ctx: commands.Context, name: str, connect_info: str, *, description: str
    ):
        """
        Add a gameserver to the list of servers

        Parameters
        ----------
        name: str
            the name of the server you want to add

        connect_info: str
            The information required to connect to the service (e.g. IP + Port)

        description: str (optional)
            a *short* description of the server

        Example
        -------
        >>> !vps add "minecraft vanilla" 1338 JabKingdom minecraft server
        Adds a server with the name `minecraft vanilla` thats running on port
        1338 and has the description `JabKingdom minecraft server`
        """
        async with db.async_sessionmaker() as session:
            try:
                await self.create_service(session, name, description, connect_info)
            except SQLAlchemyError:
                await ctx.send_help(ctx.command)
                await ctx.send("A gameserver with that name already exists")
                return
        await ctx.send(f"Successfully added {name} to the list of gameservers.")
        self.logger.info(
            "%s - %i added server %s to the list of gameservers.", ctx.author, ctx.author.id, name
        )

    @vps.command(aliases=["remove", "delete", "rm", "del"])
    async def remove_server(self, ctx: commands.Context, *, name: str):
        """
        Remove a gameserver from the list of servers

        Parameters
        ----------
        name: str
            the name of the server you want to add

        Example
        -------
        >>> !vps remove "minecraft vanilla"
        Removes the server with the name `minecraft vanilla` if it exists.
        """
        async with db.async_sessionmaker() as session:
            deleted = await self.remove_service(session, name)
        if not deleted:
            await ctx.send_help(ctx.command)
            await ctx.send("That server does not exist.")
            return
        await ctx.send(f"Successfully removed {name} from the list of gameservers.")
        self.logger.info(
            "%s - %i removed server %s from the list of gameservers.",
            ctx.author,
            ctx.author.id,
            name,
        )

    @vps.command(aliases=["list"])
    async def list_servers(self, ctx: commands.Context):
        """List all gameservers running on the VPS"""
        async with db.async_sessionmaker() as session:
            services = await self.get_services(session)
        table = "\n\n".join(
            f"Name: {service.name}\nDescription: {service.description}\n"
            f"Connect Info: {service.connect_info}"
            for service in services
        )
        message = (
            (
                f"**List of services on the VPS**\tDomain: `{self.bot.config.vps_domain}`\t"
                f"IP: `{self.bot.config.vps_ip}`\n```\n"
            )
            + table
            + "\n```"
        )
        for message_block in paginate_codeblock(message):
            await ctx.send(message_block)


async def setup(bot: Bot):
    await db.create_schema(VPS_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(VPSCog(bot))
