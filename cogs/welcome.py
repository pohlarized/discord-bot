import logging

import discord
from discord.ext import commands

from bot import Bot
from utils.errors import KnownChannelNotFoundError, UnexpectedChannelTypeError


class WelcomeCog(commands.Cog):
    """
    Cog to welcome and farewell people

    Simply sends a little welcome message when a user joins,
    ans a farewell message when a user leaves.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        """
        Greet new joining members in #tavern
        """
        if member.guild.id != self.bot.config.ids.guild_id:
            return
        msg = f"{member.mention}, Welcome to **The JabKingdom**!\nHave a great time here :wink:"
        welcome_channel = member.guild.get_channel(self.bot.config.ids.tavern_id)
        if welcome_channel is None:
            raise KnownChannelNotFoundError(self.bot.config.ids.tavern_id)
        if not isinstance(welcome_channel, discord.TextChannel):
            raise UnexpectedChannelTypeError(welcome_channel.id, type(welcome_channel))
        await welcome_channel.send(msg)
        self.logger.info("%s - %s just joined the kingdom!", member, member.id)

    @commands.Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        """
        Send goodbye message to #tavern if a member leaves
        """
        if member.guild.id != self.bot.config.ids.guild_id:
            return
        msg = f"**{member}** just left the Kingdom. Bye Bye **{member}**..."
        channel = member.guild.get_channel(self.bot.config.ids.tavern_id)
        if channel is None:
            raise KnownChannelNotFoundError(self.bot.config.ids.tavern_id)
        if not isinstance(channel, discord.TextChannel):
            raise UnexpectedChannelTypeError(channel.id, type(channel))
        await channel.send(msg)
        self.logger.info("%s - %s just left the kingdom!", member, member.id)


async def setup(bot: Bot):
    await bot.add_cog(WelcomeCog(bot))
