import datetime
import logging

import discord
from discord import (
    ChannelType,
    Member,
    Message,
    RawMessageDeleteEvent,
    RawMessageUpdateEvent,
    RawReactionActionEvent,
    Thread,
    VoiceState,
)
from discord.ext import commands
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db import to_db_datetime
from db.discord_rewind import (
    DISCORD_REWIND_SCHEMA_NAME,
    ApiKey,
    Base,
    Channel,
    Mention,
    MessageDelete,
    MessageEdit,
    MessageSend,
    ReactionAdd,
    StreamSession,
    ThreadCreate,
    VoiceSession,
)
from db.discord_rewind import Member as DBMember


class DiscordRewind(commands.Cog):
    """
    Cog to show you stats about your recent behaviour.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    async def cog_load(self):
        if self.bot.is_ready():
            async with db.async_sessionmaker() as session:
                await self.handle_session_changes(session)

    async def handle_session_changes(self, session: AsyncSession):
        await self.start_unnoticed_voice_sessions(session)
        await self.start_unnoticed_stream_sessions(session)
        await self.end_pending_voice_sessions(session)
        await self.end_pending_stream_sessions(session)

    async def get_or_create_api_key(self, session: AsyncSession, user_id: int) -> ApiKey:
        stmt = select(ApiKey).where(ApiKey.user_id == user_id)
        async with session.begin():
            api_key = (await session.execute(stmt)).scalars().first()
        if api_key is None:
            async with session.begin():
                api_key = ApiKey.generate(user_id)
                session.add(api_key)
        return api_key

    async def member_in_voice_channel(self, member_id: int, channel_id: int) -> bool:
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            return False
        member = guild.get_member(member_id)
        if member is None:
            return False
        voice_state = member.voice
        if voice_state is None:
            return False
        return voice_state.channel is not None and voice_state.channel.id == channel_id

    async def member_streaming(self, member_id: int, channel_id: int) -> bool:
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            return False
        member = guild.get_member(member_id)
        if member is None:
            return False
        voice_state = member.voice
        if voice_state is None:
            return False
        return (
            voice_state.self_stream
            and voice_state.channel is not None
            and voice_state.channel.id == channel_id
        )

    async def has_active_voice_session(
        self, session: AsyncSession, member_id: int, channel_id: int
    ) -> bool:
        stmt = select(VoiceSession).where(
            VoiceSession.user_id == member_id,
            VoiceSession.channel_id == channel_id,
            VoiceSession.end == None,  # noqa: E711
        )
        async with session.begin():
            return (await session.execute(stmt)).first() is not None

    async def has_active_stream_session(
        self, session: AsyncSession, member_id: int, channel_id: int
    ) -> bool:
        stmt = select(StreamSession).where(
            StreamSession.user_id == member_id,
            StreamSession.channel_id == channel_id,
            StreamSession.end == None,  # noqa: E711
        )
        async with session.begin():
            return (await session.execute(stmt)).first() is not None

    async def start_unnoticed_voice_sessions(self, session: AsyncSession):
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            return
        for voice_channel in guild.voice_channels:
            for member in voice_channel.members:
                if not (await self.has_active_voice_session(session, member.id, voice_channel.id)):
                    voice_session = VoiceSession(
                        user_id=member.id,
                        channel_id=voice_channel.id,
                        start=db.datetime_now(),
                        end=None,
                        end_uncertain=False,
                        start_uncertain=True,
                    )
                    async with session.begin():
                        session.add(voice_session)
                    self.logger.warning(
                        "Starting unnoticed voice session for user %i in channel %i.",
                        voice_session.user_id,
                        voice_session.channel_id,
                    )

    async def start_unnoticed_stream_sessions(self, session: AsyncSession):
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            return
        for voice_channel in guild.voice_channels:
            for member in voice_channel.members:
                if (
                    member.voice
                    and member.voice.self_stream
                    and not (
                        await self.has_active_stream_session(session, member.id, voice_channel.id)
                    )
                ):
                    stream_session = StreamSession(
                        user_id=member.id,
                        channel_id=voice_channel.id,
                        start=db.datetime_now(),
                        end=None,
                        end_uncertain=False,
                        start_uncertain=True,
                    )
                    async with session.begin():
                        session.add(stream_session)
                    self.logger.warning(
                        "Starting unnoticed stream session for user %i in channel %i.",
                        stream_session.user_id,
                        stream_session.channel_id,
                    )

    async def end_pending_voice_sessions(self, session: AsyncSession):
        stmt = select(VoiceSession).where(VoiceSession.end == None)  # noqa: E711
        async with session.begin():
            pending_sessions = (await session.execute(stmt)).scalars().all()
        async with session.begin():
            for pending_session in pending_sessions:
                if not (
                    await self.member_in_voice_channel(
                        pending_session.user_id, pending_session.channel_id
                    )
                ):
                    pending_session.end_uncertain = True
                    pending_session.end = db.datetime_now()
                    self.logger.warning(
                        "Ended pending voice session for user %i in channel %i",
                        pending_session.user_id,
                        pending_session.channel_id,
                    )
            session.add_all(pending_sessions)

    async def end_pending_stream_sessions(self, session: AsyncSession):
        stmt = select(StreamSession).where(StreamSession.end == None)  # noqa: E711
        async with session.begin():
            pending_sessions = (await session.execute(stmt)).scalars().all()
        async with session.begin():
            for pending_session in pending_sessions:
                if not (
                    await self.member_streaming(pending_session.user_id, pending_session.channel_id)
                ):
                    pending_session.end_uncertain = True
                    pending_session.end = db.datetime_now()
                    self.logger.warning(
                        "Ended pending stream session for user %i in channel %i",
                        pending_session.user_id,
                        pending_session.channel_id,
                    )
            session.add_all(pending_sessions)

    async def end_pending_voice_session_for_user(
        self, session: AsyncSession, member_id: int, timestamp: datetime.datetime
    ):
        stmt = select(VoiceSession).where(
            VoiceSession.user_id == member_id,
            VoiceSession.end == None,  # noqa: E711
        )
        async with session.begin():
            results = list((await session.execute(stmt)).scalars().all())
        async with session.begin():
            for result in results:
                result.end_uncertain = True
                result.end = timestamp
                self.logger.warning(
                    "Ended pending voice session for user %i in channel %i",
                    member_id,
                    result.channel_id,
                )
            session.add_all(results)

    async def end_pending_stream_session_for_user(
        self, session: AsyncSession, member_id: int, timestamp: datetime.datetime
    ):
        stmt = select(StreamSession).where(
            StreamSession.user_id == member_id,
            StreamSession.end == None,  # noqa: E711
        )
        async with session.begin():
            results = list((await session.execute(stmt)).scalars().all())
        async with session.begin():
            for result in results:
                result.end_uncertain = True
                result.end = timestamp
                self.logger.warning(
                    "Ended pending stream session for user %i in channel %i",
                    member_id,
                    result.channel_id,
                )
            session.add_all(results)

    async def voice_connected(
        self, session: AsyncSession, member_id: int, channel_id: int, timestamp: datetime.datetime
    ):
        await self.end_pending_voice_session_for_user(session, member_id, timestamp)
        voice_session = VoiceSession(
            user_id=member_id,
            channel_id=channel_id,
            start=timestamp,
            end=None,
            end_uncertain=False,
            start_uncertain=False,
        )
        async with session.begin():
            session.add(voice_session)

    async def voice_disconnected(
        self, session: AsyncSession, member_id: int, channel_id: int, timestamp: datetime.datetime
    ):
        stmt = select(VoiceSession).where(
            VoiceSession.user_id == member_id,
            VoiceSession.channel_id == channel_id,
            VoiceSession.end == None,  # noqa: E711
        )
        async with session.begin():
            results = list((await session.execute(stmt)).scalars().all())
        end_uncertain = False
        async with session.begin():
            if len(results) > 1:
                self.logger.warning(
                    "Found more than one started voice session for member %i.", member_id
                )
                end_uncertain = True
            elif len(results) == 0:
                self.logger.warning(
                    "User %i disconnected from voice without active session.", member_id
                )
            for result in results:
                result.end = timestamp
                result.end_uncertain = end_uncertain
            session.add_all(results)

    async def streamer_online(
        self, session: AsyncSession, member_id: int, channel_id: int, timestamp: datetime.datetime
    ):
        await self.end_pending_stream_session_for_user(session, member_id, timestamp)
        stream_session = StreamSession(
            user_id=member_id,
            channel_id=channel_id,
            start=timestamp,
            end=None,
            end_uncertain=False,
            start_uncertain=False,
        )
        async with session.begin():
            session.add(stream_session)

    async def streamer_offline(
        self, session: AsyncSession, member_id: int, channel_id: int, timestamp: datetime.datetime
    ):
        stmt = select(StreamSession).where(
            StreamSession.user_id == member_id,
            StreamSession.channel_id == channel_id,
            StreamSession.end == None,  # noqa: E711
        )
        async with session.begin():
            results = list((await session.execute(stmt)).scalars().all())
        end_uncertain = False
        async with session.begin():
            if len(results) > 1:
                self.logger.warning(
                    "Found more than one started stream session for member %i.", member_id
                )
                end_uncertain = True
            elif len(results) == 0:
                self.logger.warning("User %i ended their stream without active session.", member_id)
            for result in results:
                result.end = timestamp
                result.end_uncertain = end_uncertain
            session.add_all(results)

    @commands.command(name="rewind")
    @commands.is_owner()
    async def create_link(self, ctx: commands.Context):
        async with db.async_sessionmaker() as session:
            api_key_model = await self.get_or_create_api_key(session, ctx.author.id)
        await ctx.send(f"https://jabkindom.town/rewind/{api_key_model.urlsafe_key}")

    @commands.command()
    @commands.is_owner()
    async def get_metadata(self, ctx: commands.Context):
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            await ctx.send("Failed retrieving the guild.")
            return
        channels = []
        threads = []
        for channel in guild.channels:
            channels.append(
                Channel(
                    id=channel.id,
                    guild_id=channel.guild.id,
                    parent_id=channel.category.id if channel.category else None,
                    name=channel.name,
                    type=channel.type,
                )
            )
            if channel.type == ChannelType.text:
                try:
                    raw_threads = channel.threads
                    async for thread in channel.archived_threads(limit=None):
                        raw_threads.append(thread)
                    threads += [
                        Channel(
                            id=thread.id,
                            guild_id=thread.guild.id,
                            parent_id=thread.parent_id,
                            name=thread.name,
                            type=thread.type,
                        )
                        for thread in raw_threads
                    ]
                except discord.Forbidden:
                    pass
        members = [
            DBMember(
                id=member.id,
                username=member.name,
                display_name=member.display_name,
                avatar_url=member.display_avatar.url,
            )
            for member in guild.members
        ]
        async with db.async_sessionmaker() as session:
            async with session.begin():
                await session.execute(delete(Channel))
                await session.execute(delete(DBMember))
            async with session.begin():
                session.add_all(members)
                session.add_all(channels)
                session.add_all(threads)
        await ctx.send(
            f"Successfully gathered {len(members)} members, {len(channels)} channels and "
            f"{len(threads)} threads."
        )

    @commands.Cog.listener()
    async def on_message(self, message: Message):
        if message.author.bot:
            return
        mentions = [user.id for user in message.mentions]
        async with db.async_sessionmaker() as session:
            message_send = MessageSend(
                id=message.id,
                author_id=message.author.id,
                channel_id=message.channel.id,
                timestamp=message.created_at.replace(tzinfo=None),
                content=message.content,
            )
            mentions = [
                Mention(message_id=message.id, mentioned_id=user.id) for user in message.mentions
            ]
            async with session.begin():
                session.add(message_send)
            async with session.begin():
                session.add_all(mentions)

    @commands.Cog.listener()
    async def on_raw_message_edit(self, payload: RawMessageUpdateEvent):
        if "edited_timestamp" not in payload.data:
            # Sometimes edit events are called e.g. due to thread creation, but they're not
            # interesting for us here.
            return
        if not isinstance(payload.data["edited_timestamp"], str):
            self.logger.warning(
                "Failed to save edited message, edited timestamp %s has type %s",
                payload.data["edited_timestamp"],
                type(payload.data["edited_timestamp"]),
            )
            return
        async with db.async_sessionmaker() as session:
            timestamp = to_db_datetime(
                datetime.datetime.fromisoformat(payload.data["edited_timestamp"])
            )
            message_edit = MessageEdit(
                author_id=int(payload.data["author"]["id"]),
                message_id=payload.message_id,
                channel_id=payload.channel_id,
                timestamp=timestamp,
                new_content=payload.data["content"],
            )
            async with session.begin():
                session.add(message_edit)

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: RawMessageDeleteEvent):
        async with db.async_sessionmaker() as session:
            message_delete = MessageDelete(
                author_id=payload.cached_message.author.id
                if payload.cached_message is not None
                else None,
                message_id=payload.message_id,
                channel_id=payload.channel_id,
                timestamp=db.datetime_now(),
            )
            async with session.begin():
                session.add(message_delete)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: RawReactionActionEvent):
        async with db.async_sessionmaker() as session:
            reaction_add = ReactionAdd(
                author_id=payload.user_id,
                message_id=payload.message_id,
                channel_id=payload.channel_id,
                emoji_id=payload.emoji.id,
                emoji_name=payload.emoji.name,
                timestamp=db.datetime_now(),
            )
            async with session.begin():
                session.add(reaction_add)

    @commands.Cog.listener()
    async def on_voice_state_update(self, member: Member, before: VoiceState, after: VoiceState):
        timestamp = db.datetime_now()
        async with db.async_sessionmaker() as session:
            # Voice channel joins
            if before.channel != after.channel:
                if before.channel is not None:
                    await self.voice_disconnected(session, member.id, before.channel.id, timestamp)
                if after.channel is not None:
                    await self.voice_connected(session, member.id, after.channel.id, timestamp)

            # Streaming
            if (
                before.self_stream
                and before.channel
                and (not after.self_stream or after.channel is None)
            ):
                await self.streamer_offline(session, member.id, before.channel.id, timestamp)
            elif not before.self_stream and after.self_stream and after.channel:
                await self.streamer_online(session, member.id, after.channel.id, timestamp)

    @commands.Cog.listener()
    async def on_thread_create(self, thread: Thread):
        if thread.created_at is None:
            return
        async with db.async_sessionmaker() as session:
            thread_create = ThreadCreate(
                creator_id=thread.owner_id,
                thread_id=thread.id,
                channel_id=thread.parent_id,
                timestamp=thread.created_at.replace(tzinfo=None),
            )
            async with session.begin():
                session.add(thread_create)

    @commands.Cog.listener()
    async def on_ready(self):
        async with db.async_sessionmaker() as session:
            await self.handle_session_changes(session)

    @commands.Cog.listener()
    async def on_resumed(self):
        async with db.async_sessionmaker() as session:
            await self.handle_session_changes(session)


async def setup(bot: Bot):
    await db.create_schema(DISCORD_REWIND_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(DiscordRewind(bot))
