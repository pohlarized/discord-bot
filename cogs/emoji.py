import logging
from collections.abc import Sequence

from discord import Emoji, Guild, TextChannel
from discord.abc import Messageable
from discord.ext import commands

from bot import Bot
from utils.errors import KnownChannelNotFoundError, UnexpectedChannelTypeError


class EmojiUpdateCog(commands.Cog):
    """
    Cog to notify about emoji updates
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger: logging.Logger = logging.getLogger(__name__)

    @commands.Cog.listener()
    async def on_guild_emojis_update(self, guild: Guild, before: list[Emoji], after: list[Emoji]):
        """
        Send a message when the emojis are updated
        """

        async def send_update(
            emojis: Sequence[tuple[str, Emoji | None]], channel: Messageable, msg: str
        ):
            if len(emojis) == 0:
                return
            for emoji_update, emoji in emojis:
                msg += f'- {emoji if emoji is not None else ""} `{emoji_update}`'
            self.logger.info(msg)
            await channel.send(msg)

        if guild.id != self.bot.config.ids.guild_id:
            return

        renamed = [
            (f"{before[before.index(e)].name} -> {e.name}", e)
            for e in after
            if e in before and e.name != before[before.index(e)].name
        ]
        removed = [(e.name, None) for e in set(before) - set(after)]
        added = [(e.name, e) for e in set(after) - set(before)]
        channel = self.bot.get_channel(self.bot.config.ids.bot_channel_id)
        if channel is None:
            raise KnownChannelNotFoundError(self.bot.config.ids.bot_channel_id)
        if not isinstance(channel, TextChannel):
            raise UnexpectedChannelTypeError(channel.id, type(channel))
        await send_update(added, channel, msg="New emotes have arrived!\n")
        await send_update(removed, channel, msg="These emotes are not worthy of the kingdom:\n")
        await send_update(renamed, channel, msg="These emotes names were refined:\n")


async def setup(bot: Bot):
    await bot.add_cog(EmojiUpdateCog(bot))
