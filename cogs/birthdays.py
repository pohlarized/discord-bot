from __future__ import annotations

import datetime
import logging
import sys
import traceback
from typing import TYPE_CHECKING

from discord import TextChannel
from discord.ext import commands, tasks
from sqlalchemy import select

import db
from db.birthdays import BIRTHDAYS_SCHEMA_NAME, Base, Birthday, Congratulation
from db.locale import Locale
from utils.errors import KnownChannelNotFoundError, UnexpectedChannelTypeError

if TYPE_CHECKING:
    from collections.abc import Sequence

    from sqlalchemy.ext.asyncio import AsyncSession

    from bot import Bot

MONTHS = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December",
}


class Date:
    def __init__(self, day: int, month: int):
        self.day = day
        self.month = month

    @classmethod
    async def convert(cls, _ctx: commands.Context, argument: str) -> Date:
        if len(argument) != 4 or not argument.isdigit():  # noqa: PLR2004 format is MMDD = 4 digits
            raise InvalidDateError(argument)
        month = int(argument[:2])
        day = int(argument[2:])
        if not (1 <= day <= 31) or not (1 <= month <= 12):  # noqa: PLR2004 days in month / months in year
            raise InvalidDateError(argument)
        return Date(day, month)


class InvalidDateError(commands.BadArgument):
    def __init__(self, value):
        self.value = value
        super().__init__(f"The value {value} is not a valid date in MMDD format!")


class BirthdayCog(commands.Cog):
    """
    Cog for birthday commands

    Allows users to store their birthdays and timezones,
    then the bot will 'congratulate' the person in the
    defined channel ~close to 00:00 in their timezone.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.restore_birthday_task.start()
        self.check_birthday_task.start()

    async def cog_unload(self):
        self.restore_birthday_task.cancel()
        self.check_birthday_task.cancel()

    async def get_locale(self, session: AsyncSession, user_id: int) -> Locale | None:
        stmt = select(Locale).where(Locale.id == user_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def get_uncongratulated_birthdays(self, session: AsyncSession) -> Sequence[Birthday]:
        async with session.begin():
            birthdays = (await session.execute(select(Birthday))).scalars().all()
        async with session.begin():
            stmt = select(Congratulation.user_id).where(Congratulation.congratulated_today == True)  # noqa: E712 required by sqlalchemy
            today_congratulations = (await session.execute(stmt)).scalars().all()
        return [birthday for birthday in birthdays if birthday.user_id not in today_congratulations]

    async def get_birthday(self, session: AsyncSession, user_id: int) -> Birthday | None:
        stmt = select(Birthday).where(Birthday.user_id == user_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def add_congratulation(self, session: AsyncSession, birthday: Birthday):
        stmt = select(Congratulation).where(Congratulation.user_id == birthday.user_id)
        async with session.begin():
            congratulation = (await session.execute(stmt)).scalars().first()
        async with session.begin():
            if congratulation is None:
                congratulation = Congratulation(user_id=birthday.user_id, congratulated_today=True)
            else:
                congratulation.congratulated_today = True
            session.add(congratulation)

    async def get_active_congratulations(self, session: AsyncSession) -> Sequence[Congratulation]:
        stmt = select(Congratulation).where(Congratulation.congratulated_today == True)  # noqa: E712 required by sqlalchemy
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def uncongratulate(self, session: AsyncSession, congratulation: Congratulation):
        async with session.begin():
            congratulation.congratulated_today = False
            session.add(congratulation)

    async def set_birthday(
        self, session: AsyncSession, user_id: int, day: int, month: int
    ) -> Birthday:
        stmt = select(Birthday).where(Birthday.user_id == user_id)
        async with session.begin():
            birthday = (await session.execute(stmt)).scalars().first()
        async with session.begin():
            if birthday is None:
                birthday = Birthday(user_id=user_id, day=day, month=month)
            else:
                birthday.day = day
                birthday.month = month
            session.add(birthday)
        return birthday

    async def drop_birthday(self, session: AsyncSession, user_id: int) -> bool:
        stmt = select(Birthday).where(Birthday.user_id == user_id)
        async with session.begin():
            birthday = (await session.execute(stmt)).scalars().first()
        if birthday is None:
            return False
        async with session.begin():
            await session.delete(birthday)
            return True

    @commands.group(aliases=["birthday"])
    async def birthdays(self, ctx: commands.Context):
        """
        Group for birthday commands.
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @birthdays.command(name="set")
    async def cmd_set_birthday(self, ctx: commands.Context, date: Date):
        """
        Add your birthday to the birthday list.

        Parameters
        ----------
        date : Date
            Your date of birth in the format MMDD.

        Example
        -------
        >>> !birthday set 0311
        Sets march 11th as your birthday
        """
        async with db.async_sessionmaker() as session:
            birthday = await self.set_birthday(session, ctx.author.id, date.day, date.month)
            locale = await self.get_locale(session, ctx.author.id)
        self.logger.info("%s - %d set their birthday to `%s`", ctx.author, ctx.author.id, birthday)

        msg = f"Successfully set your birthday to {birthday}."
        if locale is None:
            msg += (
                "\nYou might want to consider setting a locale via `!locale set` so that you "
                "will be congratulated in the correct timezone."
            )
        await ctx.send(msg)

    @cmd_set_birthday.error
    async def set_birthday_handler(self, ctx: commands.Context, error: BaseException):
        if isinstance(error, commands.CommandInvokeError):
            error = error.original
        if isinstance(error, InvalidDateError):
            await ctx.send(f"The date {error.value} is not a valid MMDD date.")
        else:
            print(f"Ignoring exception in command {ctx.command}:", file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @birthdays.command(aliases=["del", "delete", "rem"])
    async def remove(self, ctx: commands.Context):
        """
        Remove your birthday from the birthday list
        """
        async with db.async_sessionmaker() as session:
            existed = await self.drop_birthday(session, ctx.author.id)
        if not existed:
            await ctx.send("You didn't register your birthday yet.")
        else:
            self.logger.info("%s removed their birthday.", ctx.author)
            await ctx.send("Successfully removed your birthday from the birthday list.")

    @birthdays.command()
    async def show(self, ctx: commands.Context):
        """
        Show the birthday thats currently set for your discord account.
        """
        async with db.async_sessionmaker() as session:
            birthday = await self.get_birthday(session, ctx.author.id)
        if birthday is None:
            await ctx.send("Oops, you forgot to register your birthday.")
            return
        await ctx.send(f"You have set your birthday to {birthday}")

    async def congratulate(self, session: AsyncSession, birthday: Birthday):
        """
        Send a congratulation message for the user to #tavern in the JabKingdom

        Parameters
        ----------
        birthday : Birthday
            The `Birthday` instance of the user to congratulate
        """
        await self.add_congratulation(session, birthday)
        server = self.bot.get_guild(self.bot.config.ids.guild_id)
        if server is not None and server.get_member(birthday.user_id) is None:
            # don't send a message for members who are not in the server any more, but still follow
            # the whole other process so that they don't trigger a "new birthday" every time the
            # background task is running
            return
        channel = self.bot.get_channel(self.bot.config.ids.tavern_id)
        if channel is None:
            raise KnownChannelNotFoundError(self.bot.config.ids.tavern_id)
        if not isinstance(channel, TextChannel):
            raise UnexpectedChannelTypeError(self.bot.config.ids.tavern_id, type(channel))
        await channel.send(f"Happy birthday <@{birthday.user_id}>")
        self.logger.info("Congratulated %s", birthday.user_id)

    # loops

    async def get_tzinfo(self, session: AsyncSession, user_id: int) -> datetime.tzinfo:
        locale = await self.get_locale(session, user_id)
        if locale is None:
            return datetime.UTC
        return locale.tzinfo

    @tasks.loop(seconds=3600.0)
    async def restore_birthday_task(self):
        """
        Check every hour whether someones birthday has recently ended
        """
        try:
            async with db.async_sessionmaker() as session:
                for congratulation in await self.get_active_congratulations(session):
                    user_locale = await self.get_tzinfo(session, congratulation.user_id)
                    local_datetime = datetime.datetime.now(tz=user_locale)
                    birthday = await self.get_birthday(session, congratulation.user_id)
                    if birthday is None:
                        # NOTE: This should only happen if the bithday was deleted right after we
                        # read it from the DB
                        continue
                    if local_datetime.day != birthday.day or local_datetime.month != birthday.month:
                        await self.uncongratulate(session, congratulation)
                        self.logger.info("%s birthday has ended", congratulation.user_id)
        except Exception:
            self.logger.exception("Failed to restore birthdays")

    @tasks.loop(seconds=60.0)
    async def check_birthday_task(self):
        """
        Check whether someones birthday has started every minute.

        Parameters
        ----------
        client : discord.Client()
            the client that sends out birthday congratulations
        """
        try:
            async with db.async_sessionmaker() as session:
                for birthday in await self.get_uncongratulated_birthdays(session):
                    user_locale = await self.get_tzinfo(session, birthday.user_id)
                    local_datetime = datetime.datetime.now(tz=user_locale)
                    if (
                        local_datetime.month == birthday.month
                        and local_datetime.day == birthday.day
                    ):
                        await self.congratulate(session, birthday)
        except Exception:
            self.logger.exception("Error in birthday task.")

    @check_birthday_task.before_loop
    async def before_congratulating(self):
        await self.bot.wait_until_ready()


async def setup(bot: Bot):
    await db.create_schema(BIRTHDAYS_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(BirthdayCog(bot))
