import asyncio
import itertools
import logging
import time
from collections.abc import Sequence

import discord
from discord.ext import commands
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

import db
from bot import Bot
from db.discord_streams import DISCORD_STREAMS_SCHEMA_NAME, Base, Following, Streamer, Subscription
from utils.context import GuildContext
from utils.errors import KnownGuildNotFoundError

TIMEOUT_SECONDS = 60


class DiscordStreamCog(commands.Cog):
    """
    Manage notifications for people streaming in discord voice channels.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    async def cog_load(self):
        if self.bot.is_ready():
            await self.clear_offline_streamers()

    async def cog_unload(self):
        self.logger.info("Unloading cog - closing DB session.")

    @staticmethod
    def format_stream_message(streamer_name: str, channel_name: str, activity: str) -> str:
        return (
            f"Oy citizens of the JabKingdom ! **{streamer_name}** is now live in "
            f"`{channel_name}` with **{activity}**! Go check it out :wink:!"
        )

    async def get_streamer(self, session: AsyncSession, streamer_id: int) -> Streamer:
        stmt = select(Streamer).where(Streamer.id == streamer_id)
        async with session.begin():
            streamer = (await session.execute(stmt)).scalars().first()
        if streamer is None:
            async with session.begin():
                streamer = Streamer(
                    id=streamer_id,
                    status=None,
                    last_game_status_update=0,
                    is_live=False,
                    live_message_id=None,
                    last_gone_offline=time.time(),
                )
                session.add(streamer)
        return streamer

    async def get_subscription(
        self, session: AsyncSession, subscriber_id: int, streamer_id: int
    ) -> Subscription | None:
        stmt = select(Subscription).where(
            Subscription.streamer == streamer_id, Subscription.subscriber == subscriber_id
        )
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def subscribe(self, session: AsyncSession, subscriber_id: int, streamer_id: int) -> bool:
        if await self.get_subscription(session, subscriber_id, streamer_id) is not None:
            return False
        subscription = Subscription(streamer=streamer_id, subscriber=subscriber_id)
        async with session.begin():
            session.add(subscription)
        return True

    async def unsubscribe(
        self, session: AsyncSession, subscriber_id: int, streamer_id: int
    ) -> bool:
        if (
            subscription := await self.get_subscription(session, subscriber_id, streamer_id)
        ) is None:
            return False
        async with session.begin():
            await session.delete(subscription)
        return True

    async def get_subscriptions(self, session: AsyncSession, subscriber_id: int) -> Sequence[int]:
        stmt = select(Subscription.streamer).where(Subscription.subscriber == subscriber_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_subscribers(self, session: AsyncSession, streamer_id: int) -> Sequence[int]:
        stmt = select(Subscription.subscriber).where(Subscription.streamer == streamer_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def get_follow(
        self, session: AsyncSession, follower_id: int, activity: str
    ) -> Following | None:
        stmt = select(Following).where(
            Following.follower == follower_id, Following.game == activity
        )
        async with session.begin():
            return (await session.execute(stmt)).scalars().first()

    async def follow(self, session: AsyncSession, follower_id: int, activity: str) -> bool:
        follow = await self.get_follow(session, follower_id, activity)
        if follow is not None:
            return False
        follow = Following(game=activity, follower=follower_id)
        async with session.begin():
            session.add(follow)
        return True

    async def unfollow(self, session: AsyncSession, follower_id: int, activity: str) -> bool:
        follow = await self.get_follow(session, follower_id, activity)
        if follow is None:
            return False
        async with session.begin():
            await session.delete(follow)
        return True

    async def get_followers(self, session: AsyncSession, activity: str) -> list[int]:
        async with session.begin():
            follows = (await session.execute(select(Following))).scalars().all()
        return [follow.follower for follow in follows if follow.game.lower() in activity.lower()]

    async def get_follows(self, session: AsyncSession, follower_id: int) -> Sequence[str]:
        stmt = select(Following.game).where(Following.follower == follower_id)
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    async def set_status(
        self, session: AsyncSession, streamer_id: int, activity: str | None
    ) -> None:
        stmt = select(Streamer).where(Streamer.id == streamer_id)
        async with session.begin():
            streamer = (await session.execute(stmt)).scalars().first()
        async with session.begin():
            if streamer is None:
                streamer = Streamer(
                    id=streamer_id,
                    status=activity,
                    last_game_status_update=time.time(),
                    is_live=False,
                    live_message_id=None,
                    last_gone_offline=0,
                )
            else:
                streamer.status = activity
                streamer.last_game_status_update = int(time.time())
            session.add(streamer)

    async def streamers_with_message(self, session: AsyncSession) -> Sequence[Streamer]:
        stmt = select(Streamer).where(Streamer.live_message_id != None)  # noqa: E711
        async with session.begin():
            return (await session.execute(stmt)).scalars().all()

    @commands.guild_only()
    @commands.group(name="discord")
    async def discord_group(self, ctx: GuildContext):
        """
        Group for discord commands
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @commands.guild_only()
    @discord_group.command(name="subscribe", aliases=["sub"])
    async def subscribe_cmd(self, ctx: GuildContext, streamer: discord.Member):
        """
        Subscribe to someones discord screenshares

        Parameters
        ----------
        streamer : Member
            A member identifier (ID, mention, display name if unique, ...)

        Example
        -------
        >>> !discord subscribe 1232432138320999
        Everytime the member with the id 1232432138320999 shares their screen
        in a voice channel, you will get mentioned in #streams
        """
        async with db.async_sessionmaker() as session:
            new_subscription = await self.subscribe(session, ctx.author.id, streamer.id)
        if new_subscription:
            self.logger.info(
                "%s - %d is now subscribed to %s - %d screenshares",
                ctx.author,
                ctx.author.id,
                streamer,
                streamer.id,
            )
            await ctx.send(f"Successfully subscribed to {streamer}")
        else:
            await ctx.send(f"You are already subscribed to {streamer}")

    @commands.guild_only()
    @discord_group.command(name="unsubscribe", aliases=["unsub"])
    async def unsubscribe_cmd(self, ctx: GuildContext, streamer: discord.Member):
        """
        Unsubscribe from someones discord screenshares

        Parameters
        ----------
        streamer : Member
            A member identifier (ID, mention, display name if unique, ...)

        Example
        -------
        >>> !discord unsubscribe 1232432138320999
        You will no longer get mentions when the user with the id 1232432138320999
        starts sharing their screen.
        """
        async with db.async_sessionmaker() as session:
            change = await self.unsubscribe(session, ctx.author.id, streamer.id)
        if change:
            self.logger.info(
                "%s - %d is no longer subscribed to %s - %d screenshares",
                ctx.author,
                ctx.author.id,
                streamer,
                streamer.id,
            )
            await ctx.send(f"Successfully unsubscribed from user {streamer}.")
        else:
            await ctx.send("You are not subscribed to this user.")

    @commands.guild_only()
    @discord_group.command(name="subscriptions")
    async def subscriptions_cmd(self, ctx: GuildContext):
        """Shows who you are subscribed to for discord screenshares"""
        async with db.async_sessionmaker() as session:
            sub_ids = await self.get_subscriptions(session, ctx.author.id)
        if not sub_ids:
            return await ctx.send("You are not subscribed to anyone.")
        member_names = [
            f"{ctx.guild.get_member(discord_id)} - {discord_id}" for discord_id in sub_ids
        ]
        members = "\n".join(member_names)
        return await ctx.send(f"You are subscribed to the following people: ```\n{members}\n```")

    @commands.guild_only()
    @discord_group.command(name="follow")
    async def follow_cmd(self, ctx: GuildContext, *, activity: str):
        """
        Follow an activity for a discord screenshare

        Parameters
        ----------
        activity : str
            (part of) the name of the activity you want to follow

        Example
        -------
        >>> !discord follow rocket
        Everytime a screenshare of an activity with a name that includes 'rocket'
        (case insensitive) starts, you will receive a mention.
        """
        activity = activity.strip().strip('"').strip("'")
        async with db.async_sessionmaker() as session:
            await self.follow(session, ctx.author.id, activity)
        self.logger.info(
            "%s - %d is now following activities with %s in the name",
            ctx.author,
            ctx.author.id,
            activity,
        )
        return await ctx.send(
            f"You are now following all activities with `{activity}` in their name."
        )

    @commands.guild_only()
    @discord_group.command(name="unfollow")
    async def unfollow_cmd(self, ctx: GuildContext, *, activity: str):
        """
        Unfollow an activity for a discord screenshare

        Parameters
        ----------
        activity : str
            (part of) the name of the activity you want to unfollow

        Example
        -------
        >>> !discord unfollow rocket
        You will no longer receive mentions of activities being screenshared that
        include 'rocket' in their name
        """
        activity = activity.strip().strip('"').strip("'")
        async with db.async_sessionmaker() as session:
            await self.unfollow(session, ctx.author.id, activity)
        self.logger.info(
            "%s - %d is no longer following activities with %s in the name",
            ctx.author,
            ctx.author.id,
            activity,
        )
        return await ctx.send(f"You are no longer following `{activity}`")

    @commands.guild_only()
    @discord_group.command(name="follows")
    async def follows_cmd(self, ctx: GuildContext):
        """Shows which activities you are following to for discord screenshares"""
        async with db.async_sessionmaker() as session:
            followed_activities = await self.get_follows(session, ctx.author.id)
        if len(followed_activities) == 0:
            return await ctx.send("You are not following any activities.")
        follow_list = "\n".join(followed_activities)
        return await ctx.send(
            f"You are following the following activities: ```\n{follow_list}\n```"
        )

    async def update_live_message(self, session: AsyncSession, member: discord.Member, status: str):
        streamer = await self.get_streamer(session, member.id)
        if streamer.live_message_id is None:
            return
        stream_channel = self.bot.get_channel(self.bot.config.ids.stream_channel_id)
        if stream_channel is None or not isinstance(stream_channel, discord.TextChannel):
            self.logger.warning("Could not get stream channel, or it's not of the correct type.")
            return
        live_message = await stream_channel.fetch_message(streamer.live_message_id)

        if member.voice is None or member.voice.channel is None:
            return
        new_embed = self.create_stream_embed(status, member, member.voice.channel.name)
        if new_embed is None:
            return
        msg = self.format_stream_message(member.display_name, member.voice.channel.name, status)
        await live_message.edit(content=msg, embed=new_embed)

    @commands.guild_only()
    @discord_group.command(aliases=["game", "activity"])
    async def status(self, ctx: GuildContext, *, activity: str):
        """
        Set your status for the next stream.
        The current status will count up to 15 min before the stream

        Parameters
        ----------
        activity : str
            The activity you want to set your status to

        Example
        -------
        >>> !discord status Pokémon
        For the next 15 minutes, all your stream announcemens will show that you play Pokémon.
        """
        activity = activity.strip().strip('"')
        async with db.async_sessionmaker() as session:
            await self.set_status(session, ctx.author.id, activity)
            await self.update_live_message(session, ctx.author, activity)
        return await ctx.send(f"Successfully set your status to `{activity}`")

    @commands.guild_only()
    @discord_group.command(name="clear_status")
    async def clear_status_cmd(self, ctx: GuildContext):
        """Remove your custom stream status"""
        async with db.async_sessionmaker() as session:
            await self.set_status(session, ctx.author.id, None)
        return await ctx.send("Successfully removed your status.")

    async def announcement_mentions(
        self, session: AsyncSession, streamer_id: int, activity: str
    ) -> str:
        mentions = [
            f"<@{user}>"
            for user in itertools.chain(
                (await self.get_subscribers(session, streamer_id)),
                (await self.get_followers(session, activity)),
            )
        ]
        return "".join(mentions)

    def infer_activty(self, streamer: Streamer, member: discord.Member) -> str:
        if streamer.status and streamer.last_game_status_update > time.time() - (15 * 60):
            # custom activity, if one was set less than 15 minutes ago
            return streamer.status
        if (
            member.activity is not None
            and member.activity.type == discord.ActivityType.playing
            and member.activity.name is not None
        ):
            # else try to get activity through discord activity
            return member.activity.name
        return "A Secret Game"

    def create_stream_embed(
        self, activity: str, member: discord.Member, channel_name: str
    ) -> discord.Embed:
        embed = discord.Embed(title=f"{member.display_name} streams {activity}", color=member.color)
        embed.add_field(name="**Streamed Activity**", value=activity, inline=True)
        embed.add_field(name="**Channel**", value=channel_name)
        embed.set_author(name=member.display_name, icon_url=member.display_avatar.url)
        embed.set_thumbnail(url=member.display_avatar.url)
        return embed

    async def streamer_online(
        self, session: AsyncSession, member: discord.Member, channel_name: str
    ):
        streamer = await self.get_streamer(session, member.id)
        async with session.begin():
            streamer.is_live = True
            session.add(streamer)
        if streamer.live_message_id is not None:
            return
        message_id = await self.create_stream_message(session, streamer, member, channel_name)
        async with session.begin():
            streamer.live_message_id = message_id
            session.add(streamer)

    async def streamer_offline(self, session: AsyncSession, member_id: int):
        streamer = await self.get_streamer(session, member_id)
        async with session.begin():
            streamer.last_gone_offline = int(time.time())
            streamer.is_live = False
            session.add(streamer)
        await asyncio.sleep(TIMEOUT_SECONDS + 1)
        async with session.begin():
            await session.refresh(streamer)
        if streamer.is_live or time.time() - streamer.last_gone_offline < TIMEOUT_SECONDS:
            return
        if streamer.live_message_id is not None:
            await self.delete_stream_message(streamer.live_message_id, member_id)
            async with session.begin():
                streamer.live_message_id = None
                session.add(streamer)

    async def delete_stream_message(self, message_id: int, streamer_id: int):
        channel = self.bot.get_partial_messageable(self.bot.config.ids.stream_channel_id)
        message = channel.get_partial_message(message_id)
        try:
            await message.delete()
        except discord.NotFound:
            self.logger.exception(
                "Tried to delete stream message with id %d for %d but failed.",
                message_id,
                streamer_id,
            )

    async def create_stream_message(
        self, session: AsyncSession, streamer: Streamer, member: discord.Member, channel_name: str
    ) -> int:
        activity = self.infer_activty(streamer, member)
        embed = self.create_stream_embed(activity, member, channel_name)
        msg = self.format_stream_message(member.display_name, channel_name, activity)
        msg += "\n" + await self.announcement_mentions(session, member.id, activity)
        notification_channel = self.bot.get_partial_messageable(
            self.bot.config.ids.stream_channel_id
        )
        message = await notification_channel.send(msg, embed=embed)
        return message.id

    @commands.Cog.listener()
    async def on_voice_state_update(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if member.guild.id != self.bot.config.ids.guild_id:
            return
        async with db.async_sessionmaker() as session:
            if not before.self_stream and after.self_stream and after.channel:
                # stream has started
                await self.streamer_online(session, member, after.channel.name)
            elif not after.self_stream or not after.channel:
                # stream has ended
                await self.streamer_offline(session, member.id)

    async def clear_offline_streamers(self):
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
        async with db.async_sessionmaker() as session:
            for streamer in await self.streamers_with_message(session):
                member = guild.get_member(streamer.id)
                if (
                    member is None or member.voice is None or not member.voice.self_stream
                ) and streamer.live_message_id is not None:
                    await self.delete_stream_message(streamer.live_message_id, streamer.id)
                    async with session.begin():
                        streamer.live_message_id = None
                        if streamer.is_live:
                            streamer.last_gone_offline = int(time.time())
                            streamer.is_live = False
                        session.add(streamer)

    @commands.Cog.listener()
    async def on_ready(self):
        await self.clear_offline_streamers()

    @commands.Cog.listener()
    async def on_resumed(self):
        await self.clear_offline_streamers()


async def setup(bot: Bot):
    await db.create_schema(DISCORD_STREAMS_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(DiscordStreamCog(bot))
