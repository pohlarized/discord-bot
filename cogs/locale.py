import logging
import zoneinfo
from zoneinfo import ZoneInfo

import discord
from discord.ext import commands
from sqlalchemy import select

import db
from bot import Bot
from db.locale import Base, Locale


class LocaleCog(commands.Cog):
    """
    Cog to set your bot-wide locale
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @commands.group(aliases=["timezone"])
    async def locale(self, ctx: commands.Context):
        """
        Manage your bot wide locale settings.
        """
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    @locale.command()
    @commands.is_owner()
    async def force(self, ctx: commands.Context, member: discord.Member, locale_str: str):
        """
        Set the locale for another member

        Params
        ------
        member: discord.Member
            The member for which to set the locale string

        locale_str: str
            The IANA string corresponding to your locale. You can find a list at
            https://gist.github.com/aviflax/a4093965be1cd008f172

        Examples
        --------
        !locale force 012349933848484 Europe/Paris
        >>> Sets the locale for the user 012349933848484 to Europe/Paris
        """
        timezone = ZoneInfo(locale_str)
        if timezone is None:
            await ctx.send(f"Sorry, i don't know the locale `{locale_str}`.")
            return
        stmt = select(Locale).where(Locale.id == member.id)
        async with db.async_sessionmaker() as session:
            async with session.begin():
                locale = (await session.execute(stmt)).scalars().first()
            if locale is None:
                locale = Locale(id=member.id, locale=locale_str)
            async with session.begin():
                session.add(locale)
        await ctx.send(f"Successfully set `{member}`s locale to `{locale_str}`.")

    @locale.command(name="set")
    async def set_locale(self, ctx: commands.Context, locale_str: str):
        """
        Set your bot-wide locale.

        Params
        ------
        locale_str: str
            The IANA string corresponding to your locale. You can find a list at
            https://gist.github.com/aviflax/a4093965be1cd008f172

        Examples
        --------
        !locale set Europe/Paris
        >>> Sets your locale setting to Europe/Paris
        """
        if locale_str not in zoneinfo.available_timezones():
            raise commands.BadArgument(f"Unknown locale {locale_str}.")  # noqa: TRY003 error displayed to user
        stmt = select(Locale).where(Locale.id == ctx.author.id)
        async with db.async_sessionmaker() as session, session.begin():
            locale = (await session.execute(stmt)).scalars().first()
            if locale is None:
                locale = Locale(id=ctx.author.id, locale=locale_str)
            locale.locale = locale_str
            session.add(locale)
        await ctx.send(f"Successfully set your locale to `{locale_str}`.")

    @locale.command(name="get")
    async def get_locale(self, ctx: commands.Context):
        """
        Get your bot-wide locale.
        """
        stmt = select(Locale).where(Locale.id == ctx.author.id)
        async with db.async_sessionmaker() as session:
            result = (await session.execute(stmt)).scalars().first()
        if result is None:
            await ctx.send("You have not set your locale yet.")
            return
        await ctx.send(f"Your locale is currently set to `{result.locale}`")


async def setup(bot: Bot):
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(LocaleCog(bot))
