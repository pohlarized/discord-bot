import calendar
import datetime
import logging
from contextlib import suppress

import discord
from discord import Member, TextChannel, Thread, User
from discord.abc import GuildChannel, Messageable
from discord.ext import commands, tasks
from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import InstrumentedAttribute, Session
from tabulate import tabulate

import db
from bot import Bot
from db.activity import ACTIVITY_SCHEMA_NAME, Base, Month, Normie, Record
from utils import format_table_message
from utils.constants import TIMEZONE
from utils.errors import (
    KnownChannelNotFoundError,
    KnownGuildNotFoundError,
    UnexpectedChannelTypeError,
)


class ShortActivityFlags(commands.FlagConverter):
    globally: bool = False


class ActivityFlags(ShortActivityFlags):
    limit: commands.Range[int, 1, 20] = 10


class ActivityTracker(commands.Cog):
    """
    Tracks users activity by channel.

    It tracks word and messagecount per channel,
    directly associated with their discord ids.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.normies = self.load_normies()
        self.month = self.load_month()
        self.logger = logging.getLogger(__name__)
        self.activity_reset_task.start()

    async def cog_unload(self):
        self.activity_reset_task.cancel()

    def load_normies(self):
        with Session(db.BLOCKING_ENGINE) as session:
            normies = session.execute(select(Normie)).scalars().all()
            return [normie.member_id for normie in normies]

    def load_month(self):
        with Session(db.BLOCKING_ENGINE) as session:
            result = session.execute(select(Month)).scalars().first()
            if result is None:
                month_number = datetime.datetime.now(TIMEZONE).month
                session.add(Month(number=month_number))
                session.commit()
            else:
                month_number = result.number
        return month_number

    @commands.group()
    async def activity(self, ctx: commands.Context):
        """Group for activity commands"""
        if not ctx.invoked_subcommand:
            await ctx.send_help(ctx.command)

    async def remove_data(self, session: AsyncSession, member_id: int) -> bool:
        """
        Remove all data of a user.

        Parameters
        ----------
        member_id: int
            the discord ID of the user who's data should be removed

        Returns
        -------
        bool
            True if at least one record was found, False otherwise
        """
        statement = select(Record).where(Record.member_id == member_id)
        async with session.begin():
            records = (await session.execute(statement)).scalars().all()
        present = False
        async with session.begin():
            for record in records:
                present = True
                await session.delete(record)
        return present

    async def confirm(self, author: User | Member, channel: Messageable, action: str) -> bool:
        await channel.send(
            f"Are you sure you want to {action}?\n"
            f'Say "Yes" to confirm.\n'
            f"**This may be impossible to undo.**"
        )
        try:
            await self.bot.wait_for(
                "message",
                timeout=15,
                check=lambda msg: msg.author == author
                and msg.channel == channel
                and msg.content.strip() == "Yes",
            )
        except TimeoutError:
            return False
        return True

    @activity.command(aliases=["optout"])
    async def opt_out(self, ctx: commands.Context):
        """Opt out of activity tracking, and reset all your stats. THIS IS PERMANENT!"""
        if not self.confirm(
            ctx.author,
            ctx.channel,
            "opt out of activity tracking and delete all your activity data",
        ):
            await ctx.send("Cancelled opt-out and deletion of your activity data")
            return
        statement = select(Normie).where(Normie.member_id == ctx.author.id)
        async with db.async_sessionmaker() as session:
            async with session.begin():
                normie_record = (await session.execute(statement)).scalars().first()
            if normie_record is not None:
                await ctx.send("You have already opted out, have a nice day :)")
                return
            normie_record = Normie(member_id=ctx.author.id)
            async with session.begin():
                session.add(normie_record)
            await self.remove_data(session, ctx.author.id)
        self.normies.append(ctx.author.id)
        await ctx.send("Successfully opted out of data collection and reset all stats.")

    @activity.command(aliases=["optin"])
    async def opt_in(self, ctx: commands.Context):
        """Opt in to activity tracking."""
        statement = select(Normie).where(Normie.member_id == ctx.author.id)
        async with db.async_sessionmaker() as session:
            async with session.begin():
                normie_record = (await session.execute(statement)).scalars().first()
            if normie_record is None:
                await ctx.send("You are already opted in for activity tracking.")
                return
            async with session.begin():
                await session.delete(normie_record)
        self.normies.remove(ctx.author.id)
        await ctx.send("Successfully opted back in to data collection.")

    async def get_username(self, discord_id: int, guild: discord.Guild) -> str:
        user = guild.get_member(discord_id)
        if user is None:
            user = self.bot.get_user(discord_id)
        if user is None:
            return f"Unknown user {discord_id}"
        return f"{user.display_name}#{user.discriminator}"

    async def create_global_leaderboard(self, session: AsyncSession, limit: int) -> str:
        return await self.create_leaderboard(
            session, Record.message_count, Record.word_count, limit
        )

    async def create_monthly_leaderboard(self, session: AsyncSession, limit: int) -> str:
        return await self.create_leaderboard(
            session, Record.message_count_monthly, Record.word_count_monthly, limit
        )

    async def create_leaderboard(
        self,
        session: AsyncSession,
        msg_count: InstrumentedAttribute,
        word_count: InstrumentedAttribute,
        limit: int,
    ) -> str:
        msg_sum = func.sum(msg_count)
        word_sum = func.sum(word_count)
        stmt = (
            select(Record.member_id, msg_sum.label("msg_count"), word_sum.label("word_count"))
            .filter(msg_count > 0)
            .group_by(Record.member_id)
            .order_by(msg_sum.desc())
            .limit(limit)
        )
        async with session.begin():
            results = (await session.execute(stmt)).all()
        if len(results) == 0:
            return "I did not see anyone chatting this month."
        headers = ["Username", "Words", "Messages", "Words/Message"]
        guild = self.bot.get_guild(self.bot.config.ids.guild_id)
        if guild is None:
            raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
        rows = [
            [
                await self.get_username(result.member_id, guild),
                result.word_count,
                result.msg_count,
                result.word_count / result.msg_count,
            ]
            for result in results
        ]
        return tabulate(rows, headers=headers, floatfmt=".2f")

    @activity.command(aliases=["lb", "sb", "scoreboard"])
    async def leaderboard(self, ctx: commands.Context, *, flags: ActivityFlags):
        """
        Show the top 10 most active people by message count

        Also display their total words/messages sent during the tracking period

        Parameters
        ----------
        <flag>globally: bool
            boolean value to determine whether you want to view global data or just
            a limited timeframe.
            accepts boolean-like strings like ('yes', 'no'), ('True', 'False') etc.

        <flag>limit: Range[1, 20]
            a single integer in the range from 1 to 20 determining how many entries to show

        Example
        -------
        >>> !activity leaderboard
        Shows the top 10 people who sent the most messages in the current month

        >>> !activity leaderboard globally: yes limit: 4
        Shows the top 4 people who sent the most messages since start of the recordings
        """
        # TODO: implement blacklist
        async with ctx.typing(), db.async_sessionmaker() as session:
            if flags.globally:
                lb = await self.create_global_leaderboard(session, limit=flags.limit)
            else:
                lb = await self.create_monthly_leaderboard(session, limit=flags.limit)
        for page in format_table_message(lb, "Activity leaderboard"):
            await ctx.send(page)

    async def get_channel_or_thread_name(self, discord_id: int) -> str:
        channel_or_thread = self.bot.get_channel(discord_id)
        if channel_or_thread is None:
            guild = self.bot.get_guild(self.bot.config.ids.guild_id)
            if guild is None:
                raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
            with suppress(discord.NotFound):
                channel_or_thread = await guild.fetch_channel(discord_id)
        elif not isinstance(channel_or_thread, Thread) and not isinstance(
            channel_or_thread, GuildChannel
        ):
            raise UnexpectedChannelTypeError(discord_id, type(channel_or_thread))
        if channel_or_thread is None:
            return f"#{discord_id}#"
        return channel_or_thread.name[:37]

    async def create_global_personal_leaderboard(
        self, session: AsyncSession, member_id: int
    ) -> list[str]:
        word_count = Record.word_count
        message_count = Record.message_count
        return await self.create_personal_leaderboard(session, member_id, word_count, message_count)

    async def create_monthly_personal_leaderboard(
        self, session: AsyncSession, member_id: int
    ) -> list[str]:
        word_count = Record.word_count_monthly
        message_count = Record.message_count_monthly
        return await self.create_personal_leaderboard(session, member_id, word_count, message_count)

    async def create_personal_leaderboard(
        self, session: AsyncSession, member_id: int, word_count, message_count
    ) -> list[str]:
        stmt = (
            select(
                Record.member_id,
                Record.channel_id,
                word_count.label("word_count"),
                message_count.label("msg_count"),
            )
            .filter(Record.member_id == member_id, message_count > 0)
            .order_by(message_count.desc())
        )
        results = (await session.execute(stmt)).all()
        if len(results) == 0:
            return ["I did not see you chat this month"]
        msg_sum = sum(result.msg_count for result in results)
        word_sum = sum(result.word_count for result in results)
        headline = (
            f"Total words: {word_sum}\tTotal messages: {msg_sum}\t"
            f"words/message: {word_sum/msg_sum:.2f}"
        )
        rows = [
            (
                await self.get_channel_or_thread_name(result.channel_id),
                str(result.word_count),
                str(result.msg_count),
            )
            for result in results
        ]
        headers = ("Channel Name", "Words", "Messages")
        lb = tabulate(rows, headers=headers)
        return format_table_message(lb, headline)

    @activity.command(aliases=["clb"])
    async def channel_leaderboard(
        self, ctx: commands.Context, channel: discord.TextChannel, *, flags: ActivityFlags
    ):
        """
        Show the top 10 most active people in a certain channel by message count

        Also display their total words/messages sent during the tracking period

        Parameters
        ----------
        channel: Channel
            The channel you want to get the leaderboard for

        <flag>globally: bool, default: False
            boolean value to determine whether you want to view global data or just
            a limited timeframe.
            accepts boolean-like strings like ('yes', 'no'), ('True', 'False') etc.

        <flag>limit: Range[1, 20], default: 10
            a single integer in the range from 1 to 20 determining how many entries to show

        Example
        -------
        >>> !activity channel_leaderboard #general
        Shows the top 10 people who sent the most messages in the current month in the
        #general channel

        >>> !activity leaderboard #general globally: yes limit: 4
        Shows the top 4 people who sent the most messages since start of the recordings in the
        #general channel
        """
        msg_count = Record.message_count if flags.globally else Record.message_count_monthly
        word_count = Record.word_count if flags.globally else Record.word_count_monthly
        stmt = (
            select(
                Record.member_id,
                Record.channel_id,
                word_count.label("word_count"),
                msg_count.label("message_count"),
            )
            .where(Record.channel_id == channel.id, msg_count > 0)
            .order_by(msg_count.desc())
            .limit(flags.limit)
        )
        async with ctx.typing():
            async with db.async_sessionmaker() as session:
                results = (await session.execute(stmt)).all()
            if len(results) == 0:
                await ctx.send(
                    f"There has been no activity for {channel.mention} in the specified timeframe."
                )
                return
            headers = ["Username", "Words", "Messages", "Words/Message"]
            guild = self.bot.get_guild(self.bot.config.ids.guild_id)
            if guild is None:
                raise KnownGuildNotFoundError(self.bot.config.ids.guild_id)
            rows = [
                [
                    await self.get_username(result.member_id, guild),
                    result.word_count,
                    result.message_count,
                    result.word_count / result.message_count,
                ]
                for result in results
            ]
        table = tabulate(rows, headers=headers, floatfmt=".2f")
        for page in format_table_message(table, f"Activity leaderboard for {channel.mention}"):
            await ctx.send(page)

    @activity.command()
    async def me(self, ctx: commands.Context, *, flags: ShortActivityFlags):
        """
        Show stats about your own discord activity

        Parameters
        ----------
        <flag>globally: bool
            boolean value to determine whether you want to view global data or just
            a limited timeframe.
            accepts boolean-like strings like ('yes', 'no'), ('True', 'False') etc.

        Example
        -------
        >>> !activity me globally: yes
        Shows you stats about your all time activity
        """
        async with ctx.typing(), db.async_sessionmaker() as session:
            if flags.globally:
                pages = await self.create_global_personal_leaderboard(session, ctx.author.id)
            else:
                pages = await self.create_monthly_personal_leaderboard(session, ctx.author.id)
        for page in pages:
            await ctx.send(page)

    @activity.command()
    @commands.is_owner()
    async def delete(self, ctx: commands.Context, discord_id: int):
        """
        Delete all saved tracking data about a user

        Parameters
        ----------
        discordid: int
            the discordid of the person you want to remove the stats from

        Example
        -------
        >>> !activity delete 1232432139320999
        Deletes all tracking data of the user with the discordid 1232432139320999
        """
        async with db.async_sessionmaker() as session:
            present = await self.remove_data(session, discord_id)
        if present:
            return await ctx.send("Successfully removed all data about this user.")
        return await ctx.send("No data for this user could be found.")

    async def get_record(self, session: AsyncSession, author_id: int, channel_id: int):
        async with session.begin():
            record = (
                (
                    await session.execute(
                        select(Record).where(
                            Record.channel_id == channel_id, Record.member_id == author_id
                        )
                    )
                )
                .scalars()
                .first()
            )
        if record is None:
            record = Record(
                member_id=author_id,
                channel_id=channel_id,
                word_count=0,
                message_count=0,
                word_count_monthly=0,
                message_count_monthly=0,
            )
        return record

    @activity.command()
    @commands.is_owner()
    async def set_month(self, ctx: commands.Context, number: int):
        async with db.async_sessionmaker() as session:
            await self.set_current_month(session, number)
        await ctx.send(f"Set current month to {self.month}")

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        ctx = await self.bot.get_context(message)
        if (
            message.content.startswith("!")
            or ctx.author.bot
            or not ctx.guild
            or ctx.guild.id != self.bot.config.ids.guild_id
            or ctx.author.id in self.normies
        ):
            return
        async with db.async_sessionmaker() as session:
            record = await self.get_record(session, ctx.author.id, ctx.channel.id)
            wordcount = len(message.content.strip(" ").split(" "))
            record.message_count += 1
            record.message_count_monthly += 1
            record.word_count += wordcount
            record.word_count_monthly += wordcount
        async with db.async_sessionmaker() as session, session.begin():
            session.add(record)

    async def reset_monthly_counts(self, session: AsyncSession):
        async with session.begin():
            results = (await session.execute(select(Record))).scalars()
        async with session.begin():
            for result in results:
                result.word_count_monthly = 0
                result.message_count_monthly = 0
                session.add(result)

    async def set_current_month(self, session: AsyncSession, number: int | None = None):
        if number is None:
            number = datetime.datetime.now(TIMEZONE).month
        async with session.begin():
            month = (await session.execute(select(Month))).scalars().first()
        if month is None:
            month = Month(number=number)
        async with session.begin():
            month.number = number
            session.add(month)
        self.month = number

    @tasks.loop(hours=1)
    async def activity_reset_task(self):
        current_month = datetime.datetime.now(TIMEZONE).month
        if current_month == self.month:
            return
        bot_stuff = self.bot.get_channel(self.bot.config.ids.bot_channel_id)
        if bot_stuff is None:
            raise KnownChannelNotFoundError(self.bot.config.ids.bot_channel_id)
        if not isinstance(bot_stuff, TextChannel):
            raise UnexpectedChannelTypeError(self.bot.config.ids.bot_channel_id, type(bot_stuff))
        async with bot_stuff.typing(), db.async_sessionmaker() as session:
            lb = await self.create_monthly_leaderboard(session, limit=10)
            for page in format_table_message(
                lb, f"Activity leaderboard for {calendar.month_name[current_month]}"
            ):
                await bot_stuff.send(page)
            await bot_stuff.send("Resetting montly data.")
            await self.reset_monthly_counts(session)
            await self.set_current_month(session)

    @activity_reset_task.before_loop
    async def before_activity_reset(self):
        await self.bot.wait_until_ready()


async def setup(bot: Bot):
    await db.create_schema(ACTIVITY_SCHEMA_NAME)
    Base.metadata.create_all(bind=db.BLOCKING_ENGINE)
    await bot.add_cog(ActivityTracker(bot))
