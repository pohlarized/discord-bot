import logging
import random

import discord
from discord.ext import commands

from bot import Bot


class WhatCog(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)

    @staticmethod
    async def get_previous_message(message: discord.Message):
        found = False
        tmp_msg = None
        async for msg in message.channel.history(limit=10):
            if found:
                tmp_msg = msg
                break
            if msg.content == message.content and msg.author == message.author:
                found = True

        return tmp_msg if found else None

    @staticmethod
    def has_embed(message: discord.Message):
        return len(message.embeds) > 0 or len(message.attachments) > 0

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        what_likelihood = 0.2
        if message.author.id == self.bot.user.id:
            return
        if message.content.strip().lower() == "what":
            previous = await self.get_previous_message(message)
            if previous is None:
                return
            if self.has_embed(previous):
                return
            # randomize it
            if random.random() > what_likelihood:
                return
            # normal code again
            msg = previous.content.replace("***", "").replace("**", "")
            msg = f"**{msg}**".upper()
            await message.channel.send(msg)
            self.logger.info(
                "%s - %s whatted in channel %s - %s. Content: %s",
                message.author,
                message.author.id,
                message.channel,
                message.channel.id,
                msg,
            )


async def setup(bot: Bot):
    await bot.add_cog(WhatCog(bot))
