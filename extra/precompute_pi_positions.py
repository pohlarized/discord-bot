import struct


# This expects digits 0-9 encoded in 4 bits respectively, the format use in
# https://www.angio.net/pi/piquery.html
def decompress(b: bytes) -> str:
    chars = []
    for byte in b:
        c0 = byte >> 4
        c1 = byte & 0xF
        chars.append(str(c0))
        chars.append(str(c1))
    return "".join(chars)


def create_position_map(digits: str) -> list[int]:
    return [digits.find(str(i)) + 1 for i in range(100_000)]


def pack_positions(position_map: list[int]) -> bytes:
    return struct.pack("I" * len(position_map), *position_map)


if __name__ == "__main__":
    with open("./pi50.4.bin", "rb") as fp:
        pi = fp.read()
    digits = decompress(pi)
    position_map = create_position_map(digits)
    b = pack_positions(position_map)
    with open("../pi_digits_packed.bin", "wb") as fp:
        fp.write(b)
