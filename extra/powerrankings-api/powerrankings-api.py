import io
import math
from datetime import date, datetime, timedelta
from itertools import product
from typing import Union

import matplotlib
import seaborn as sns
from fastapi import FastAPI
from fastapi.responses import StreamingResponse
from matplotlib import pyplot as plt
from pydantic import BaseModel
from sqlalchemy import select
from sqlalchemy.orm import Session

import db
from db.powerrankings import Record


class DiscordUser(BaseModel):
    name: str
    id: int
    color: str


class Arguments(BaseModel):
    user_list: list[DiscordUser]
    limit: int
    start_date: date = date(2022, 8, 15) + timedelta(days=30) + timedelta(days=24)


app = FastAPI()

linestyles = ['solid', 'dashed', 'dashdot', 'dotted']
colors = ['#cc241d', '#98971a', '#d79921', '#458588', '#b16286', '#689d6a', '#a89984', '#d65d0e', '#f071fc', '#282828']
line_props = [{'color': color, 'linestyle': style} for style, color in product(linestyles, colors)]

def asciiify(s: str) -> str:
    return ''.join(c if ord(c) <= 128 else '_' for c in s)

def add_lists(a: list, b: list) -> list:
    if len(a) == 0:
        return [] + b
    elif len(b) == 0:
        return [] + a
    elif len(a) != len(b):
        raise Exception('Lists are not the same length!')
    return [x+y for x, y in zip(a, b)]


def ceil_hundred(num: Union[int, float]) -> int:
    return math.ceil(num / 100) * 100


def calculate_power(no_emojis: int, no_messages: int) -> float:
    if no_messages < 30:
        return 0.0
    emojis_per_message = no_emojis / no_messages
    return emojis_per_message * math.pi * math.e * 100


def get_all_members(session: Session) -> list[int]:
    stmt = select(Record.member_id).group_by(Record.member_id)
    resp = session.execute(stmt).scalars().all()
    return resp


def get_records(session: Session, member_id: int) -> list[Record]:
    return session.execute(select(Record).where(Record.member_id == member_id)).scalars().all()


def daterange(start_date: date, end_date: date) -> list[date]:
    day_amount = (end_date - start_date).days
    return [start_date + timedelta(i) for i in range(day_amount+1)]


def powers_for_member(session: Session, member_id: int, dates: list[date]) -> list[float]:
    records = get_records(session, member_id)
    powers = []
    for date in dates:
        start_date = date - timedelta(days=30)
        important_records = [r for r in records if r.day >= start_date and r.day < date]
        msg_sum = sum((r.no_messages for r in important_records))
        emoji_sum = sum((r.no_emojis for r in important_records))
        powers.append(calculate_power(emoji_sum, msg_sum))
    return powers

def plot_top10_ranking_sum(powerrankings: dict[int, list[float]],
                           start_date: date,
                           end_date: date) -> io.BytesIO:
    x = list(range(len(next(powerrankings.values().__iter__()))))
    y = []
    for i in range(len(x)):
        top10_ids = sorted(
            powerrankings.keys(), key=lambda x: powerrankings[x][i], reverse=True
        )[:10]
        y.append(sum(powerrankings[x][i] for x in top10_ids))
    plt.plot(x, y)
    plt.ylim(0, max(y) * 1.05)
    plt.title(f'Sum of top 10 powers between {start_date.isoformat()} and {end_date.isoformat()}')
    plt.xlabel(f'Days since {start_date.isoformat()}')
    plt.ylabel('Power sum')
    buf = io.BytesIO()
    plt.savefig(buf, bbox_inches='tight', dpi=300)# , bbox_inches='tight')
    plt.clf()
    return buf

def plot_powerrankings(powerrankings: dict[int, list[float]],
                       user_dict: dict[int, dict[str, str]],
                       start_date: date,
                       end_date: date,
                       to_label: list[int]) -> io.BytesIO:
    x = list(range(len(next(powerrankings.values().__iter__()))))
    for userid, powers in powerrankings.items():
        if userid in to_label:
            plt.plot(x,
                     powers,
                     label=asciiify(user_dict[userid]['name']),
                     **line_props[to_label.index(userid)]
                     )
        else:
            # plot everyone with a label
            # plt.plot(
            #     x,
            #     powers,
            #     label=user_dict[userid]['name'],
            #     color=user_dict[userid]['color']
            # )

            # plot everyone, but only top10 get a label
            # plt.plot(x, powers)

            # only plot top10
            pass
    max_power = max([max(v) for v in powerrankings.values()])
    plt.ylim(0, ceil_hundred(max_power * 1.05))
    plt.title(f'Powerrankings between {start_date.isoformat()} and {end_date.isoformat()}')
    plt.xlabel(f'Days since {start_date.isoformat()}')
    plt.ylabel('Power')
    plt.legend(bbox_to_anchor=(1, 1), loc='upper left')
    sns.despine()
    buf = io.BytesIO()
    plt.savefig(buf, bbox_inches='tight', dpi=300)# , bbox_inches='tight')
    plt.clf()
    return buf


def discord_users_to_dict(user_list: list[DiscordUser]) -> dict[int, dict[str, str]]:
    return {user.id: {'name': user.name, 'color': user.color} for user in user_list}


@app.post('/graph')
def get_graph(args: Arguments):
    user_dict = discord_users_to_dict(args.user_list)
    matplotlib.rcParams['font.family'] = 'Linux Biolinum'
    end_date = datetime.utcnow().date()
    dates = daterange(args.start_date, end_date)
    with Session(db.BLOCKING_ENGINE) as session:
        members = get_all_members(session)
        powerrankings = {member: powers_for_member(session, member, dates)
                         for member in members
                         if sum(powers_for_member(session, member, dates)) != 0}
    # top n currently most powerful players
    players_to_draw = sorted(
        powerrankings.keys(), key=lambda x: powerrankings[x][-1], reverse=True
    )[:args.limit]
    plot_buf = plot_powerrankings(powerrankings, user_dict, args.start_date, end_date, players_to_draw)
    plot_buf.seek(0)
    return StreamingResponse(plot_buf, media_type="image/png")


@app.post('/top10_sum_graph')
def get_average_graph():
    matplotlib.rcParams['font.family'] = 'Linux Biolinum'
    start_date = date(2022, 8, 15) + timedelta(days=30) + timedelta(days=24)
    end_date = datetime.utcnow().date()
    dates = daterange(start_date, end_date)
    with Session(db.BLOCKING_ENGINE) as session:
        members = get_all_members(session)
        powerrankings = {member: powers_for_member(session, member, dates)
                         for member in members
                         if sum(powers_for_member(session, member, dates)) != 0}
    plot_buf = plot_top10_ranking_sum(powerrankings, start_date, end_date)
    plot_buf.seek(0)
    return StreamingResponse(plot_buf, media_type="image/png")
