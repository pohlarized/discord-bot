# TODO: limit DB lookups to times of the CURRENT YEAR (at time of lookup i guess? or perhaps
# manually set idk.)

# TODO: in the future, don't serialize to JSON, but instead store things in the DB i guess.
# But for now that would require messing a lot with tables and such and i don't want to do that...
# Just have one big JSON that the API loads into memory when it starts up and then just reads from
# it :)

import datetime
import itertools
import re
from collections import Counter, defaultdict
from collections.abc import Sequence
from pathlib import Path

from emoji import analyze as emoji_analyze
from pydantic import BaseModel
from sqlalchemy import func, select
from sqlalchemy.orm import Session

import db
from db.discord_rewind import (
    Channel,
    CommandExecuted,
    Member,
    Mention,
    MessageDelete,
    MessageEdit,
    MessageSend,
    ReactionAdd,
    StreamSession,
    VoiceEventKind,
    VoiceSession,
)

EMOJI_REGEX = r"<:\w+:[0-9]+>"
OUTPUT_DIRECTORY = Path("/tmp/discord-rewind-data")  # noqa: S108 we don't want to keep the data


# TODO: unify format. The `GuildStats` and `UserStats` should never (or usually not) contain IDs,
# but rather channel names.
# Same with user IDs and user names.
# We do have the `Member` and `Channel` DB models that should allow resolving these.


class GuildStats(BaseModel):
    most_used_channels: list[tuple[int, int]]  # maps channel name to number of messages
    most_used_voice_channels: list[
        tuple[int, datetime.timedelta]
    ]  # maps channel IDs to time in channel
    most_active_voice_users: list[
        tuple[int, datetime.timedelta]
    ]  # maps user ID to amount of time in voice
    most_active_stream_user: list[
        tuple[int, datetime.timedelta]
    ]  # maps user ID to amount of time in stream
    most_active_message_user: list[tuple[int, int]]  # maps user ID to number of messages sent
    most_active_words_user: list[tuple[int, int]]  # maps user ID to number of words sent
    most_consistent_users: list[
        tuple[int, tuple[datetime.date, datetime.date]]
    ]  # maps user ID to date start and end (exclusive) of their message streak
    no_messages: int
    voice_time: datetime.timedelta
    stream_time: datetime.timedelta
    no_emojis: int
    no_mentions: int
    most_used_emojis: list[tuple[str, int]]  # maps emoji ID to amount of times used
    most_mentioned_users: list[tuple[int, int]]  # maps user ID to amount of times mentioned
    most_reacted_with_emojis: list[tuple[str, int]]  # maps emoji name to number of uses


class UserStats(BaseModel):
    user_id: int
    no_messages_sent: int
    no_messages_edited: int
    no_messages_deleted: int
    no_words_sent: int
    no_channels_used: int
    no_emojis_used: int
    emojis_used: list[tuple[str, int]]
    time_in_voice: datetime.timedelta
    time_in_voice_with_others: list[
        tuple[int, datetime.timedelta]
    ]  # maps user ID to shared time in voice
    time_streamed: datetime.timedelta
    distinct_days_chatted: int
    longest_message_days_streak: (
        tuple[datetime.date, datetime.date] | None
    )  # start and end (exclusive) of the streak
    most_used_channels: list[tuple[int, int]]  # maps channel names to number of messages sent
    most_used_commands: list[tuple[str, int]]  # maps command name to number of times used
    most_used_voice_channels: list[
        tuple[int, datetime.timedelta]
    ]  # maps channel id to number of minutes in channel
    most_mentioned_users: list[tuple[int, int]]  # maps user ID to number of mentions
    most_reacted_with_emojis: list[tuple[str, int]]  # maps emoji name to number of uses


class Database:
    def __init__(self, session: Session):
        self.session = session

    def members(self) -> Sequence[Member]:
        return self.session.execute(select(Member)).scalars().all()

    def get_channel(self, channel_id: int) -> Channel | None:
        return (
            self.session.execute(select(Channel).where(Channel.id == channel_id)).scalars().first()
        )

    def get_member(self, member_id: int) -> Member | None:
        return self.session.execute(select(Member).where(Member.id == member_id)).scalars().first()

    def messages(self, user_id: int | None) -> Sequence[MessageSend]:
        stmt = select(MessageSend)
        if user_id is not None:
            stmt = stmt.where(MessageSend.author_id == user_id)
        return self.session.execute(stmt).scalars().all()

    def message_edits(self, user_id: int) -> Sequence[MessageEdit]:
        stmt = select(MessageEdit).where(MessageEdit.author_id == user_id)
        return self.session.execute(stmt).scalars().all()

    def message_deletes(self, user_id: int) -> Sequence[MessageDelete]:
        stmt = select(MessageDelete).where(MessageDelete.author_id == user_id)
        return self.session.execute(stmt).scalars().all()

    def all_voice_sessions(self) -> Sequence[VoiceSession]:
        return self.session.execute(select(VoiceSession)).scalars().all()

    def voice_sessions(self, user_id: int | None) -> Sequence[VoiceSession]:
        stmt = select(VoiceSession)
        if user_id is not None:
            stmt = stmt.where(VoiceSession.user_id == user_id, VoiceSession.end != None)  # noqa: E711 maybe required by sqlalchemy
        return self.session.execute(stmt).scalars().all()

    def stream_sessions(self, user_id: int | None) -> Sequence[StreamSession]:
        stmt = select(StreamSession).where(StreamSession.end != None)  # noqa: E711 maybe required by sqlalchemy
        if user_id is not None:
            stmt = stmt.where(StreamSession.user_id == user_id)
        return self.session.execute(stmt).scalars().all()

    def most_used_channels(self, user_id: int | None) -> Sequence[tuple[int, int]]:
        stmt = select(
            Channel.id,
            func.count(MessageSend.id).label("MessageCount"),
        ).join(MessageSend, MessageSend.channel_id == Channel.id)
        if user_id is not None:
            stmt = stmt.where(MessageSend.author_id == user_id)
        stmt = stmt.group_by(Channel.id).order_by(func.count(MessageSend.channel_id).desc())
        return self.session.execute(stmt).all()

    def most_used_commands(self, user_id: int) -> Sequence[tuple[str, int]]:
        stmt = (
            select(CommandExecuted.command_name, func.count(CommandExecuted.command_name))
            .where(CommandExecuted.author_id == user_id)
            .group_by(CommandExecuted.command_name)
            .order_by(func.count(CommandExecuted.command_name).desc())
        )
        return self.session.execute(stmt).all()

    def most_mentioned(self, author_id: int | None) -> Sequence[tuple[int, int]]:
        """
        Returned most mentioned people as tuple[mentioned_id, mention_amount].
        """
        stmt = select(Mention.mentioned_id, func.count(Mention.mentioned_id)).join(
            MessageSend, MessageSend.id == Mention.message_id
        )
        if author_id is not None:
            stmt = stmt.where(MessageSend.author_id == author_id)
        stmt = stmt.group_by(Mention.mentioned_id).order_by(func.count(Mention.mentioned_id).desc())
        return self.session.execute(stmt).all()

    def most_used_reactions(self, user_id: int | None) -> Sequence[tuple[str, int]]:
        """
        Returns most used emoji for reactions as tuple[emoji_id, amount]
        """
        # TODO: do we have emojis with the same name but different IDs?
        stmt = select(ReactionAdd.emoji_name, func.count(ReactionAdd.emoji_name))
        if user_id is not None:
            stmt = stmt.where(ReactionAdd.author_id == user_id)
        stmt = stmt.group_by(ReactionAdd.emoji_name).order_by(
            func.count(ReactionAdd.emoji_name).desc()
        )
        return self.session.execute(stmt).all()


def count_emojis(message: str) -> Counter:
    # discord emojis + unicode emojis
    return Counter(re.findall(EMOJI_REGEX, message)) + Counter(
        token.chars for token in emoji_analyze(message, join_emoji=True)
    )


def longest_date_streak(dates: set[datetime.date]) -> tuple[datetime.date, datetime.date] | None:
    if len(dates) == 0:
        return None
    if len(dates) == 1:
        date = next(iter(dates))
        return (date, date + datetime.timedelta(days=1))
    sorted_dates = sorted(dates)
    streak_start = sorted_dates[0]
    longest_streak = 1
    current_streak = 1
    previous_date = sorted_dates[0]
    for date in sorted_dates[1:]:
        if (date - previous_date).days == 1:
            current_streak += 1
        else:
            current_streak = 1
            streak_start = date
        longest_streak = max(longest_streak, current_streak)
        previous_date = date
    return (streak_start, streak_start + datetime.timedelta(days=longest_streak))


def most_used_voice_channels(
    voice_sessions: Sequence[VoiceSession],
) -> list[tuple[int, datetime.timedelta]]:
    """
    returns list of tuples (channel_id, time_in_channel)
    """
    channels = defaultdict(datetime.timedelta)
    for session in voice_sessions:
        if session.is_complete():
            channels[session.channel_id] += session.end - session.start
    return sorted(channels.items(), key=lambda x: x[1], reverse=True)


def stats_for_user(
    db: Database, user_id: int, voice_times: defaultdict[int, datetime.timedelta]
) -> UserStats:
    """
    voice_times: a dict mapping the id of another user to the amount of time spent with that user
    in voice
    """
    messages = db.messages(user_id)
    total_messages = len(list(messages))
    total_words = sum(len(message.content.strip().split(" ")) for message in messages)
    total_channels = len({message.channel_id for message in messages})
    emojis_counter = sum((count_emojis(message.content) for message in messages), start=Counter())
    distinct_days = {message.timestamp.date() for message in messages}
    message_edits = db.message_edits(user_id)
    message_deletes = db.message_deletes(user_id)
    longest_streak = longest_date_streak(distinct_days)
    most_used_channels = db.most_used_channels(user_id)
    most_used_commands = db.most_used_commands(user_id)
    voice_sessions = db.voice_sessions(user_id)
    voice_channels = most_used_voice_channels(voice_sessions)
    voice_time = sum(
        (session.session_length() for session in voice_sessions if session.is_complete()),
        start=datetime.timedelta(),
    )
    stream_sessions = db.stream_sessions(user_id)
    stream_time = sum(
        (session.session_length() for session in stream_sessions if session.is_complete()),
        start=datetime.timedelta(),
    )
    most_mentioned = db.most_mentioned(user_id)
    most_reacted_with_emoji = db.most_used_reactions(user_id)
    stats = UserStats(
        user_id=user_id,
        no_messages_sent=total_messages,
        no_messages_edited=len(list(message_edits)),
        no_messages_deleted=len(list(message_deletes)),
        no_words_sent=total_words,
        no_channels_used=total_channels,
        no_emojis_used=emojis_counter.total(),
        emojis_used=emojis_counter.most_common(),
        time_in_voice=voice_time,
        time_in_voice_with_others=sorted(voice_times.items(), key=lambda x: x[1], reverse=True),
        time_streamed=stream_time,
        distinct_days_chatted=len(distinct_days),
        longest_message_days_streak=longest_streak,
        most_used_channels=list(most_used_channels),
        most_used_commands=list(most_used_commands),
        most_used_voice_channels=voice_channels,
        most_mentioned_users=list(most_mentioned),
        most_reacted_with_emojis=list(most_reacted_with_emoji),
    )
    print(stats)
    return stats


def shared_voice_times(
    database: Database,
) -> defaultdict[int, defaultdict[int, datetime.timedelta]]:
    sessions = database.all_voice_sessions()
    events = list(
        itertools.chain.from_iterable(
            session.into_events() for session in sessions if session.is_complete()
        )
    )
    events.sort(key=lambda e: e.timestamp)

    # keys are channel IDs, values are dictionaries mapping user IDs to their *join* timestamp.
    channels = defaultdict(dict)
    # keys are user IDs, values are dictionaries mapping user IDs to timedeltas of their shared time
    user_times = defaultdict(lambda: defaultdict(datetime.timedelta))
    for event in events:
        match event.kind:
            case VoiceEventKind.Start:
                channels[event.channel_id][event.user_id] = event.timestamp
            case VoiceEventKind.End:
                join_timestamp = channels[event.channel_id].pop(event.user_id)
                for other_user_id, other_user_join_time in channels[event.channel_id].items():
                    shared_time = event.timestamp - max(join_timestamp, other_user_join_time)
                    user_times[event.user_id][other_user_id] += shared_time
                    user_times[other_user_id][event.user_id] += shared_time
    return user_times


def stats_for_guild(db: Database, user_stats: list[UserStats]) -> GuildStats:
    most_used_channels = db.most_used_channels(None)
    voice_sessions = db.voice_sessions(None)
    voice_channels = most_used_voice_channels(voice_sessions)
    voice_users = most_active_voice_users(user_stats)
    stream_users = most_active_stream_users(user_stats)
    message_users = most_active_message_users(user_stats)
    word_users = most_active_word_users(user_stats)
    consistent_users = most_consistent_users(user_stats)
    messages = db.messages(None)
    emojis_counter = sum((count_emojis(message.content) for message in messages), start=Counter())
    total_messages = len(list(messages))
    voice_time = sum(
        (session.session_length() for session in voice_sessions if session.is_complete()),
        start=datetime.timedelta(),
    )
    stream_sessions = db.stream_sessions(None)
    stream_time = sum(
        (session.session_length() for session in stream_sessions if session.is_complete()),
        start=datetime.timedelta(),
    )
    most_mentioned = db.most_mentioned(None)
    total_mentions = sum(m[1] for m in most_mentioned)
    most_reacted_with_emoji = db.most_used_reactions(None)
    return GuildStats(
        most_used_channels=list(most_used_channels),
        most_used_voice_channels=list(voice_channels),
        most_active_voice_users=voice_users,
        most_active_stream_user=stream_users,
        most_active_message_user=message_users,
        most_active_words_user=word_users,
        most_consistent_users=consistent_users,
        no_messages=total_messages,
        voice_time=voice_time,
        stream_time=stream_time,
        no_emojis=emojis_counter.total(),
        no_mentions=total_mentions,
        most_used_emojis=emojis_counter.most_common(),
        most_mentioned_users=list(most_mentioned),
        most_reacted_with_emojis=list(most_reacted_with_emoji),
    )


def most_active_voice_users(user_stats: list[UserStats]) -> list[tuple[int, datetime.timedelta]]:
    """
    Maps user ID to time in voice.
    """
    return [
        (user_stat.user_id, user_stat.time_in_voice)
        for user_stat in sorted(user_stats, key=lambda x: x.time_in_voice, reverse=True)
    ]


def most_active_stream_users(user_stats: list[UserStats]) -> list[tuple[int, datetime.timedelta]]:
    """
    Maps user ID to time streamed.
    """
    return [
        (user_stat.user_id, user_stat.time_streamed)
        for user_stat in sorted(user_stats, key=lambda x: x.time_streamed, reverse=True)
    ]


def most_active_message_users(user_stats: list[UserStats]) -> list[tuple[int, int]]:
    """
    Maps user ID to amount of messages sent.
    """
    return [
        (user_stat.user_id, user_stat.no_messages_sent)
        for user_stat in sorted(user_stats, key=lambda x: x.no_messages_sent, reverse=True)
    ]


def most_active_word_users(user_stats: list[UserStats]) -> list[tuple[int, int]]:
    """
    Maps user ID to amount of words sent.
    """
    return [
        (user_stat.user_id, user_stat.no_words_sent)
        for user_stat in sorted(user_stats, key=lambda x: x.no_words_sent, reverse=True)
    ]


def most_consistent_users(
    user_stats: list[UserStats],
) -> list[tuple[int, tuple[datetime.date, datetime.date]]]:
    return [
        (user_stat.user_id, user_stat.longest_message_days_streak)
        for user_stat in sorted(
            [
                user_stat
                for user_stat in user_stats
                if user_stat.longest_message_days_streak is not None
            ],
            key=lambda x: x.longest_message_days_streak[1] - x.longest_message_days_streak[0],
        )
    ]


def main():
    # TODO: only take into account statistics with timestamp <= start_time
    start_time = datetime.datetime.now(tz=datetime.UTC)
    print(f"Starting stats calculation for {start_time.isoformat()}.")
    with Session(db.BLOCKING_ENGINE) as session:
        database = Database(session)
        voice_times = shared_voice_times(database)
        # stats_for_user(database, 105357121406664704)
        user_stats = [
            stats_for_user(database, member.id, voice_times[member.id])
            for member in database.members()
        ]
        guild_stats = stats_for_guild(database, user_stats)
    OUTPUT_DIRECTORY.mkdir(exist_ok=True)
    file_name = f"guild-stats-163292084214824960-{start_time.isoformat()}.json"
    with open(
        OUTPUT_DIRECTORY.joinpath(file_name),
        "w",
    ) as fp:
        fp.write(guild_stats.model_dump_json())
    for user_stat in user_stats:
        file_name = f"user-stats-{user_stat.user_id}-{start_time.isoformat()}.json"
        with open(OUTPUT_DIRECTORY.joinpath(file_name), "w") as fp:
            fp.write(user_stat.model_dump_json())
    print("Finished stats calculation")


if __name__ == "__main__":
    main()
