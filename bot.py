from __future__ import annotations

import asyncio
import logging
import traceback
from logging import LogRecord
from logging.handlers import RotatingFileHandler
from pathlib import Path
from typing import TYPE_CHECKING

import aiohttp
import discord
from discord.ext import commands, tasks

import config
from utils.errors import InvariantError

if TYPE_CHECKING:
    from utils.context import CommandContext

initial_extensions = [
    "cogs.discord_rewind",
    "cogs.locale",
    "cogs.birthdays",
    "cogs.welcome",
    "cogs.steam",
    "cogs.twitch",
    "cogs.discord_streams",
    "cogs.reaction_roles",
    "cogs.custom_commands",
    "cogs.testing",
    "cogs.text_transformation",
    "cogs.remindme",
    "cogs.admin",
    "cogs.rtfm",
    "cogs.vps",
    "cogs.activity",
    "cogs.what",
    "cogs.owo",
    "cogs.emoji",
    "cogs.jabomons",
    "cogs.powerrankings",
    "cogs.year_progress",
    "cogs.oneuppuzzle",
    "cogs.gitlab",
]


def init_logger():
    """
    Initialize the logger

    Create a console logger and a seperate file logger
    Filter urllib and discord logs
    """

    def log_filter(record: LogRecord):
        return "urllib" not in record.name and "discord" not in record.name

    file_handler = RotatingFileHandler(
        Path(__file__).parent.joinpath("bot.log"),
        maxBytes=1024 * 1024,
        backupCount=3,
        encoding="utf8",
        delay=True,
    )
    file_handler.addFilter(log_filter)

    console_handler = logging.StreamHandler()
    console_handler.addFilter(log_filter)

    logging.basicConfig(
        handlers=(file_handler, console_handler),
        level=logging.INFO,
        format="%(asctime)s [%(name)-16.16s] [%(levelname)-4.4s]  %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )


class Bot(commands.Bot):
    user: discord.ClientUser
    aiohttp_session: aiohttp.ClientSession

    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        self.aiohttp_session = None  # type: ignore
        self.config: config.Config = config.config
        super().__init__(*args, **kwargs)

    async def setup_hook(self):
        self.aiohttp_session = aiohttp.ClientSession(loop=asyncio.get_running_loop())
        for extension in initial_extensions:
            try:
                await self.load_extension(extension)
                self.logger.info("Successfully loaded %s", extension)
            except Exception:
                self.logger.exception(
                    "Failed to load initial extension %s: %s", extension, traceback.format_exc()
                )
        self.join_active_threads.start()

    async def on_message(self, _message: discord.Message):
        """
        Emptied so the commands wont be executed twice, all command execution
        happens in cogs/custom_commands.py
        """
        pass

    async def on_error(self, event: str, *_args, **_kwargs):
        self.logger.exception("Non-command exception in %s.", event)

    async def on_command_error(self, ctx: CommandContext, error: BaseException):
        if hasattr(ctx.command, "on_error"):
            return
        if isinstance(error, InvariantError):
            await ctx.send(f"Failed operation that should never fail: {error}")
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send_help(ctx.command)
            await ctx.send(f"Missing required argument `{error.param}`.")
        elif isinstance(error, commands.BadArgument):
            await ctx.send_help(ctx.command)
            await ctx.send(f"Failed to parse argument: `{error}`")
        elif isinstance(error, commands.NoPrivateMessage):
            await ctx.send("This command may not be used in DMs.")
        elif isinstance(error, commands.MissingPermissions):
            await ctx.reply(str(error))
        else:
            tb = "".join(traceback.format_exception(error))
            self.logger.error(
                "Ignoring exception in command %s:\n%s", ctx.command.qualified_name, tb
            )

    async def on_ready(self):
        if self.user is not None:
            self.logger.info("Successfully logged in as %s - %s", self.user.name, self.user.id)
        self.logger.info("discordpy version: %s.", discord.__version__)
        self.logger.info("Ready")

    async def on_thread_create(self, thread: discord.Thread):
        if self.user is None:
            return
        try:
            await thread.fetch_member(self.user.id)
        except discord.NotFound:
            self.logger.info("Detected newly created thread %s - attempting to join!", thread.name)
            await thread.join()
        except Exception:
            return

    @tasks.loop(hours=12)
    async def join_active_threads(self):
        if self.user is None:
            return
        for guild in self.guilds:
            for thread in await guild.active_threads():
                try:
                    await thread.fetch_member(self.user.id)
                except discord.NotFound:
                    self.logger.info("Found unjoined thread %s - joining!", thread.name)
                    await thread.join()
                except discord.Forbidden:
                    self.logger.info("Could not join thread %s - missing access!")
                except Exception:
                    self.logger.exception(
                        "Unknown error fetching members for thread %s.", thread.name
                    )
                    continue

    @join_active_threads.before_loop
    async def before_join_active_threads(self):
        await self.wait_until_ready()


if __name__ == "__main__":
    init_logger()
    bot = Bot(
        command_prefix="!",
        activity=discord.Game(name="!help <command>", type=1),
        intents=discord.Intents.all(),
        member_cache_flags=discord.MemberCacheFlags.all(),
        log_level=logging.INFO,
        help_command=commands.DefaultHelpCommand(width=500),
    )
    bot.run(config.config.token, reconnect=True)
