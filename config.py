from __future__ import annotations

import tomllib
from dataclasses import dataclass
from pathlib import Path


@dataclass
class CaldavCredentials:
    username: str
    password: str
    base_url: str
    calendar_link: str


@dataclass
class IDs:
    # guilds
    guild_id: int
    # categories
    gaming_category: int
    # channels and threads
    bot_channel_id: int
    stream_channel_id: int
    tavern_id: int
    jousting_stadium: int
    tournament_suggestions: int
    one_word_only_channel: int
    reaction_roles_channel: int
    oneuppuzzle_channel: int
    # roles
    knight_role_id: int
    council_role: int
    state_secretary_role: int
    admin_role: int
    # users
    no_command_user_id: int
    king_id: int
    # emotes
    emt_min: int
    emt_two: int
    emt_three: int
    emt_four: int
    emt_five: int
    emt_six: int
    emt_seven: int
    emt_eight: int
    emt_nine: int
    emt_ten: int
    emt_max: int


@dataclass
class Config:
    ids: IDs
    token: str
    catapi_key: str
    challonge_api_key: str
    steam_api_key: str
    twitch_id: str
    twitch_secret: str
    trn_api_key: str
    rlstats_api_key: str
    gitlab_personal_access_token: str
    gitlab_project_id: int
    gitlab_assignee_id: int
    vps_ip: str
    vps_domain: str
    twitter_consumer_key: str
    twitter_consumer_secret: str
    twitter_access_token_key: str
    twitter_access_token_secret: str
    pg_user: str
    pg_pass: str
    pg_host: str
    rarities: dict[int, float]
    report_reasons: list[dict[str, str]]
    shiny_chance: float
    caldav_credentials: dict[int, CaldavCredentials]
    oup_api_key: str
    oup_collection_id: str
    oup_project_id: str
    oup_firestore_api_url: str

    @classmethod
    def from_dict(cls, data: dict) -> Config:
        data["ids"] = IDs(**data["ids"])
        data["rarities"] = {int(k): v for k, v in data["rarities"].items()}  # make rarity keys ints
        data["caldav_credentials"] = {
            int(k): CaldavCredentials(**v) for k, v in data["caldav_credentials"].items()
        }
        return cls(**data)


with open(Path(__file__).parent.joinpath("config.toml"), "rb") as filp:
    config = Config.from_dict(tomllib.load(filp))
