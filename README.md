
General purpose discordbot using [discordpy](https://github.com/Rapptz/discord.py).
Not meant to be run as is, but all the different functions are grouped into modules in the `cogs/`
directory.
