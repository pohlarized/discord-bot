import discord
from discord.ext.commands import Command, Context

from bot import Bot


class BotContext(Context):
    bot: Bot


class CommandContext(BotContext):
    command: Command


class GuildContext(BotContext):
    author: discord.Member
    guild: discord.Guild
    channel: discord.VoiceChannel | discord.TextChannel | discord.Thread
    me: discord.Member
    prefix: str
