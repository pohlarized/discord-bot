from .constants import MAX_MESSAGE_LENGTH
from .paginate import paginate_codeblock


def format_table_message(
    table: str, headline: str | None = None, subtext: str | None = None
) -> list[str]:
    headline = f"## {headline}" if headline is not None else ""
    subtext = f"-# {subtext}" if subtext is not None else ""
    table_pages = paginate_codeblock(
        f"```{table}```", MAX_MESSAGE_LENGTH - len(headline) - len(subtext) - 6 - 2
    )
    if headline:
        table_pages[0] = f"{headline}\n{table_pages[0]}"
    if subtext:
        table_pages[-1] = f"{table_pages[-1]}\n{subtext}"
    return table_pages
