from zoneinfo import ZoneInfo

TIMEZONE = ZoneInfo("Europe/Berlin")
MAX_MESSAGE_LENGTH = 2000
