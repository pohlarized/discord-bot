import datetime

AMOUNT_SUFFIXES = {1: "st", 2: "nd", 3: "rd"}


def stndrdth(number: int) -> str:
    """
    Get the proper suffix ("st", "nd", "rd" or "th") for a given number.

    Parameters
    ----------
    number : int
        the number you want to get the suffix of.
    """
    if number % 100 in (11, 12, 13):
        return "th"
    return AMOUNT_SUFFIXES.get(number % 10, "th")


def plural_s(amount: int) -> str:
    """
    Return an "s" if one is required for the unit given this amount, otherwise return an empty
    string.

    Params
    ------
    amount: int
        The amount

    Returns
    -------
    str
        "s" if amount != 1 else ""
    """
    return "s" if amount != 1 else ""


def format_timedelta(td: datetime.timedelta) -> str:
    years = td.days // 365
    months = (td.days % 365) // 30
    days = (td.days % 365) % 30
    hours = td.seconds // 3600
    minutes = (td.seconds % 3600) // 60
    seconds = (td.seconds % 3600) % 60
    text_parts = [
        f"{amount} {unit}{plural_s(amount)}"
        for amount, unit in (
            (years, "year"),
            (months, "month"),
            (days, "day"),
            (hours, "hour"),
            (minutes, "minute"),
            (seconds, "second"),
        )
        if amount != 0
    ]
    return ", ".join(text_parts)
