from collections.abc import Iterable

from utils.constants import MAX_MESSAGE_LENGTH


def paginate_multiple_separators(
    message: str, *, maxlen: int = MAX_MESSAGE_LENGTH, separators: Iterable[str] = "\n"
) -> list[str]:
    messages = []
    while len(message) > maxlen:
        # Find the last occurence of one of the split_chars whose index is before maxlen
        # The char we split by is always the last char of a page/split.
        splits = [
            -(message[:maxlen][::-1].find(char)) - (len(message) - maxlen) for char in separators
        ]
        split_pos = max(splits)
        messages.append(message[:split_pos])
        message = message[split_pos:].strip()
    messages.append(message)
    return messages


def paginate_single_separator(
    message: str, *, maxlen: int = MAX_MESSAGE_LENGTH, separator: str = "\n"
) -> list[str]:
    splits = message.split(separator)
    messages = []
    current_message = []
    while splits:
        while (
            splits
            and sum(len(m) for m in current_message)
            + len(splits[0])
            + len(current_message) * len(separator)
            < maxlen
        ):
            current_message.append(splits.pop(0))
        messages.append(separator.join(current_message))
        current_message.clear()
    return messages


def paginate_codeblock(message: str, maxlen: int = MAX_MESSAGE_LENGTH - 6) -> list[str]:
    """
    Given a message that is enclosed in a "large" code block (```), split it into multiple
    codeblocks if it exceeds the message limit.
    Splits the message at the latest possible newline.

    Params
    ------
    msg: str
        The message to split.

    Returns
    -------
    list[str]
        The resulting split messages. They're already formated into code blocks.
    """
    raw_content = message.removeprefix("```").removesuffix("```")
    messages = paginate_single_separator(raw_content, maxlen=maxlen, separator="\n")
    return [f"```{message}```" for message in messages]
