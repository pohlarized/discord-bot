class InvariantError(BaseException):
    pass


class KnownGuildNotFoundError(InvariantError):
    def __init__(self, guild_id: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.guild_id = guild_id
        self.message = f"Unexpectedly failed to find guild with ID {guild_id}."

    def __str__(self) -> str:
        return self.message


class KnownChannelNotFoundError(InvariantError):
    def __init__(self, channel_id: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.channel_id = channel_id
        self.message = f"Unexpectedly failed to find channel with ID {channel_id}."

    def __str__(self) -> str:
        return self.message


class UnexpectedChannelTypeError(InvariantError):
    def __init__(self, channel_id: int, kind: type, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.channel_id = channel_id
        self.message = f"Channel with ID {channel_id} had unexpected type: {kind}."

    def __str__(self) -> str:
        return self.message
